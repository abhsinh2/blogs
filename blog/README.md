# MongoDB

## Schema

Collection Name: blogs

Structure
```
{
	"_id": ""
	"content": "", 
	"urllink": "", 
	"author": "", 
	"title": "", 
	"tags": ["", ""], 
	"comments": [
		{
			"content": "", 
			"email": "", 
			"author": "",
			"date": ""
		}
	],
	"date": ""
}
```

Collection Name: users

Structure
```
{
	"_id": "<username>", 
	"email": "", 
	"password": "",
	"firstname": "",
	"lastname": "",
	"isCompany": ,
	"companyId", "",
	"siteId": "",
	"deptId": "",
	"subdomain": ""
}
```

Collection Name: sessions

Structure
```
{
	"username": "",
	"_id": "<sessionId>"
}
```

Collection Name: companies

Structure
```
{
	{ 
	"_id" : ObjectId(""), 
	"name" : "", 
	"sites" : [ {
		 "id" : , 
		 "name" : "", 
		 "departments" : [ { 
		 	"id" : , 
		 	"name" : "" 
		 }] 
	 }] 
	}
}
```

## Indexes: For fast retrieval

### blogs:

{"urllink": 1}

{"author": 1}

{"author": 1, "date": -1}

{"tags": 1}

{"tags": 1, "date": -1}

{"date": -1}

### sessions:

{"username":1}

### companies:

{"sites.id":1}
{"sites.departments.id":1}

# How to run

## Standalone

you need to create one json file having information as below

```
{
	"http.port": 8080,
	"https.port": 4443,
	"db_name": "blog",
	"connection_string": "mongodb://localhost:27017",
	"blog_collection": "blogs",
	"session_collection": "sessions",
	"user_collection": "users",
	"company_collection": "companies"
}
```

export JAVA_HOME=`/usr/libexec/java_home -v 1.8`

### Using internal configuration file

java -cp target/dependency/*:target/blog-1.0.1-SNAPSHOT.jar com.cisco.cmad.blog.StartUp

### Using external configuration file passed as system variable

java -cp target/dependency/*:target/blog-1.0.1-SNAPSHOT.jar -DBlogConfFile=<Full_Path_To_JSON> com.cisco.cmad.blog.StartUp

Example:

java -cp target/dependency/*:target/blog-1.0.1-SNAPSHOT.jar -DBlogConfFile=/Users/abhsinh2/Desktop/blog-application-conf.json com.cisco.cmad.blog.StartUp

### Using external configuration file passed as argument to main class

java -cp target/dependency/*:target/blog-1.0.1-SNAPSHOT.jar com.cisco.cmad.blog.StartUp <Full_Path_To_JSON>

Example:

java -cp target/dependency/*:target/blog-1.0.1-SNAPSHOT.jar com.cisco.cmad.blog.StartUp "/Users/abhsinh2/Desktop/blog-application-conf.json"

### Using big fat file

java -jar /usr/src/blog/blog-1.0.1-SNAPSHOT-fat.jar com.cisco.cmad.blog.StartUp

## Using Maven

mvn clean install test -DskipTests=true -Prun-startup

## Through Docker

### Build Docker

docker build -t blog-app .

### Run Docker

### Using internal configuration file

docker run -it --name blog-running-app -p 8084:8080 -p 4443:4443 blog-app

### Using internal configuration file

docker run -v <PATH/blog-application-conf.json>:/usr/blog/src/main/resources/conf/blog-application-conf.json -it --name blog-running-app blog-app

## Running Unit tests cases

mvn clean test -DskipITs=true -DskipUTs=false -DRunMongo=true

## Running Integration test cases

mvn clean verify -DskipUTs=true -DRunMongo=false





