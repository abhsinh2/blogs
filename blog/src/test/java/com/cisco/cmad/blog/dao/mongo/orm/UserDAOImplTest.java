package com.cisco.cmad.blog.dao.mongo.orm;

import java.io.IOException;

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.cisco.cmad.blog.model.User;

import de.flapdoodle.embed.mongo.MongodProcess;

/**
 * Test class to test morphic based api for UserDAO
 * 
 * @author abhsinh2
 *
 */
public class UserDAOImplTest extends MongoDAOTest {

	private UserDAOImpl userDAO;
	protected static MongodProcess MONGO;
	
	public UserDAOImplTest() {
		super();
		userDAO = new UserDAOImpl(dataStore);
	}

	@BeforeClass
	public static void beforeClass() throws IOException, InterruptedException {
		MONGO = MongoDAOTest.mongoStart(null);
		Thread.sleep(5000);
	}

	@AfterClass
	public static void afterClass() throws InterruptedException {
		MongoDAOTest.mongoShutdown(MONGO);
		Thread.sleep(5000);
	}
	
	@Before
	public void beforeTest() {
		
	}
	
	@After
	public void afterTest() {
		
	}
	
	@Test
	public void testAddUserObject() {
		this.userDAO.clearAll();
		
		// Test where user username is null
		User user = new User();
		boolean isCreated = userDAO.add(user);
		assertFalse(isCreated);
				
		// Test where user password is null
		user = new User();
		user.setUsername("Jerry");		
		isCreated = userDAO.add(user);
		assertFalse(isCreated);
		
		// Test adding new User
		user = new User();
		user.setUsername("Jerry");
		user.setEmail("jerry@gmail.com");
		user.setPassword("welcome");		
		isCreated = userDAO.add(user);
		assertTrue(isCreated);
		
		// Test adding same user again
		isCreated = userDAO.add(user);
		assertFalse(isCreated);
	}
	
	@Test
	public void testValidLogin() {
		userDAO.clearAll();
		
		String pass = "welcome";
		String username = "Jerry";
		
		User user = new User();
		user.setUsername(username);
		user.setPassword(pass);
		user.setEmail("jerry@gmail.com");		
		
		boolean isCreated = userDAO.add(user);
		assertTrue(isCreated);
		
		user = userDAO.validateLogin(username, pass);
		assertNotNull(user);
		
		user = userDAO.validateLogin("Jerry", "welcome123");
		assertNull(user);
		
		user = userDAO.validateLogin("Jerry123", "welcome123");
		assertNull(user);
	}
	
	@Test
	public void testFindByUsername() {
		userDAO.clearAll();
		
		String username = "Jerry";
		String pass = "welcome";		
		
		User user = new User();
		user.setUsername(username);
		user.setPassword(pass);
		user.setEmail("jerry@gmail.com");		
		
		boolean isCreated = userDAO.add(user);
		assertTrue(isCreated);
		
		User result = userDAO.findByUsername(username);
		assertNotNull(result);
		
		result = userDAO.findByUsername("ramdom");
		assertNull(result);
	}
	
	@Test
	public void testClearAll() {
		userDAO.clearAll();
		
		String username = "Jerry";
		String pass = "welcome";		
		
		User user = new User();
		user.setUsername(username);
		user.setPassword(pass);
		user.setEmail("jerry@gmail.com");		
		
		boolean isCreated = userDAO.add(user);
		assertTrue(isCreated);
		
		User result = userDAO.findByUsername(username);
		assertNotNull(result);
		
		userDAO.clearAll();
		
		result = userDAO.findByUsername(username);
		assertNull(result);
	}

}
