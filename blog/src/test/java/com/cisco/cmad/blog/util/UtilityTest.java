package com.cisco.cmad.blog.util;

import static org.junit.Assert.*;
import org.junit.Test;


/**
 * Test class to test Utility.
 * 
 * @author abhsinh2
 *
 */
public class UtilityTest {

	@Test
	public void testMakePasswordHash() {
		String pass = "welcome";
		String appender = "12345";
		String newPass = new String("welcome");
		
		String hashedPass1 = Utility.encodePassword(pass, appender);	
		String hashedPass2 = Utility.encodePassword(pass, appender);
		String hashedPass3 = Utility.encodePassword(newPass, appender);
		
		String hashedPass4 = Utility.decodePassword(hashedPass1);	
		
		assertTrue(hashedPass1.equals(hashedPass2));
		assertTrue(hashedPass1.equals(hashedPass3));
		
		assertTrue(pass.equals(hashedPass4));
	}

	@Test
	public void testToVertxJson() {
		com.google.gson.JsonObject googleJsonObject = new com.google.gson.JsonObject();
		googleJsonObject.addProperty("name", "Jerry");
		
		io.vertx.core.json.JsonObject vertxJsonObject = Utility.toVertxJson(googleJsonObject);
		
		assertEquals(googleJsonObject.get("name").getAsString(), vertxJsonObject.getString("name"));
	}

}
