package com.cisco.cmad.blog;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import com.cisco.cmad.blog.util.Utility;
import com.google.gson.JsonElement;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import de.flapdoodle.embed.mongo.MongodExecutable;
import de.flapdoodle.embed.mongo.MongodProcess;
import de.flapdoodle.embed.mongo.MongodStarter;
import de.flapdoodle.embed.mongo.config.IMongodConfig;
import de.flapdoodle.embed.mongo.config.MongodConfigBuilder;
import de.flapdoodle.embed.mongo.config.Net;
import de.flapdoodle.embed.mongo.distribution.Version;
import de.flapdoodle.embed.process.runtime.Network;

public abstract class BlogTest {

	public BlogTest() {
		
	}
	
	public static JsonObject readUTConfigurationFile() {
		String jsonFilePath = System.getProperty("user.dir") + "/src/test/resources"
				+ "/conf/blog-unit-test-conf.json";
		try {
			JsonElement root = new JsonParser().parse(new FileReader(new File(jsonFilePath)));	
			return root.getAsJsonObject();
		} catch (JsonIOException | JsonSyntaxException | FileNotFoundException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public static JsonObject readITConfigurationFile() {
		String jsonFilePath = System.getProperty("user.dir") + "/src/test/resources"
				+ "/conf/blog-integration-test-conf.json";
		try {
			JsonElement root = new JsonParser().parse(new FileReader(new File(jsonFilePath)));	
			return root.getAsJsonObject();
		} catch (JsonIOException | JsonSyntaxException | FileNotFoundException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public static JsonObject readConfiguration() {
		try {
			JsonObject rootJsonObject = BlogTest.readUTConfigurationFile();
			Utility.setConfigurationJsonObject(rootJsonObject);	
			return rootJsonObject;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static MongodProcess mongoStart(JsonObject rootJsonObject) throws IOException {
		if (rootJsonObject == null)
			rootJsonObject = readConfiguration();
		
		System.out.println(rootJsonObject);
		int MONGO_PORT = rootJsonObject.get("mongo_port").getAsInt();
		
		String runMongo = System.getProperty("RunMongo");
		
		if (runMongo != null && runMongo.equals("false")) {
			// Dont run Mongo
		} else {
			MongodStarter starter = MongodStarter.getDefaultInstance();

			IMongodConfig mongodConfig = new MongodConfigBuilder().version(Version.Main.PRODUCTION)
					.net(new Net(MONGO_PORT, Network.localhostIsIPv6())).build();

			MongodExecutable mongodExecutable = starter.prepare(mongodConfig);
			MongodProcess MONGO = mongodExecutable.start();
			return MONGO;
		}
		
		return null;		
	}

	public static void mongoShutdown(MongodProcess MONGO) {
		if (MONGO != null)
			MONGO.stop();
	}
}
