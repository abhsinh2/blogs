package com.cisco.cmad.blog.verticle;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.cisco.cmad.blog.BlogTest;
import com.cisco.cmad.blog.dto.BlogDTO;
import com.cisco.cmad.blog.dto.UserDTO;
import com.cisco.cmad.blog.model.Blog;
import com.cisco.cmad.blog.model.User;
import com.cisco.cmad.blog.util.Utility;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import de.flapdoodle.embed.mongo.MongodProcess;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.json.Json;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;

/**
 * Unit test case to test BlockedDBVertxBlogVerticle
 * 
 * @author abhsinh2
 *
 */
@RunWith(VertxUnitRunner.class)
public class VertxMongoVertxBlogVerticleTest extends VertxBlogVerticleTest {

	private Vertx vertx;
	private static MongodProcess MONGO;

	public VertxMongoVertxBlogVerticleTest() throws Exception {
		// using MONGO_NATIVE_API to add users. But Verticle will provide
		// implementation using vertx-mongo api
		super(DB_ACCESS_TYPE.MONGO_NATIVE_API);
	}

	@BeforeClass
	public static void start() throws IOException, InterruptedException {
		MONGO = BlogTest.mongoStart(null);
		Thread.sleep(5000);
	}
	
	@AfterClass
	public static void shutdown() throws InterruptedException {
		BlogTest.mongoShutdown(MONGO);
		Thread.sleep(5000);
	}

	/**
	 * Deploy our verticle.
	 *
	 * @param context
	 *            the test context.
	 */
	@Before
	public void setUp(TestContext context) throws IOException {
		//blogDAO.clearAll();
		//userDAO.clearAll();
		//sessionDAO.clearAll();

		vertx = Vertx.vertx();

		io.vertx.core.json.JsonObject configJsonObject = new io.vertx.core.json.JsonObject();
		configJsonObject.put("testField1", 123).put("testField2", "hello");

		DeploymentOptions options = new DeploymentOptions().setConfig(configJsonObject);

		VertxMongoVertxBlogVerticle blogVerticle = new VertxMongoVertxBlogVerticle(vertx);
		vertx.deployVerticle(blogVerticle, options, context.asyncAssertSuccess());
	}

	/**
	 * cleanup everything by closing the vert.x instance
	 *
	 * @param context
	 *            the test context
	 */
	@After
	public void tearDown(TestContext context) {
		blogDAO.clearAll();
		userDAO.clearAll();
		sessionDAO.clearAll();
		
		if (vertx != null && context != null)
			vertx.close(context.asyncAssertSuccess());
	}

	@Test
	public void testAddNewBlog(TestContext context) {
		Async async = context.async();

		User user = new User();
		user.setUsername("Jerry");
		user.setPassword("welcome");
		user.setFirstname("Jerry");
		user.setLastname("J");
		user.setEmail("jerry@gmail.com");

		userDAO.add(user);

		BlogDTO blogDto = new BlogDTO();
		blogDto.setAuthor("Jerry");
		blogDto.setTitle("Title");
		blogDto.setContent("Content");

		final String json = Json.encodePrettily(blogDto);

		vertx.createHttpClient().post(Utility.getHttpPort(), "localhost", "/services/blogs/newblog")
				.putHeader(HttpHeaders.CONTENT_TYPE, "application/json")
				.putHeader(HttpHeaders.CONTENT_LENGTH, Integer.toString(json.length())).handler(response -> {
					context.assertEquals(response.statusCode(), 201);
					context.assertTrue(response.headers().get(HttpHeaders.CONTENT_TYPE).contains("text/html"));
					response.bodyHandler(body -> {
						//context.assertTrue(body.toString().contains("Title"));
						async.complete();
					});
				}).write(json).end();
	}

	@Test
	public void testGetBlogByUrllink(TestContext context) {
		Async async = context.async();

		User user = new User();
		user.setUsername("Jerry");
		user.setPassword("welcome");
		user.setFirstname("Jerry");
		user.setLastname("J");
		user.setEmail("jerry@gmail.com");

		//boolean isUserAdded = userDAO.add(user);
		//context.assertTrue(isUserAdded);

		Set<String> tags = new HashSet<>();
		tags.add("java");

		Blog blog = new Blog();
		blog.setAuthor("Jerry");
		blog.setTitle("Title1");
		blog.setContent("Content");
		blog.setTags(tags);

		String urllink = blogDAO.add(blog);

		vertx.createHttpClient().getNow(Utility.getHttpPort(), "localhost", "/services/blogs/" + urllink, response -> {
			context.assertEquals(response.statusCode(), 200);
			context.assertTrue(response.headers().get(HttpHeaders.CONTENT_TYPE).contains("application/json"));
			response.handler(body -> {
				JsonElement root = new JsonParser().parse(body.toString());
				com.google.gson.JsonObject jsonObj = root.getAsJsonObject();
				context.assertTrue(jsonObj.get("title").getAsString().equals(blog.getTitle()));
				async.complete();
			});
		});

		Async async1 = context.async();
		String dummy = "dummy";
		vertx.createHttpClient().getNow(Utility.getHttpPort(), "localhost", "/services/blogs/" + dummy, response -> {
			context.assertEquals(response.statusCode(), 404);
			async1.complete();
		});
	}

	@Test
	public void testGetBlogsForUser(TestContext context) {
		Async async = context.async();

		User user = new User();
		user.setUsername("Jerry");
		user.setPassword("welcome");
		user.setFirstname("Jerry");
		user.setLastname("J");
		user.setEmail("jerry@gmail.com");

		userDAO.add(user);

		Set<String> tags = new HashSet<>();
		tags.add("java");

		String author = "Jerry";

		Blog blog = new Blog();
		blog.setAuthor(author);
		blog.setTitle("Title1");
		blog.setContent("Content");
		blog.setTags(tags);

		blogDAO.add(blog);

		vertx.createHttpClient().get(Utility.getHttpPort(), "localhost", "/services/blogs/user/" + author)
				.putHeader(HttpHeaders.CONTENT_TYPE, "text/html")
				.putHeader(HttpHeaders.CONTENT_LENGTH, Integer.toString(author.length())).handler(response -> {
					context.assertEquals(response.statusCode(), 201);
					context.assertTrue(response.headers().get(HttpHeaders.CONTENT_TYPE).contains("application/json"));
					response.bodyHandler(body -> {
						JsonElement root = new JsonParser().parse(body.toString());
						com.google.gson.JsonArray rootArray = root.getAsJsonArray();
						com.google.gson.JsonObject jsonObj = rootArray.get(0).getAsJsonObject();
						context.assertTrue(jsonObj.get("title").getAsString().equals(blog.getTitle()));
						async.complete();
					});
				}).write(author).end();

		String dummy = "dummy";

		vertx.createHttpClient().get(Utility.getHttpPort(), "localhost", "/services/blogs/user/" + dummy)
				.putHeader(HttpHeaders.CONTENT_TYPE, "text/html")
				.putHeader(HttpHeaders.CONTENT_LENGTH, Integer.toString(dummy.length())).handler(response -> {
					context.assertEquals(response.statusCode(), 201);
					response.bodyHandler(body -> {
						JsonElement root = new JsonParser().parse(body.toString());
						com.google.gson.JsonArray rootArray = root.getAsJsonArray();
						context.assertTrue(rootArray.size() == 0);
						async.complete();
					});
				}).write(dummy).end();
	}

	@Test
	public void testGetBlogsForTag(TestContext context) {
		Async async = context.async();

		User user = new User();
		user.setUsername("Jerry");
		user.setPassword("welcome");
		user.setFirstname("Jerry");
		user.setLastname("J");
		user.setEmail("jerry@gmail.com");

		userDAO.add(user);

		String tag = "java";

		Set<String> tags = new HashSet<>();
		tags.add(tag);

		Blog blog = new Blog();
		blog.setAuthor("Jerry");
		blog.setTitle("Title1");
		blog.setContent("Content");
		blog.setTags(tags);

		blogDAO.add(blog);

		vertx.createHttpClient().get(Utility.getHttpPort(), "localhost", "/services/blogs/tag/" + tag)
				.putHeader(HttpHeaders.CONTENT_TYPE, "text/html")
				.putHeader(HttpHeaders.CONTENT_LENGTH, Integer.toString(tag.length())).handler(response -> {
					context.assertEquals(response.statusCode(), 201);
					context.assertTrue(response.headers().get(HttpHeaders.CONTENT_TYPE).contains("application/json"));
					response.bodyHandler(body -> {
						JsonElement root = new JsonParser().parse(body.toString());
						com.google.gson.JsonArray rootArray = root.getAsJsonArray();
						com.google.gson.JsonObject jsonObj = rootArray.get(0).getAsJsonObject();
						context.assertTrue(jsonObj.get("title").getAsString().equals(blog.getTitle()));
						async.complete();
					});
				}).write(tag).end();

		String dummy = "dummy";

		vertx.createHttpClient().get(Utility.getHttpPort(), "localhost", "/services/blogs/tag/" + dummy)
				.putHeader(HttpHeaders.CONTENT_TYPE, "text/html")
				.putHeader(HttpHeaders.CONTENT_LENGTH, Integer.toString(dummy.length())).handler(response -> {
					context.assertEquals(response.statusCode(), 201);
					response.bodyHandler(body -> {
						JsonElement root = new JsonParser().parse(body.toString());
						com.google.gson.JsonArray rootArray = root.getAsJsonArray();
						context.assertTrue(rootArray.size() == 0);
						async.complete();
					});
				}).write(dummy).end();
	}

	@Test
	public void testDeleteBlog(TestContext context) {
		Async async = context.async();
		async.complete();
	}

	@Test
	public void testBlogNotFound(TestContext context) {
		Async async = context.async();
		async.complete();
	}

	@Test
	public void testAddNewUser(TestContext context) {
		Async async = context.async();

		UserDTO userDto = new UserDTO();
		userDto.setUsername("Jerry");
		userDto.setPassword("welcome");
		userDto.setFirstname("Jerry");
		userDto.setLastname("J");
		userDto.setEmail("jerry@gmail.com");

		final String json = Json.encodePrettily(userDto);

		vertx.createHttpClient().post(Utility.getHttpPort(), "localhost", "/services/users/signup")
				.putHeader(HttpHeaders.CONTENT_TYPE, "application/json")
				.putHeader(HttpHeaders.CONTENT_LENGTH, Integer.toString(json.length())).handler(response -> {
					context.assertEquals(response.statusCode(), 200);
					context.assertTrue(response.headers().get(HttpHeaders.CONTENT_TYPE).contains("application/json"));
					response.bodyHandler(body -> {
						//context.assertTrue(body.toString().contains("User Added"));
						async.complete();
					});
				}).write(json).end();
	}

	@Test
	public void testLogin(TestContext context) {
		Async async = context.async();

		UserDTO userDto = new UserDTO();
		userDto.setUsername("Jerry");
		userDto.setPassword("welcome");
		userDto.setFirstname("Jerry");
		userDto.setLastname("J");
		userDto.setEmail("jerry@gmail.com");

		userDAO.add(userDto.toModel());

		UserDTO loginDto = new UserDTO();
		loginDto.setUsername("Jerry");
		loginDto.setPassword("welcome");

		String json = Json.encodePrettily(loginDto);

		// Test valid login
		vertx.createHttpClient().post(Utility.getHttpPort(), "localhost", "/services/users/login")
				.putHeader(HttpHeaders.CONTENT_TYPE, "application/json")
				.putHeader(HttpHeaders.CONTENT_LENGTH, Integer.toString(json.length())).handler(response -> {
					context.assertEquals(response.statusCode(), 201);
					context.assertTrue(response.headers().get(HttpHeaders.CONTENT_TYPE).contains("text/html"));
					response.bodyHandler(body -> {
						//context.assertTrue(body.toString().contains("Valid Login"));
						async.complete();
					});
				}).write(json).end();

		// Test invalid login
		loginDto = new UserDTO();
		loginDto.setUsername("Jerry");
		loginDto.setPassword("welcome123");
		json = Json.encodePrettily(loginDto);

		vertx.createHttpClient().post(Utility.getHttpPort(), "localhost", "/services/users/login")
				.putHeader(HttpHeaders.CONTENT_TYPE, "application/json")
				.putHeader(HttpHeaders.CONTENT_LENGTH, Integer.toString(json.length())).handler(response -> {
					context.assertEquals(response.statusCode(), 401);
					context.assertTrue(response.headers().get(HttpHeaders.CONTENT_TYPE).contains("text/html"));
					response.bodyHandler(body -> {
						//context.assertTrue(body.toString().contains("Invalid Credentials"));
						async.complete();
					});
				}).write(json).end();
	}

	@Test
	public void testLogout(TestContext context) {
		Async async = context.async();
		async.complete();
	}
}
