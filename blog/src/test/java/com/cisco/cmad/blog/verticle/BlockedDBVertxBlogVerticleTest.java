package com.cisco.cmad.blog.verticle;

import java.io.IOException;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.bson.types.ObjectId;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.cisco.cmad.blog.BlogTest;
import com.cisco.cmad.blog.dto.BlogDTO;
import com.cisco.cmad.blog.dto.CommentDTO;
import com.cisco.cmad.blog.dto.CompanyDTO;
import com.cisco.cmad.blog.dto.DepartmentDTO;
import com.cisco.cmad.blog.dto.SiteDTO;
import com.cisco.cmad.blog.dto.UserDTO;
import com.cisco.cmad.blog.model.Blog;
import com.cisco.cmad.blog.model.Company;
import com.cisco.cmad.blog.model.Department;
import com.cisco.cmad.blog.model.Site;
import com.cisco.cmad.blog.model.User;
import com.cisco.cmad.blog.util.Utility;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import de.flapdoodle.embed.mongo.MongodProcess;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.json.Json;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;

/**
 * Unit test case to test BlockedDBVertxBlogVerticle
 * 
 * @author abhsinh2
 *
 */
@RunWith(VertxUnitRunner.class)
public class BlockedDBVertxBlogVerticleTest extends VertxBlogVerticleTest {

	private Vertx vertx;
	private static MongodProcess MONGO;

	public BlockedDBVertxBlogVerticleTest() throws Exception {
		super(DB_ACCESS_TYPE.MORPHIC_ORM_API);
	}

	@BeforeClass
	public static void start() throws IOException, InterruptedException {
		MONGO = BlogTest.mongoStart(null);
		Thread.sleep(5000);
	}

	@AfterClass
	public static void shutdown() throws InterruptedException {
		BlogTest.mongoShutdown(MONGO);
		Thread.sleep(5000);
	}

	/**
	 * Deploy our verticle.
	 *
	 * @param context
	 *            the test context.
	 */
	@Before
	public void setUp(TestContext context) throws IOException {
		// blogDAO.clearAll();
		// userDAO.clearAll();
		// sessionDAO.clearAll();

		vertx = Vertx.vertx();

		io.vertx.core.json.JsonObject configJsonObject = new io.vertx.core.json.JsonObject();
		configJsonObject.put("testField1", 123).put("testField2", "hello");

		DeploymentOptions options = new DeploymentOptions().setConfig(configJsonObject);

		BlockedDBVertxBlogVerticle blogVerticle = new BlockedDBVertxBlogVerticle(blogDAO, userDAO, sessionDAO,
				companyDAO);
		vertx.deployVerticle(blogVerticle, options, context.asyncAssertSuccess());
	}

	/**
	 * cleanup everything by closing the vert.x instance
	 *
	 * @param context
	 *            the test context
	 * @throws InterruptedException
	 */
	@After
	public void tearDown(TestContext context) throws InterruptedException {
		blogDAO.clearAll();
		userDAO.clearAll();
		sessionDAO.clearAll();
		companyDAO.clearAll();

		// Thread.sleep(3000);

		if (vertx != null && context != null)
			vertx.close(context.asyncAssertSuccess());
	}

	@Test
	public void testAddNewBlog(TestContext context) {
		Async async = context.async();

		User user = new User();
		user.setUsername("Jerry");
		user.setPassword("welcome");
		user.setFirstname("Jerry");
		user.setLastname("J");
		user.setEmail("jerry@gmail.com");

		userDAO.add(user);

		UserDTO loginDto = new UserDTO();
		loginDto.setUsername("Jerry");
		loginDto.setPassword("welcome");

		String userjson = Json.encodePrettily(loginDto);

		HttpClient client = vertx.createHttpClient();

		client.post(Utility.getHttpPort(), "localhost", "/services/users/login")
				.putHeader(HttpHeaders.CONTENT_TYPE, "application/json")
				.putHeader(HttpHeaders.CONTENT_LENGTH, Integer.toString(userjson.length())).handler(loginresponse -> {
					context.assertEquals(loginresponse.statusCode(), 201);
					context.assertTrue(loginresponse.headers().get(HttpHeaders.CONTENT_TYPE).contains("text/html"));

					String vertx_session_key = "vertx-web.session";
					String sessionId = "";
					for (String cookie : loginresponse.cookies()) {
						if (cookie.startsWith(vertx_session_key)) {
							int len = vertx_session_key.length();
							sessionId = cookie.substring(cookie.indexOf(vertx_session_key) + len + 1,
									cookie.indexOf(";"));
							break;
						}
					}

					BlogDTO blogDto = new BlogDTO();
					blogDto.setAuthor("Jerry");
					blogDto.setTitle("Title");
					blogDto.setContent("Content");

					String blogjson = Json.encodePrettily(blogDto);

					client.post(Utility.getHttpPort(), "localhost", "/services/blogs/newblog")
							.putHeader(HttpHeaders.CONTENT_TYPE, "application/json")
							.putHeader(HttpHeaders.CONTENT_LENGTH, Integer.toString(blogjson.length()))
							.putHeader(HttpHeaders.COOKIE, vertx_session_key + "=" + sessionId)
							.handler(newblogresponse -> {
						context.assertEquals(newblogresponse.statusCode(), 201);
						context.assertTrue(
								newblogresponse.headers().get(HttpHeaders.CONTENT_TYPE).contains("text/plain"));
						newblogresponse.bodyHandler(body -> {
							// context.assertTrue(body.toString().contains("Post
							// Added"));
							async.complete();
						});
					}).write(blogjson).end();
				}).write(userjson).end();
	}

	@Test
	public void testGetAllBlogs(TestContext context) {
		System.out.println("testGetAllBlogs");
		Async async = context.async();

		User user = new User();
		user.setUsername("Jerry");
		user.setPassword("welcome");
		user.setFirstname("Jerry");
		user.setLastname("J");
		user.setEmail("jerry@gmail.com");

		userDAO.add(user);

		Set<String> tags = new HashSet<>();
		tags.add("java");

		Blog blog = new Blog();
		blog.setAuthor("Jerry");
		blog.setTitle("Title1");
		blog.setContent("Content");
		blog.setTags(tags);

		blogDAO.add(blog);

		HttpClient client = vertx.createHttpClient();

		client.getNow(Utility.getHttpPort(), "localhost", "/services/blogs", response -> {
			context.assertEquals(response.statusCode(), 200);
			context.assertTrue(response.headers().get(HttpHeaders.CONTENT_TYPE).contains("application/json"));
			response.handler(body -> {
				JsonElement root = new JsonParser().parse(body.toString());
				com.google.gson.JsonArray rootArray = root.getAsJsonArray();
				com.google.gson.JsonObject jsonObj = rootArray.get(0).getAsJsonObject();
				context.assertTrue(jsonObj.get("title").getAsString().equals(blog.getTitle()));
				async.complete();
			});
		});
	}

	@Test
	public void testGetBlogByUrllink(TestContext context) {
		System.out.println("testGetBlogByUrllink");
		Async async = context.async();

		User user = new User();
		user.setUsername("Jerry");
		user.setPassword("welcome");
		user.setFirstname("Jerry");
		user.setLastname("J");
		user.setEmail("jerry@gmail.com");

		userDAO.add(user);

		Set<String> tags = new HashSet<>();
		tags.add("java");

		Blog blog = new Blog();
		blog.setAuthor("Jerry");
		blog.setTitle("Title1");
		blog.setContent("Content");
		blog.setTags(tags);

		String urllink = blogDAO.add(blog);

		HttpClient client = vertx.createHttpClient();

		client.getNow(Utility.getHttpPort(), "localhost", "/services/blogs/" + urllink, response -> {
			context.assertEquals(response.statusCode(), 200);
			context.assertTrue(response.headers().get(HttpHeaders.CONTENT_TYPE).contains("application/json"));
			response.handler(body -> {
				JsonElement root = new JsonParser().parse(body.toString());
				com.google.gson.JsonObject jsonObj = root.getAsJsonObject();
				context.assertTrue(jsonObj.get("title").getAsString().equals(blog.getTitle()));
				async.complete();
			});
		});

		Async async1 = context.async();
		String dummy = "dummy";
		client.getNow(Utility.getHttpPort(), "localhost", "/services/blogs/" + dummy, response -> {
			context.assertEquals(response.statusCode(), 404);
			async1.complete();
		});
	}

	@Test
	public void testGetBlogsForUser(TestContext context) {
		Async async = context.async();

		User user = new User();
		user.setUsername("Jerry");
		user.setPassword("welcome");
		user.setFirstname("Jerry");
		user.setLastname("J");
		user.setEmail("jerry@gmail.com");

		userDAO.add(user);

		Set<String> tags = new HashSet<>();
		tags.add("java");

		String author = "Jerry";

		Blog blog = new Blog();
		blog.setAuthor(author);
		blog.setTitle("Title1");
		blog.setContent("Content");
		blog.setTags(tags);

		blogDAO.add(blog);

		HttpClient client = vertx.createHttpClient();

		client.get(Utility.getHttpPort(), "localhost", "/services/blogs/user/" + author)
				.putHeader(HttpHeaders.CONTENT_TYPE, "text/html")
				.putHeader(HttpHeaders.CONTENT_LENGTH, Integer.toString(author.length())).handler(response -> {
					context.assertEquals(response.statusCode(), 200);
					context.assertTrue(response.headers().get(HttpHeaders.CONTENT_TYPE).contains("application/json"));
					response.bodyHandler(body -> {
						JsonElement root = new JsonParser().parse(body.toString());
						com.google.gson.JsonArray rootArray = root.getAsJsonArray();
						com.google.gson.JsonObject jsonObj = rootArray.get(0).getAsJsonObject();
						context.assertTrue(jsonObj.get("title").getAsString().equals(blog.getTitle()));
						async.complete();
					});
				}).write(author).end();

		String dummy = "dummy";

		client.get(Utility.getHttpPort(), "localhost", "/services/blogs/user/" + dummy)
				.putHeader(HttpHeaders.CONTENT_TYPE, "text/html")
				.putHeader(HttpHeaders.CONTENT_LENGTH, Integer.toString(dummy.length())).handler(response -> {
					context.assertEquals(response.statusCode(), 404);
				}).write(dummy).end();
	}

	@Test
	public void testGetBlogsForTag(TestContext context) {
		Async async = context.async();

		User user = new User();
		user.setUsername("Jerry");
		user.setPassword("welcome");
		user.setFirstname("Jerry");
		user.setLastname("J");
		user.setEmail("jerry@gmail.com");

		userDAO.add(user);

		String tag = "java";

		Set<String> tags = new HashSet<>();
		tags.add(tag);

		Blog blog = new Blog();
		blog.setAuthor("Jerry");
		blog.setTitle("Title1");
		blog.setContent("Content");
		blog.setTags(tags);

		blogDAO.add(blog);

		HttpClient client = vertx.createHttpClient();

		client.get(Utility.getHttpPort(), "localhost", "/services/blogs/tag/" + tag)
				.putHeader(HttpHeaders.CONTENT_TYPE, "text/html")
				.putHeader(HttpHeaders.CONTENT_LENGTH, Integer.toString(tag.length())).handler(response -> {
					context.assertEquals(response.statusCode(), 200);
					context.assertTrue(response.headers().get(HttpHeaders.CONTENT_TYPE).contains("application/json"));
					response.bodyHandler(body -> {
						JsonElement root = new JsonParser().parse(body.toString());
						com.google.gson.JsonArray rootArray = root.getAsJsonArray();
						com.google.gson.JsonObject jsonObj = rootArray.get(0).getAsJsonObject();
						context.assertTrue(jsonObj.get("title").getAsString().equals(blog.getTitle()));
						async.complete();
					});
				}).write(tag).end();

		String dummy = "dummy";

		client.get(Utility.getHttpPort(), "localhost", "/services/blogs/tag/" + dummy)
				.putHeader(HttpHeaders.CONTENT_TYPE, "text/html")
				.putHeader(HttpHeaders.CONTENT_LENGTH, Integer.toString(dummy.length())).handler(response -> {
					context.assertEquals(response.statusCode(), 404);
				}).write(dummy).end();
	}

	@Test
	public void testDeleteBlog(TestContext context) {
		Async async = context.async();
		// TODO
		async.complete();
	}

	@Test
	public void testAddNewComment(TestContext context) {
		Async async = context.async();

		User user = new User();
		user.setUsername("Jerry");
		user.setPassword("welcome");
		user.setFirstname("Jerry");
		user.setLastname("J");
		user.setEmail("jerry@gmail.com");

		userDAO.add(user);

		Blog blog = new Blog();
		blog.setAuthor("Jerry");
		blog.setTitle("Title1");
		blog.setContent("Content");

		String urllink = blogDAO.add(blog);

		CommentDTO commentDto = new CommentDTO();
		commentDto.setAuthor("Jerry");
		commentDto.setContent("Content");
		commentDto.setEmail("some@email.com");
		commentDto.setDate(new Date());

		final String json = Json.encodePrettily(commentDto);

		HttpClient client = vertx.createHttpClient();

		client.post(Utility.getHttpPort(), "localhost", "/services/blogs/" + urllink + "/newcomment")
				.putHeader(HttpHeaders.CONTENT_TYPE, "application/json")
				.putHeader(HttpHeaders.CONTENT_LENGTH, Integer.toString(json.length())).handler(response -> {
					// TOD0: because user not logged in
					context.assertEquals(response.statusCode(), 401);
					context.assertTrue(response.headers().get(HttpHeaders.CONTENT_TYPE).contains("text/plain"));
					response.bodyHandler(body -> {
						// context.assertNotNull(body.toString().equals("Comment
						// added"));
						async.complete();
					});
				}).write(json).end();
	}

	@Test
	public void testAddNewUser(TestContext context) {
		Async async = context.async();

		UserDTO userDto = new UserDTO();
		userDto.setUsername("Jerry");
		userDto.setPassword("welcome");
		userDto.setFirstname("Jerry");
		userDto.setLastname("J");
		userDto.setEmail("jerry@gmail.com");

		final String json = Json.encodePrettily(userDto);

		HttpClient client = vertx.createHttpClient();

		client.post(Utility.getHttpPort(), "localhost", "/services/users/signup")
				.putHeader(HttpHeaders.CONTENT_TYPE, "application/json")
				.putHeader(HttpHeaders.CONTENT_LENGTH, Integer.toString(json.length())).handler(response -> {
					context.assertEquals(response.statusCode(), 201);
					context.assertTrue(response.headers().get(HttpHeaders.CONTENT_TYPE).contains("text/plain"));
					response.bodyHandler(body -> {
						context.assertTrue(body.toString().contains("User Added"));
						async.complete();
					});
				}).write(json).end();
	}

	@Test
	public void testLogin(TestContext context) {
		Async async = context.async();

		UserDTO userDto = new UserDTO();
		userDto.setUsername("Jerry");
		userDto.setPassword("welcome");
		userDto.setFirstname("Jerry");
		userDto.setLastname("J");
		userDto.setEmail("jerry@gmail.com");

		userDAO.add(userDto.toModel());

		UserDTO loginDto = new UserDTO();
		loginDto.setUsername("Jerry");
		loginDto.setPassword("welcome");

		String json = Json.encodePrettily(loginDto);

		HttpClient client = vertx.createHttpClient();

		// Test valid login
		client.post(Utility.getHttpPort(), "localhost", "/services/users/login")
				.putHeader(HttpHeaders.CONTENT_TYPE, "application/json")
				.putHeader(HttpHeaders.CONTENT_LENGTH, Integer.toString(json.length())).handler(response -> {
					context.assertEquals(response.statusCode(), 201);
					context.assertTrue(response.headers().get(HttpHeaders.CONTENT_TYPE).contains("text/html"));
					response.bodyHandler(body -> {
						context.assertTrue(body.toString().contains("Valid Login"));
						async.complete();
					});
				}).write(json).end();

		// Test invalid login
		loginDto = new UserDTO();
		loginDto.setUsername("Jerry");
		loginDto.setPassword("welcome123");
		json = Json.encodePrettily(loginDto);

		client.post(Utility.getHttpPort(), "localhost", "/services/users/login")
				.putHeader(HttpHeaders.CONTENT_TYPE, "application/json")
				.putHeader(HttpHeaders.CONTENT_LENGTH, Integer.toString(json.length())).handler(response -> {
					context.assertEquals(response.statusCode(), 401);
					context.assertTrue(response.headers().get(HttpHeaders.CONTENT_TYPE).contains("text/html"));
					response.bodyHandler(body -> {
						// context.assertTrue(body.toString().contains("Invalid
						// Credentials"));
						async.complete();
					});
				}).write(json).end();
	}

	@Test
	public void testLogout(TestContext context) {
		Async async = context.async();
		// TODO
		async.complete();
	}

	@Test
	public void testBlogNotFound(TestContext context) {
		Async async = context.async();
		// TODO
		async.complete();
	}

	@Test
	public void testInternalServerError(TestContext context) {
		Async async = context.async();
		// TODO
		async.complete();
	}

	@Test
	public void testGetAllSignedInUsers(TestContext context) {
		Async async = context.async();
		// TODO
		async.complete();
	}

	@Test
	public void testGetCompanies(TestContext context) {
		System.out.println("testGetCompanies");
		Async async = context.async();

		Company company = new Company();
		company.setName("Cisco");

		Site site = new Site();
		site.setName("India");

		Department dept = new Department();
		dept.setName("CSG");

		site.addDepartment(dept);
		company.addSite(site);

		companyDAO.addCompany(company);

		HttpClient client = vertx.createHttpClient();

		client.getNow(Utility.getHttpPort(), "localhost", "/services/companies", response -> {
			context.assertEquals(response.statusCode(), 200);
			context.assertTrue(response.headers().get(HttpHeaders.CONTENT_TYPE).contains("application/json"));
			response.handler(body -> {
				JsonElement root = new JsonParser().parse(body.toString());
				com.google.gson.JsonArray rootArray = root.getAsJsonArray();
				com.google.gson.JsonObject jsonObj = rootArray.get(0).getAsJsonObject();
				context.assertTrue(jsonObj.get("companyName").getAsString().equals(company.getName()));
				async.complete();
			});
		});
	}

	@Test
	public void testGetSites(TestContext context) {
		System.out.println("testGetSites");
		Async async = context.async();

		Company company = new Company();
		company.setName("Cisco");

		Site site = new Site();
		site.setName("India");

		Department dept = new Department();
		dept.setName("CSG");

		site.addDepartment(dept);
		company.addSite(site);

		ObjectId companyId = companyDAO.addCompany(company);

		System.out.println("companyId:" + companyId);

		HttpClient client = vertx.createHttpClient();

		client.getNow(Utility.getHttpPort(), "localhost", "/services/companies/" + companyId.toHexString() + "/sites",
				response -> {
					context.assertEquals(response.statusCode(), 200);
					context.assertTrue(response.headers().get(HttpHeaders.CONTENT_TYPE).contains("application/json"));
					response.handler(body -> {
						System.out.println("body:" + body.toString());
						JsonElement root = new JsonParser().parse(body.toString());
						com.google.gson.JsonArray rootArray = root.getAsJsonArray();
						com.google.gson.JsonObject jsonObj = rootArray.get(0).getAsJsonObject();
						context.assertTrue(jsonObj.get("siteName").getAsString().equals(site.getName()));
						async.complete();
					});
				});
	}

	@Test
	public void testGetDepartments(TestContext context) {
		System.out.println("testGetDepartments");
		Async async = context.async();

		Company company = new Company();
		company.setName("Cisco");

		Site site = new Site();
		site.setName("India");

		Department dept = new Department();
		dept.setName("CSG");

		site.addDepartment(dept);
		company.addSite(site);

		ObjectId companyId = companyDAO.addCompany(company);

		HttpClient client = vertx.createHttpClient();

		client.getNow(Utility.getHttpPort(), "localhost",
				"/services/companies/" + companyId.toHexString() + "/sites/" + site.getId() + "/departments",
				response -> {
					context.assertEquals(response.statusCode(), 200);
					context.assertTrue(response.headers().get(HttpHeaders.CONTENT_TYPE).contains("application/json"));
					response.handler(body -> {
						JsonElement root = new JsonParser().parse(body.toString());
						com.google.gson.JsonArray rootArray = root.getAsJsonArray();
						com.google.gson.JsonObject jsonObj = rootArray.get(0).getAsJsonObject();
						context.assertTrue(jsonObj.get("deptName").getAsString().equals(dept.getName()));
						async.complete();
					});
				});
	}

	@Test
	public void testAddCompany(TestContext context) {
		System.out.println("testAddCompany");
		Async async = context.async();

		CompanyDTO companyDto = new CompanyDTO();
		companyDto.setCompanyName("Cisco");

		final String json = Json.encodePrettily(companyDto);

		HttpClient client = vertx.createHttpClient();

		client.post(Utility.getHttpPort(), "localhost", "/services/companies")
				.putHeader(HttpHeaders.CONTENT_TYPE, "application/json")
				.putHeader(HttpHeaders.CONTENT_LENGTH, Integer.toString(json.length())).handler(response -> {
					context.assertEquals(response.statusCode(), 201);
					context.assertTrue(response.headers().get(HttpHeaders.CONTENT_TYPE).contains("text/plain"));
					response.bodyHandler(body -> {
						context.assertNotNull(body.toString());
						async.complete();
					});
				}).write(json).end();
	}

	@Test
	public void testAddSite(TestContext context) {
		System.out.println("testAddCompany");
		Async async = context.async();

		Company company = new Company();
		company.setName("Cisco");

		ObjectId companyId = companyDAO.addCompany(company);

		SiteDTO siteDto = new SiteDTO();
		siteDto.setSiteName("India");

		final String json = Json.encodePrettily(siteDto);

		HttpClient client = vertx.createHttpClient();

		client.post(Utility.getHttpPort(), "localhost", "/services/companies/" + companyId.toHexString() + "/sites")
				.putHeader(HttpHeaders.CONTENT_TYPE, "application/json")
				.putHeader(HttpHeaders.CONTENT_LENGTH, Integer.toString(json.length())).handler(response -> {
					context.assertEquals(response.statusCode(), 201);
					context.assertTrue(response.headers().get(HttpHeaders.CONTENT_TYPE).contains("text/plain"));
					response.bodyHandler(body -> {
						context.assertNotNull(body.toString());
						async.complete();
					});
				}).write(json).end();
	}

	@Test
	public void testAddDepartment(TestContext context) {
		System.out.println("testAddDepartment");
		Async async = context.async();

		Company company = new Company();
		company.setName("Cisco");

		Site site = new Site();
		site.setName("India");
		site.setId(Site.getUniqueId());

		company.addSite(site);

		ObjectId companyId = companyDAO.addCompany(company);

		// Now test
		DepartmentDTO deptDto = new DepartmentDTO();
		deptDto.setDeptName("CSG");

		final String json = Json.encodePrettily(deptDto);

		HttpClient client = vertx.createHttpClient();

		client.post(Utility.getHttpPort(), "localhost",
				"/services/companies/" + companyId.toHexString() + "/sites/" + site.getId() + "/departments")
				.putHeader(HttpHeaders.CONTENT_TYPE, "application/json")
				.putHeader(HttpHeaders.CONTENT_LENGTH, Integer.toString(json.length())).handler(response -> {
					context.assertEquals(response.statusCode(), 201);
					context.assertTrue(response.headers().get(HttpHeaders.CONTENT_TYPE).contains("text/plain"));
					response.bodyHandler(body -> {
						context.assertNotNull(body.toString());
						async.complete();
					});
				}).write(json).end();
	}
}
