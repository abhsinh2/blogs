package com.cisco.cmad.blog.dao.mongo;

import com.cisco.cmad.blog.BlogTest;
import com.cisco.cmad.blog.mongo.ServicesFactory;
import com.google.gson.JsonObject;
import com.mongodb.client.MongoDatabase;

/**
 * Base test class for mongo native api based testing.
 * 
 * @author abhsinh2
 *
 */
public abstract class MongoDAOTest extends BlogTest {

	protected MongoDatabase mongoDatabase;	
	protected JsonObject rootJsonObject;
	
	public MongoDAOTest() {
		rootJsonObject = readConfiguration();
		mongoDatabase = ServicesFactory.getMongoDatabase();	
	}	
}
