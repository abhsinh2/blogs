package com.cisco.cmad.blog.dao.mongo.orm;

import org.mongodb.morphia.Datastore;

import com.cisco.cmad.blog.BlogTest;
import com.cisco.cmad.blog.mongo.ServicesFactory;
import com.google.gson.JsonObject;

/**
 * Base test class to test morphic based api.
 * 
 * @author abhsinh2
 *
 */
public abstract class MongoDAOTest extends BlogTest {

	protected Datastore dataStore;
	protected JsonObject rootJsonObject;
	
	public MongoDAOTest() {		
		rootJsonObject = readConfiguration();
		dataStore = ServicesFactory.getMongoMorphiaDatastore();
	}
}
