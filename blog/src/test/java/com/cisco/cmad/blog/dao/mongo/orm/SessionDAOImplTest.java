package com.cisco.cmad.blog.dao.mongo.orm;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.cisco.cmad.blog.model.Session;

import de.flapdoodle.embed.mongo.MongodProcess;

/**
 * Test class to test morphic based api for SessionDAO
 * 
 * @author abhsinh2
 *
 */
public class SessionDAOImplTest extends MongoDAOTest {

	private SessionDAOImpl sessionDAO;
	protected static MongodProcess MONGO;
	
	public SessionDAOImplTest() {
		super();
		sessionDAO = new SessionDAOImpl(dataStore);
	}

	@BeforeClass
	public static void beforeClass() throws IOException, InterruptedException {
		MONGO = MongoDAOTest.mongoStart(null);
		Thread.sleep(5000);
	}

	@AfterClass
	public static void afterClass() throws InterruptedException {
		MongoDAOTest.mongoShutdown(MONGO);
		Thread.sleep(5000);
	}
	
	@Before
	public void beforeTest() {
		
	}
	
	@After
	public void afterTest() {
		
	}
	
	@Test
	public void testFindUserNameBySessionId() {
		String sessionId = sessionDAO.startSession("Jerry");
		String username = sessionDAO.findUserNameBySessionId(sessionId);
		
		assertEquals("Jerry", username);
	}
	
	@Test
	public void testStartSession() {
		String sessionId = sessionDAO.startSession("Jerry");
		assertNotNull(sessionId);
	}
	
	@Test
	public void testEndSession() {
		String sessionId = sessionDAO.startSession("Jerry");
		assertNotNull(sessionId);
		
		sessionDAO.endSession(sessionId);
		Session session = sessionDAO.getSession(sessionId);
		
		assertNull(session);
	}
	
	@Test
	public void testGetSession() {
		String sessionId = sessionDAO.startSession("Jerry");
		assertNotNull(sessionId);
		
		Session session = sessionDAO.getSession(sessionId);
		assertNotNull(session);
		
		assertEquals(sessionId, session.getSessionId());
	}
	
	@Test
	public void testClearAll() {
		sessionDAO.clearAll();
		
		String sessionId = sessionDAO.startSession("Jerry");
		assertNotNull(sessionId);
		
		Session session = sessionDAO.getSession(sessionId);
		assertNotNull(session);
		
		sessionDAO.clearAll();
		
		session = sessionDAO.getSession(sessionId);
		assertNull(session);
	}
	
	@Test
	public void testGetAllSignedInUsernames() {
		sessionDAO.clearAll();
		
		String sessionId1 = sessionDAO.startSession("abc1");
		assertNotNull(sessionId1);
		
		String sessionId2 = sessionDAO.startSession("abc2");
		assertNotNull(sessionId2);
		
		List<String> usernames = sessionDAO.getAllSignedInUsernames();		
		assertTrue(usernames.size() == 2);
		
		sessionDAO.endSession(sessionId1);
		
		usernames = sessionDAO.getAllSignedInUsernames();		
		assertTrue(usernames.size() == 1);
	}

}
