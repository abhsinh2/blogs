package com.cisco.cmad.blog.verticle;

import org.mongodb.morphia.Datastore;

import com.cisco.cmad.blog.BlogTest;
import com.cisco.cmad.blog.dao.BlogDAO;
import com.cisco.cmad.blog.dao.CompanyDAO;
import com.cisco.cmad.blog.dao.SessionDAO;
import com.cisco.cmad.blog.dao.UserDAO;
import com.cisco.cmad.blog.mongo.ServicesFactory;
import com.google.gson.JsonObject;

public abstract class VertxBlogVerticleTest extends BlogTest {
	
	public static enum DB_ACCESS_TYPE {
		MONGO_NATIVE_API, MORPHIC_ORM_API, VERTX_MONGO_API
	}
	
	protected BlogDAO blogDAO;
	protected UserDAO userDAO;
	protected SessionDAO sessionDAO;	
	protected CompanyDAO companyDAO;
	
	protected JsonObject rootJsonObject;

	public VertxBlogVerticleTest(DB_ACCESS_TYPE dbAccessType) {
		rootJsonObject = readConfiguration();
		
		if (dbAccessType == DB_ACCESS_TYPE.MONGO_NATIVE_API) {
			com.mongodb.client.MongoDatabase mongoDatabase = ServicesFactory.getMongoDatabase();
			blogDAO = new com.cisco.cmad.blog.dao.mongo.BlogDAOImpl(mongoDatabase);
			userDAO = new com.cisco.cmad.blog.dao.mongo.UserDAOImpl(mongoDatabase);
			sessionDAO = new com.cisco.cmad.blog.dao.mongo.SessionDAOImpl(mongoDatabase);
			companyDAO = new com.cisco.cmad.blog.dao.mongo.CompanyDAOImpl(mongoDatabase);
		} else if (dbAccessType == DB_ACCESS_TYPE.MORPHIC_ORM_API) {
			Datastore datastore = ServicesFactory.getMongoMorphiaDatastore();
			blogDAO = new com.cisco.cmad.blog.dao.mongo.orm.BlogDAOImpl(datastore);
			userDAO = new com.cisco.cmad.blog.dao.mongo.orm.UserDAOImpl(datastore);
			sessionDAO = new com.cisco.cmad.blog.dao.mongo.orm.SessionDAOImpl(datastore);
			companyDAO = new com.cisco.cmad.blog.dao.mongo.orm.CompanyDAOImpl(datastore);
		}
	}
}
