package com.cisco.cmad.blog.dao.mongo;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.*;

import org.bson.types.ObjectId;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.cisco.cmad.blog.model.Company;
import com.cisco.cmad.blog.model.Department;
import com.cisco.cmad.blog.model.Site;

import de.flapdoodle.embed.mongo.MongodProcess;

/**
 * Test class to test mongo native api based testing for CompanyDAO
 * 
 * @author abhsinh2
 *
 */
public class CompanyDAOImplTest extends MongoDAOTest {
	private CompanyDAOImpl companyDAO;
	protected static MongodProcess MONGO;

	public CompanyDAOImplTest() {
		super();
		companyDAO = new CompanyDAOImpl(mongoDatabase);
	}

	@BeforeClass
	public static void beforeClass() throws IOException, InterruptedException {
		MONGO = MongoDAOTest.mongoStart(null);
		Thread.sleep(5000);
	}

	@AfterClass
	public static void afterClass() throws InterruptedException {
		MongoDAOTest.mongoShutdown(MONGO);
		Thread.sleep(5000);
	}

	@Before
	public void beforeTest() {

	}

	@After
	public void afterTest() {

	}

	@Test
	public void testAddCompany() {
		companyDAO.clearAll();
		assertTrue(companyDAO.getCompanies().size() == 0);

		Company company = new Company();
		company.setName("Cisco");
		ObjectId companyId = companyDAO.addCompany(company);
		assertNotNull(companyId);
		assertTrue(companyDAO.getCompanies().size() == 1);
		
		Company newCompany = new Company();
		newCompany.setName(company.getName());
		ObjectId newCompanyId = companyDAO.addCompany(newCompany);
		assertEquals(newCompanyId, companyId);
		assertTrue(companyDAO.getCompanies().size() == 1);
	}

	@Test
	public void testAddSite() {
		companyDAO.clearAll();
		assertTrue(companyDAO.getCompanies().size() == 0);

		Company company = new Company();
		company.setName("Cisco");

		ObjectId companyId = companyDAO.addCompany(company);
		assertNotNull(companyId);
		
		List<Company> companies = companyDAO.getCompanies();
		assertTrue(companies.size() == 1);

		Company resCompany = companies.get(0);

		Site site = new Site();
		site.setName("NewSite");
		Integer siteId = companyDAO.addSite(companyId, site);
		assertNotNull(siteId);
		
		List<Site> sites = companyDAO.getSites(companyId);
		assertTrue(sites.size() == 1);
		
		Site newSite = new Site();
		newSite.setName(site.getName());
		Integer newSiteId = companyDAO.addSite(resCompany.get_id(), newSite);
		assertEquals(newSiteId, siteId);
		
		sites = companyDAO.getSites(companyId);
		assertTrue(sites.size() == 1);
	}

	@Test
	public void testAddDepartment() {
		companyDAO.clearAll();
		assertTrue(companyDAO.getCompanies().size() == 0);

		Company company = new Company();
		company.setName("Cisco");

		ObjectId companyId = companyDAO.addCompany(company);
		assertNotNull(companyId);
		
		List<Company> companies = companyDAO.getCompanies();
		assertTrue(companies.size() == 1);

		Site site = new Site();
		site.setName("NewSite");
		Integer siteId = companyDAO.addSite(companyId, site);
		assertNotNull(siteId);
		
		List<Site> sites = companyDAO.getSites(companyId);
		assertTrue(sites.size() == 1);
		
		Department dept = new Department();
		dept.setName("NewDept");
		Integer deptId = companyDAO.addDepartment(companyId, siteId, dept);
		assertNotNull(deptId);
		
		List<Department> depts = companyDAO.getDepartments(companyId, siteId);
		assertTrue(depts.size() == 1);
		
		Department newDept = new Department();
		newDept.setName(dept.getName());
		Integer newDeptId = companyDAO.addDepartment(companyId, siteId, newDept);
		assertEquals(deptId, newDeptId);
		
		depts = companyDAO.getDepartments(companyId, siteId);
		assertTrue(depts.size() == 1);
	}

	@Test
	public void testGetCompanies() {
		companyDAO.clearAll();
		assertTrue(companyDAO.getCompanies().size() == 0);

		Company company = new Company();
		company.setName("Cisco");

		companyDAO.addCompany(company);

		assertTrue(companyDAO.getCompanies().size() == 1);
	}
	
	@Test
        public void testGetCompany() {
                companyDAO.clearAll();
                assertTrue(companyDAO.getCompanies().size() == 0);

                Company company = new Company();
                company.setName("Cisco");

                ObjectId companyId = companyDAO.addCompany(company);

                assertTrue(companyDAO.getCompany(companyId).getName().equals("Cisco"));
        }

	@Test
	public void testGetSites() {
		companyDAO.clearAll();
		assertTrue(companyDAO.getCompanies().size() == 0);

		Company company = new Company();
		company.setName("Cisco");

		companyDAO.addCompany(company);

		List<Company> companies = companyDAO.getCompanies();
		assertTrue(companies.size() == 1);

		Company resCompany = companies.get(0);

		Site site = new Site();
		site.setName("New Site");
		companyDAO.addSite(resCompany.get_id(), site);

		List<Site> sites = companyDAO.getSites(resCompany.get_id());
		assertTrue(sites.size() == 1);
	}

	@Test
	public void testGetDepartments() {
		companyDAO.clearAll();
		assertTrue(companyDAO.getCompanies().size() == 0);

		Company company = new Company();
		company.setName("Cisco");

		companyDAO.addCompany(company);

		List<Company> companies = companyDAO.getCompanies();
		assertTrue(companies.size() == 1);

		Company resCompany = companies.get(0);

		Site site = new Site();
		site.setName("New Site");
		Integer siteId = companyDAO.addSite(resCompany.get_id(), site);

		Department dept = new Department();
		dept.setName("NewDept");
		companyDAO.addDepartment(resCompany.get_id(), siteId, dept);

		List<Department> depts = companyDAO.getDepartments(resCompany.get_id(), siteId);
		assertTrue(depts.size() == 1);
	}

	@Test
	public void testClearAll() {
		companyDAO.clearAll();
		assertTrue(companyDAO.getCompanies().size() == 0);

		Company company = new Company();
		company.setName("Cisco");

		companyDAO.addCompany(company);

		assertTrue(companyDAO.getCompanies().size() == 1);

		companyDAO.clearAll();

		assertTrue(companyDAO.getCompanies().size() == 0);
	}
}
