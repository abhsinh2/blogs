package com.cisco.cmad.blog.dao.mongo.orm;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.cisco.cmad.blog.model.Comment;

import de.flapdoodle.embed.mongo.MongodProcess;

import com.cisco.cmad.blog.model.Blog;

/**
 * Test class to test morphic based api for BlogDAO
 * 
 * @author abhsinh2
 *
 */
public class BlogDAOImplTest extends MongoDAOTest {

	private BlogDAOImpl blogDAO;
	protected static MongodProcess MONGO;

	public BlogDAOImplTest() {
		super();
		blogDAO = new BlogDAOImpl(dataStore);
	}

	@BeforeClass
	public static void beforeClass() throws IOException, InterruptedException {
		MONGO = MongoDAOTest.mongoStart(null);
		Thread.sleep(5000);
	}

	@AfterClass
	public static void afterClass() throws InterruptedException {
		MongoDAOTest.mongoShutdown(MONGO);
		Thread.sleep(5000);
	}

	@Before
	public void beforeTest() {

	}

	@After
	public void afterTest() {
	}

	@Test
	public void testFindByUrllink() {
		Blog resultSimplePost = blogDAO.findByUrlLink("dummy_link");
		assertNull(resultSimplePost);

		String urllink = blogDAO.add(new Blog("simple post1", "my first post1 body", "Jerry"));
		resultSimplePost = blogDAO.findByUrlLink(urllink);
		assertNotNull(resultSimplePost);
	}

	@Test
	public void testFindAllPosts() {
		blogDAO.clearAll();

		blogDAO.add(new Blog("simple post1", "my first post1 body", "Jerry"));
		blogDAO.add(new Blog("simple post2", "my first post2 body", "Jerry"));
		blogDAO.add(new Blog("simple post3", "my first post3 body", "Jerry"));

		List<Blog> posts = blogDAO.find(2, true);
		assertTrue(posts.size() == 2);

		posts = blogDAO.find(2, false);
		assertTrue(posts.size() == 2);
	}

	@Test
	public void testFindAllPostsByTag() {
		Set<String> tags = new HashSet<>();
		tags.add("java");
		tags.add("mongo");
		blogDAO.add(new Blog("tag post", "my first post body", "Jerry", tags));

		List<Blog> posts = blogDAO.findByTag("java", null, Boolean.TRUE);
		assertTrue(posts.size() == 1);

		posts = blogDAO.findByTag("vertx", null, Boolean.TRUE);
		assertTrue(posts.size() == 0);
	}

	@Test
	public void testFindByUsername() {
		List<Blog> resultBlogs = blogDAO.findByAuthor("dummy", null, Boolean.TRUE);
		assertTrue(resultBlogs.size() == 0);

		blogDAO.add(new Blog("simple post1", "my first post1 body", "Jerry"));
		blogDAO.add(new Blog("simple post2", "my first post2 body", "Jerry"));

		resultBlogs = blogDAO.findByAuthor("Jerry", null, Boolean.TRUE);
		assertTrue(resultBlogs.size() == 2);
	}

	@Test
	public void testAddBlogObject() {
		Blog simplePost = new Blog();
		simplePost.setAuthor("Jerry");
		simplePost.setContent("my first post body");
		simplePost.setTitle("simple post");

		Blog postWithTag = new Blog();
		postWithTag.setAuthor("Jerry");
		postWithTag.setContent("my first post body");
		postWithTag.setTitle("tag post");
		Set<String> tags = new HashSet<>();
		tags.add("java");
		postWithTag.setTags(tags);

		String simplePostLink = blogDAO.add(simplePost);
		String postWithTagLink = blogDAO.add(postWithTag);

		// Verification
		Blog resultSimplePost = blogDAO.findByUrlLink(simplePostLink);
		Blog resultPostWithTag = blogDAO.findByUrlLink(postWithTagLink);

		assertEquals(simplePost.getTitle(), resultSimplePost.getTitle());
		assertTrue(resultSimplePost.getTags().isEmpty());

		assertEquals(postWithTag.getTitle(), resultPostWithTag.getTitle());
		assertTrue(resultPostWithTag.getTags().size() == 1);
	}

	@Test
	public void testAddBlog() {
		String simplePostLink = blogDAO.add(new Blog("simple post", "my first post body", "Jerry"));
		Set<String> tags = new HashSet<>();
		tags.add("java");
		String postWithTagLink = blogDAO.add(new Blog("tag post", "my first post body", "Jerry", tags));

		// Verification
		Blog resultSimplePost = blogDAO.findByUrlLink(simplePostLink);
		Blog resultPostWithTag = blogDAO.findByUrlLink(postWithTagLink);

		assertEquals("simple post", resultSimplePost.getTitle());
		assertTrue(resultSimplePost.getTags().isEmpty());

		assertEquals("tag post", resultPostWithTag.getTitle());
		assertTrue(resultPostWithTag.getTags().size() == 1);
	}

	@Test
	public void testAddCommentObject() {
		Comment comment = new Comment();
		comment.setAuthor("Tom");
		comment.setContent("Nice Post");
		comment.setEmail("tom@gmail.com");

		String simplePostLink = blogDAO.add(new Blog("simple post", "my first post body", "Jerry"));

		// Verify if comment size is 0
		Blog resultSimplePost = blogDAO.findByUrlLink(simplePostLink);
		assertEquals("simple post", resultSimplePost.getTitle());
		assertTrue(resultSimplePost.getTags().isEmpty());
		assertTrue(resultSimplePost.getComments().isEmpty());

		// Add a comment
		blogDAO.addComment(comment, simplePostLink);

		// Verify if comment size is 1
		resultSimplePost = blogDAO.findByUrlLink(simplePostLink);
		assertEquals("simple post", resultSimplePost.getTitle());
		assertTrue(resultSimplePost.getTags().isEmpty());
		assertTrue(resultSimplePost.getComments().size() == 1);
	}

	@Test
	public void testAddComment() {
		String simplePostLink = blogDAO.add(new Blog("simple post", "my first post body", "Jerry"));

		// Verify if comment size is 0
		Blog resultSimplePost = blogDAO.findByUrlLink(simplePostLink);
		assertEquals("simple post", resultSimplePost.getTitle());
		assertTrue(resultSimplePost.getTags().isEmpty());
		assertTrue(resultSimplePost.getComments().isEmpty());

		// Add a comment
		blogDAO.addComment(new Comment("Tom", "tom@gmail.com", "Nice Post"), simplePostLink);

		// Verify if comment size is 1
		resultSimplePost = blogDAO.findByUrlLink(simplePostLink);
		assertEquals("simple post", resultSimplePost.getTitle());
		assertTrue(resultSimplePost.getTags().isEmpty());
		assertTrue(resultSimplePost.getComments().size() == 1);
	}

	@Test
	public void testClearAll() {
		blogDAO.clearAll();

		blogDAO.add(new Blog("simple post", "my first post body", "Jerry"));
		List<Blog> posts = blogDAO.find(-1, true);
		assertTrue(posts.size() == 1);

		blogDAO.clearAll();
		posts = blogDAO.find(-1, true);
		assertTrue(posts.size() == 0);
	}

}
