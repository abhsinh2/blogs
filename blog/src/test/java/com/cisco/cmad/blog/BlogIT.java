package com.cisco.cmad.blog;

import static com.jayway.restassured.RestAssured.delete;
import static com.jayway.restassured.RestAssured.get;
import static com.jayway.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.equalTo;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonObject;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.parsing.Parser;

/**
 * Integration Test for Blog application. Unlike unit test, integration test
 * ends with IT. It’s a convention from the Failsafe plugin to distinguish unit
 * (starting or ending with Test) from integration tests (starting or ending
 * with IT)
 * 
 * @author abhsinh2
 *
 */
public class BlogIT extends BlogTest {

	public BlogIT() throws Exception {

	}

	@BeforeClass
	public static void configureRestAssured() {
		JsonObject jsonObj = BlogTest.readITConfigurationFile();
		RestAssured.baseURI = "http://localhost";
		RestAssured.port = jsonObj.get("http.port").getAsInt();
		RestAssured.defaultParser = Parser.TEXT;
		
		System.out.println("RestAssured.port:" + RestAssured.port);
	}

	@AfterClass
	public static void unconfigureRestAssured() {
		RestAssured.reset();
	}

	@Test
	public void checkWeCanAddAndRetrievePost() {
		System.out.println("Running checkWeCanAddAndRetrievePost");
		
		given().body("{\"title\":\"Title1\", \"content\":\"Content1\", \"author\":\"jerry\"}")
				.request().post("/services/blogs/newblog").thenReturn().contentType().equals("text/plain");
		//assertThat(urllink).isEqualToIgnoringCase("title1");

		/*
		get("/services/blogs/" + urllink).then().assertThat().statusCode(200).body("title", equalTo("Title1"))
				.body("content", equalTo("Content1")).body("author", equalTo("jerry"));

		delete("/services/blogs/" + urllink).then().assertThat().statusCode(204);

		get("/services/posts/" + urllink).then().assertThat().statusCode(404);
		*/
	}

}
