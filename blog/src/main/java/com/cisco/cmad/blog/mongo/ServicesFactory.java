package com.cisco.cmad.blog.mongo;

import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;

import com.cisco.cmad.blog.util.Utility;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoDatabase;

/**
 * Mongo Service Factory
 * 
 * @author abhsinh2
 *
 */
public class ServicesFactory {

	private static ThreadLocal<Datastore> mongoMorphiaDatastoreTL = new ThreadLocal<Datastore>();
	private static ThreadLocal<MongoDatabase> mongoDatabaseTL = new ThreadLocal<MongoDatabase>();

	/**
	 * @return Morphic Datastore from the thread local
	 */
	public static Datastore getMongoMorphiaDatastore() {
		if (mongoMorphiaDatastoreTL.get() == null) {
			MongoClientURI connectionString = new MongoClientURI(Utility.getMongoURIString());
			MongoClient mongoClient = new MongoClient(connectionString);

			Datastore datastore = null;
			Morphia morphia = new Morphia();
			morphia.mapPackage("com.cisco.cmad.blog.model");
			datastore = morphia.createDatastore(mongoClient, Utility.getMongoDatabase());

			datastore.ensureIndexes();
			mongoMorphiaDatastoreTL.set(datastore);
			return datastore;
		}
		return mongoMorphiaDatastoreTL.get();
	}

	/**
	 * @return MongoDatabase from the thread local
	 */
	public static MongoDatabase getMongoDatabase() {
		if (mongoDatabaseTL.get() == null) {
			MongoClientURI connectionString = new MongoClientURI(Utility.getMongoURIString());
			MongoClient mongoClient = new MongoClient(connectionString);
			MongoDatabase blogDatabase = mongoClient.getDatabase(Utility.getMongoDatabase());

			
			mongoDatabaseTL.set(blogDatabase);
			
			return blogDatabase;
		}
		return mongoDatabaseTL.get();
	}
}