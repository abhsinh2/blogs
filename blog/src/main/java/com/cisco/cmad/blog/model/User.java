package com.cisco.cmad.blog.model;

import org.bson.Document;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

import com.cisco.cmad.blog.util.Utility;
import com.google.gson.JsonObject;

/**
 * Model class to store User
 * 
 * @author abhsinh2
 *
 */
@Entity(value = Utility.DEFAULT_USER_COLLECTION, noClassnameStored = true)
public class User extends InstanceImpl {

	// Attributes used as json keys
	public static final String USERNAME_TAG = "username";
	public static final String PASSWORD_TAG = "password";
	public static final String EMAIL_TAG = "email";
	public static final String FIRST_NAME_TAG = "firstname";
	public static final String LAST_NAME_TAG = "lastname";

	public static final String IS_COMPANY_TAG = "isCompany";
	public static final String COMPANY_ID_TAG = "companyId";
	public static final String SITE_ID_TAG = "siteId";
	public static final String DEPT_ID_TAG = "deptId";
	public static final String SUB_DOMAIN_TAG = "subdomain";

	@Id
	private String username;
	private String password;
	private String email;
	private String firstname;
	private String lastname;

	private Boolean isCompany = Boolean.FALSE;
	private String companyId;
	private Integer siteId;
	private Integer deptId;
	private String subdomain;

	public User() {
		super();
	}

	public User(JsonObject json) {
		super(json);

		this.username = this.getValue(json, USERNAME_TAG, String.class);
		this.firstname = this.getValue(json, FIRST_NAME_TAG, String.class);
		this.lastname = this.getValue(json, LAST_NAME_TAG, String.class);
		this.password = this.getValue(json, PASSWORD_TAG, String.class);
		this.email = this.getValue(json, EMAIL_TAG, String.class);

		isCompany = this.getValue(json, IS_COMPANY_TAG, Boolean.class);
		companyId = this.getValue(json, COMPANY_ID_TAG, String.class);
		siteId = this.getValue(json, SITE_ID_TAG, Integer.class);
		deptId = this.getValue(json, DEPT_ID_TAG, Integer.class);
		subdomain = this.getValue(json, SUB_DOMAIN_TAG, String.class);
	}

	public User(Document document) {
		if (document != null) {
			this.setUsername(document.get(User._ID_TAG).toString());

			if (document.get(User.EMAIL_TAG) != null)
				this.setEmail(document.getString(User.EMAIL_TAG));

			if (document.get(User.FIRST_NAME_TAG) != null)
				this.setFirstname(document.getString(User.FIRST_NAME_TAG));

			if (document.get(User.LAST_NAME_TAG) != null)
				this.setLastname(document.getString(User.LAST_NAME_TAG));

			if (document.get(User.IS_COMPANY_TAG) != null)
				this.setIsCompany(document.getBoolean(User.IS_COMPANY_TAG));

			if (document.get(User.COMPANY_ID_TAG) != null)
				this.setCompanyId(document.getString(User.COMPANY_ID_TAG));

			if (document.get(User.SITE_ID_TAG) != null)
				this.setSiteId(document.getInteger(User.SITE_ID_TAG));

			if (document.get(User.DEPT_ID_TAG) != null)
				this.setDeptId(document.getInteger(User.DEPT_ID_TAG));

			if (document.get(User.SUB_DOMAIN_TAG) != null)
				this.setSubdomain(document.getString(User.SUB_DOMAIN_TAG));
		}
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Boolean getIsCompany() {
		return isCompany;
	}

	public void setIsCompany(Boolean isCompany) {
		if (isCompany == null)
			isCompany = Boolean.FALSE;
		this.isCompany = isCompany;
	}

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	public Integer getSiteId() {
		return siteId;
	}

	public void setSiteId(Integer siteId) {
		this.siteId = siteId;
	}

	public Integer getDeptId() {
		return deptId;
	}

	public void setDeptId(Integer deptId) {
		this.deptId = deptId;
	}

	public String getSubdomain() {
		return subdomain;
	}

	public void setSubdomain(String subdomain) {
		this.subdomain = subdomain;
	}

	public JsonObject toJson() {
		JsonObject json = new JsonObject();
		json.addProperty(_ID_TAG, this.username);
		json.addProperty(FIRST_NAME_TAG, this.firstname);
		json.addProperty(LAST_NAME_TAG, this.lastname);
		json.addProperty(PASSWORD_TAG, this.password);
		json.addProperty(EMAIL_TAG, this.email);

		json.addProperty(IS_COMPANY_TAG, this.isCompany);
		json.addProperty(COMPANY_ID_TAG, this.companyId);
		json.addProperty(SITE_ID_TAG, this.siteId);
		json.addProperty(DEPT_ID_TAG, this.deptId);
		json.addProperty(SUB_DOMAIN_TAG, this.subdomain);

		return json;
	}

	@Override
	public Document toMongoDocument() {
		Document document = new Document();
		document.append(User._ID_TAG, this.getUsername());

		addToDocument(document, User.EMAIL_TAG, this.getEmail());
		addToDocument(document, User.FIRST_NAME_TAG, this.getFirstname());
		addToDocument(document, User.LAST_NAME_TAG, this.getLastname());
		addToDocument(document, User.IS_COMPANY_TAG, this.getIsCompany());
		addToDocument(document, User.COMPANY_ID_TAG, this.getCompanyId());
		addToDocument(document, User.SITE_ID_TAG, this.getSiteId());
		addToDocument(document, User.DEPT_ID_TAG, this.getDeptId());
		addToDocument(document, User.SUB_DOMAIN_TAG, this.getSubdomain());

		return document;
	}

	@Override
	public String toString() {
		return "User [username=" + username + ", password=" + password + ", email=" + email + ", firstname=" + firstname
				+ ", lastname=" + lastname + ", isCompany=" + isCompany + ", companyId=" + companyId + ", siteId="
				+ siteId + ", deptId=" + deptId + ", subdomain=" + subdomain + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((username == null) ? 0 : username.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (username == null) {
			if (other.username != null)
				return false;
		} else if (!username.equals(other.username))
			return false;
		return true;
	}

}
