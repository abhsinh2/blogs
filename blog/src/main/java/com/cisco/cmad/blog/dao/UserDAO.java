package com.cisco.cmad.blog.dao;

import com.cisco.cmad.blog.model.User;

/**
 * Data Access Object to handle User
 * 
 * @author abhsinh2
 *
 */
public interface UserDAO {

	/**
	 * Adds user to database for give user
	 * 
	 * @param user
	 * @return
	 */
	public boolean add(User user);

	/**
	 * Returns User if username and password match else returns null.
	 * 
	 * @param username
	 * @param password
	 * @return
	 */
	public User validateLogin(String username, String password);

	/**
	 * Find User by given username.
	 * 
	 * @param username
	 * @return
	 */
	public User findByUsername(String username);
	
	/**
	 * removes all the users
	 */
	public void clearAll();
}
