package com.cisco.cmad.blog.model;

import java.util.Date;

import org.bson.Document;
import org.mongodb.morphia.annotations.Embedded;

import com.cisco.cmad.blog.util.Utility;
import com.google.gson.JsonObject;

/**
 * Model class to store Comments
 * 
 * @author abhsinh2
 *
 */
@Embedded
public class Comment extends InstanceImpl {

	// Attributes used as json keys
	public static final String CONTENT_TAG = "content";
	public static final String EMAIL_TAG = "email";
	public static final String AUTHOR_TAG = "author";
	public static final String DATE_TAG = "date";

	private String content;
	private String email;
	private String author;
	private Date date;

	public Comment() {
		super();
	}

	public Comment(JsonObject json) {
		super(json);
		this.content = json.get(CONTENT_TAG).getAsString();
		this.email = json.get(EMAIL_TAG).getAsString();
		this.author = json.get(AUTHOR_TAG).getAsString();
		this.date = Utility.convertToDate(json.get(DATE_TAG).getAsString());
	}

	public Comment(Document document) {
		super(document);

		if (document != null) {
			this.setAuthor(document.getString(Comment.AUTHOR_TAG));
			this.setContent(document.getString(Comment.CONTENT_TAG));
			this.setEmail(document.getString(Comment.EMAIL_TAG));
			this.setDate(document.getDate(Comment.DATE_TAG));
		}
	}
	
	public Comment(String author, String email, String content) {
		this.author = author;
		this.email = email;
		this.content = content;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public Date getDate() {
		if (this.date == null) {
			return new Date();
		}
		return this.date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public JsonObject toJson() {
		JsonObject json = new JsonObject();
		json.addProperty(CONTENT_TAG, this.content);
		json.addProperty(EMAIL_TAG, this.email);
		json.addProperty(AUTHOR_TAG, this.author);
		json.addProperty(DATE_TAG, this.date.toString());
		return json;
	}

	@Override
	public Document toMongoDocument() {
		Document comment = new Document();
		comment.append(Comment.AUTHOR_TAG, this.getAuthor());
		comment.append(Comment.CONTENT_TAG, this.getContent());
		comment.append(Comment.EMAIL_TAG, this.getEmail());
		comment.append(Comment.DATE_TAG, this.getDate());
		return comment;
	}

	@Override
	public String toString() {
		return "Comment [content=" + content + ", email=" + email + ", author=" + author + ", date=" + date + "]";
	}
}
