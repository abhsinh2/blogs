package com.cisco.cmad.blog.dao.mongo;

import com.cisco.cmad.blog.dao.SessionDAO;
import com.cisco.cmad.blog.model.Session;
import com.cisco.cmad.blog.util.Utility;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

import org.bson.Document;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

/**
 * Implementation for SessionDAO using MongoDB native API.
 * 
 * @author abhsinh2
 *
 */
public class SessionDAOImpl extends MongoDAO implements SessionDAO {
	private final MongoCollection<Document> sessionsCollection;

	public SessionDAOImpl(MongoDatabase mondoDatabase) {
		super(mondoDatabase);
		sessionsCollection = mondoDatabase.getCollection(Utility.getMongoSessionCollection());
	}

	public String findUserNameBySessionId(String sessionId) {
		Session session = getSession(sessionId);

		if (session == null) {
			return null;
		} else {
			return session.getUsername();
		}
	}

	public String startSession(String username) {
		// get 32 byte random number. that's a lot of bits.
		SecureRandom generator = new SecureRandom();
		byte randomBytes[] = new byte[32];
		generator.nextBytes(randomBytes);

		Base64.Encoder encoder = Base64.getEncoder();

		String sessionId = encoder.encodeToString(randomBytes);

		return startSession(username, sessionId);
	}

	@Override
	public String startSession(String username, String sessionId) {
		// build the BSON object
		Document session = new Document(Session.USERNAME_TAG, username);
		session.append(Session._ID_TAG, sessionId);

		sessionsCollection.deleteMany(new Document(Session.USERNAME_TAG, username));

		sessionsCollection.insertOne(session);

		return session.getString(Session._ID_TAG);
	}

	@Override
	public String endSession(String sessionId) {
		Session session = this.getSession(sessionId);
		sessionsCollection.deleteOne(new Document(Session._ID_TAG, sessionId));
		return session.getUsername();
	}

	@Override
	public Session getSession(String sessionId) {
		Document sessionDocument = sessionsCollection.find(new Document(Session._ID_TAG, sessionId)).first();
		if (sessionDocument != null) {
			return new Session(sessionDocument);
		}
		return null;
	}

	@Override
	public MongoCollection<Document> getCollection() {
		return sessionsCollection;
	}

	@Override
	public void clearAll() {
		// this.sessionsCollection.drop();
		this.sessionsCollection.deleteMany(new Document());
	}

	@Override
	public List<String> getAllSignedInUsernames() {
		List<Document> allSignedInUsers = sessionsCollection.find().into(new ArrayList<>());

		List<String> usernames = new ArrayList<>();

		if (allSignedInUsers != null && !allSignedInUsers.isEmpty()) {
			for (Document doc : allSignedInUsers) {
				usernames.add(doc.getString(Session.USERNAME_TAG));
			}
		}
		
		System.out.println("SignedIn User List Size" + usernames.size());
		
		return usernames;
	}
}
