package com.cisco.cmad.blog.model;

import org.bson.Document;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Indexed;

import com.cisco.cmad.blog.util.Utility;
import com.google.gson.JsonObject;

/**
 * Model class to store Session
 * 
 * @author abhsinh2
 *
 */
@Entity(value = Utility.DEFAULT_SESSION_COLLECTION, noClassnameStored = true)
public class Session extends InstanceImpl {

	// Attributes used as json keys
	public static final String USERNAME_TAG = "username";

	@Id
	private String sessionId;
	
	@Indexed(unique = false, dropDups = false)
	private String username;

	public Session() {
		super();
	}

	public Session(JsonObject json) {
		super(json);
		this.username = this.getValue(json, USERNAME_TAG, String.class);
		this.sessionId = this.getValue(json, _ID_TAG, String.class);
	}
	
	public Session(Document document) {
		super(document);		

		if (document != null) {
			this.setUsername(document.get(Session.USERNAME_TAG).toString());
			this.setSessionId(document.get(Session._ID_TAG).toString());
		}	
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public JsonObject toJson() {
		JsonObject json = new JsonObject();
		json.addProperty(USERNAME_TAG, this.username);
		json.addProperty(_ID_TAG, this.sessionId);
		return json;
	}
	
	@Override
	protected Document toMongoDocument() {
		Document session = new Document();
		session.append(Session._ID_TAG, this.getSessionId());
		session.append(Session.USERNAME_TAG, this.getUsername());
		return session;
	}

	@Override
	public String toString() {
		return "Session [sessionId=" + sessionId + ", username=" + username + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((sessionId == null) ? 0 : sessionId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Session other = (Session) obj;
		if (sessionId == null) {
			if (other.sessionId != null)
				return false;
		} else if (!sessionId.equals(other.sessionId))
			return false;
		return true;
	}
	
}
