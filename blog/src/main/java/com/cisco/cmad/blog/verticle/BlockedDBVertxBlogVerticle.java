package com.cisco.cmad.blog.verticle;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.bson.types.ObjectId;

import com.cisco.cmad.blog.auth.MongoDBUserAuthProvider;
import com.cisco.cmad.blog.dao.BlogDAO;
import com.cisco.cmad.blog.dao.CompanyDAO;
import com.cisco.cmad.blog.dao.SessionDAO;
import com.cisco.cmad.blog.dao.UserDAO;
import com.cisco.cmad.blog.dto.BlogDTO;
import com.cisco.cmad.blog.dto.CommentDTO;
import com.cisco.cmad.blog.dto.CompanyDTO;
import com.cisco.cmad.blog.dto.DepartmentDTO;
import com.cisco.cmad.blog.dto.SiteDTO;
import com.cisco.cmad.blog.dto.UserDTO;
import com.cisco.cmad.blog.model.Blog;
import com.cisco.cmad.blog.model.Comment;
import com.cisco.cmad.blog.model.Company;
import com.cisco.cmad.blog.model.Department;
import com.cisco.cmad.blog.model.Site;
import com.cisco.cmad.blog.model.User;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonObject;

import io.vertx.core.http.HttpHeaders;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.Json;
import io.vertx.ext.auth.AuthProvider;
import io.vertx.ext.web.RoutingContext;

/**
 * Vertx Verticle for blocking db operation
 * 
 * @author abhsinh2
 *
 */
public class BlockedDBVertxBlogVerticle extends VertxBlogVerticle {

	protected BlogDAO blogDAO;
	protected UserDAO userDAO;
	protected SessionDAO sessionDAO;
	protected CompanyDAO companyDAO;

	public BlockedDBVertxBlogVerticle(BlogDAO blogDAO, UserDAO userDAO, SessionDAO sessionDAO, CompanyDAO companyDAO) {
		this(new MongoDBUserAuthProvider(userDAO), blogDAO, userDAO, sessionDAO, companyDAO);
	}

	public BlockedDBVertxBlogVerticle(AuthProvider authProvider, BlogDAO blogDAO, UserDAO userDAO,
			SessionDAO sessionDAO, CompanyDAO companyDAO) {
		super(sessionDAO, authProvider);

		this.blogDAO = blogDAO;
		this.userDAO = userDAO;
		this.sessionDAO = sessionDAO;
		this.companyDAO = companyDAO;
	}

	@Override
	public void addNewBlog(RoutingContext routingContext) {
		System.out.println("BlockedDBVertxBlogVerticle.addNewBlog:" + routingContext.getBodyAsString());
		HttpServerResponse response = routingContext.response();

		String username = verifySessionInRequest(routingContext, this.sessionDAO);
		if (username == null) {
			System.out.println("BlockedDBVertxBlogVerticle.addNewBlog username is null");
			response.end();
			return;
		}

		BlogDTO blogdto = Json.decodeValue(routingContext.getBodyAsString(), BlogDTO.class);
		blogdto.setDate(new Date());
		System.out.println("Testing config params: " + config().getInteger("testField1"));

		Blog blog = blogdto.toModel();
		blog.setAuthor(username);
		blogDAO.add(blog);

		response.setStatusCode(201);
		response.putHeader(HttpHeaders.CONTENT_TYPE, "text/plain");
		response.end("Blog Added");
		System.out.println("BlockedDBVertxBlogVerticle.addNewBlog completes");
	}

	@Override
	public void getBlog(RoutingContext routingContext) {
		System.out.println("BlockedDBVertxBlogVerticle.getBlog");

		String urllink = routingContext.request().getParam("urllink");
		Blog blog = blogDAO.findByUrlLink(urllink);

		HttpServerResponse response = routingContext.response();

		if (blog != null) {
			User user = userDAO.findByUsername(blog.getAuthor());

			BlogDTO blogdto = new BlogDTO(blog);

			if (user != null) {
				blogdto.setUserFirst(user.getFirstname());
				blogdto.setUserLast(user.getLastname());
			}

			ObjectMapper mapper = new ObjectMapper();
			JsonNode node = mapper.valueToTree(blogdto);

			response.setStatusCode(200);
			response.putHeader(HttpHeaders.CONTENT_TYPE, "application/json");
			response.end(node.toString());
		} else {
			response.setStatusCode(404);
			response.putHeader(HttpHeaders.CONTENT_TYPE, "text/html");
			response.end("Blog not found");
		}

		System.out.println("BlockedDBVertxBlogVerticle.getBlog completes");
	}

	@Override
	protected void getAllBlogs(RoutingContext routingContext) {
		System.out.println("BlockedDBVertxBlogVerticle.getAllBlogs");
		List<Blog> blogs = blogDAO.find(10, true);
		if (blogs != null && blogs.size() > 0) {
			List<BlogDTO> dtos = new ArrayList<>(blogs.size());
			for (Blog blog : blogs) {
				User user = userDAO.findByUsername(blog.getAuthor());

				BlogDTO blogdto = new BlogDTO(blog);

				if (user != null) {
					blogdto.setUserFirst(user.getFirstname());
					blogdto.setUserLast(user.getLastname());
				}

				dtos.add(blogdto);
			}

			ObjectMapper mapper = new ObjectMapper();
			JsonNode node = mapper.valueToTree(dtos);

			HttpServerResponse response = routingContext.response();

			response.setStatusCode(200);
			response.putHeader(HttpHeaders.CONTENT_TYPE, "application/json");
			response.end(node.toString());
		} else {
			routingContext.response().setStatusCode(404).end("Blog not found");
		}
		System.out.println("BlockedDBVertxBlogVerticle.getAllBlogs completes");
	}

	@Override
	protected void getBlogsForUser(RoutingContext routingContext) {
		System.out.println("BlockedDBVertxBlogVerticle.getBlogsForUser");
		String username = routingContext.request().getParam("username");
		List<Blog> blogs = blogDAO.findByAuthor(username, null, Boolean.TRUE);

		if (blogs != null && blogs.size() > 0) {
			List<BlogDTO> dtos = new ArrayList<>(blogs.size());
			for (Blog blog : blogs) {
				User user = userDAO.findByUsername(blog.getAuthor());

				BlogDTO blogdto = new BlogDTO(blog);

				if (user != null) {
					blogdto.setUserFirst(user.getFirstname());
					blogdto.setUserLast(user.getLastname());
				}

				dtos.add(blogdto);
			}

			ObjectMapper mapper = new ObjectMapper();
			JsonNode node = mapper.valueToTree(dtos);

			HttpServerResponse response = routingContext.response();

			response.setStatusCode(200);
			response.putHeader(HttpHeaders.CONTENT_TYPE, "application/json");
			response.end(node.toString());
		} else {
			routingContext.response().setStatusCode(404).end("Blog not found");
		}

		System.out.println("BlockedDBVertxBlogVerticle.getBlogsForUser completes");
	}

	@Override
	public void getBlogsByTag(RoutingContext routingContext) {
		System.out.println("BlockedDBVertxBlogVerticle.getBlogsByTag");
		String tag = routingContext.request().getParam("thetag");	
		
		List<Blog> blogs = null;
		if (tag == null || tag.equals("all")) {
			blogs = blogDAO.find(null, Boolean.TRUE);
		} else {
			blogs = blogDAO.findByTag(tag, null, Boolean.TRUE);
		}

		if (blogs != null && blogs.size() > 0) {
			List<BlogDTO> dtos = new ArrayList<>(blogs.size());
			for (Blog blog : blogs) {
				User user = userDAO.findByUsername(blog.getAuthor());

				BlogDTO blogdto = new BlogDTO(blog);

				if (user != null) {
					blogdto.setUserFirst(user.getFirstname());
					blogdto.setUserLast(user.getLastname());
				}

				dtos.add(blogdto);
			}

			ObjectMapper mapper = new ObjectMapper();
			JsonNode node = mapper.valueToTree(dtos);

			HttpServerResponse response = routingContext.response();

			response.setStatusCode(200);
			response.putHeader(HttpHeaders.CONTENT_TYPE, "application/json");
			response.end(node.toString());
		} else {
			routingContext.response().setStatusCode(404).end("Blog not found");
		}

		System.out.println("BlockedDBVertxBlogVerticle.getBlogsByTag completes");
	}

	@Override
	public void deleteBlog(RoutingContext routingContext) {
		System.out.println("BlockedDBVertxBlogVerticle.deleteBlog");
		System.out.println("BlockedDBVertxBlogVerticle.deleteBlog completes");
	}

	@Override
	public void addNewComment(RoutingContext routingContext) {
		System.out.println("BlockedDBVertxBlogVerticle.addNewComment");
		HttpServerResponse response = routingContext.response();

		String username = verifySessionInRequest(routingContext, this.sessionDAO);
		if (username == null) {
			System.out.println("BlockedDBVertxBlogVerticle.addNewComment username is null");
			response.end();
			return;
		}

		String urllink = routingContext.request().getParam("urllink");

		CommentDTO commentDto = Json.decodeValue(routingContext.getBodyAsString(), CommentDTO.class);
		commentDto.setDate(new Date());

		Comment comment = commentDto.toModel();
		comment.setAuthor(username);

		blogDAO.addComment(comment, urllink);
		
		// websocket start	
		JsonObject commentWSData = commentDto.toJson();
		
		JsonObject wsData = new JsonObject();
		wsData.addProperty("urllink", urllink);
		wsData.add("comment", commentWSData);
		
		JsonObject rootObj = new JsonObject();
		rootObj.addProperty("messageType", "NewComment");
		rootObj.add("messageObject", wsData);
		
		this.sendMessageOverWebSocket(rootObj.toString());
		// websocket end
		
		JsonObject returnObj = new JsonObject();
		returnObj.addProperty("author", username);
		
		response.setStatusCode(201);
		response.putHeader(HttpHeaders.CONTENT_TYPE, "application/json");
		response.end(returnObj.toString());

		System.out.println("BlockedDBVertxBlogVerticle.addNewComment");
	}

	@Override
	public void addNewUser(RoutingContext routingContext) {
		System.out.println("BlockedDBVertxBlogVerticle.addNewUser");
		System.out.println("BlockedDBVertxBlogVerticle.addNewUser:" + routingContext.getBodyAsString());

		UserDTO userDto = Json.decodeValue(routingContext.getBodyAsString(), UserDTO.class);

		User user = userDto.toModel();
		userDAO.add(user);

		HttpServerResponse response = routingContext.response();

		response.setStatusCode(201);
		response.putHeader(HttpHeaders.CONTENT_TYPE, "text/plain");
		response.end("User Added");

		System.out.println("BlockedDBVertxBlogVerticle.addNewUser completes");
	}

	@Override
	public void login(RoutingContext routingContext) {
		System.out.println("BlockedDBVertxBlogVerticle.login");
		System.out.println("BlockedDBVertxBlogVerticle.login:" + routingContext.getBodyAsString());
		HttpServerResponse response = routingContext.response();

		UserDTO userDto = Json.decodeValue(routingContext.getBodyAsString(), UserDTO.class);

		String username = userDto.getUsername();
		String password = userDto.getPassword();
		System.out.println("Login: User submitted: " + username + "  " + password);

		User user = userDAO.validateLogin(username, password);

		System.out.println("BlockedDBVertxBlogVerticle.login user:" + user);

		if (user == null) {
			response.setStatusCode(401);
			response.putHeader(HttpHeaders.CONTENT_TYPE, "text/html");
			response.end("Invalid Login");
		} else {
			// Handle session using Session
			handleLogin(username, this.sessionDAO, routingContext);
			
			// websocket start	
			JsonObject wsData = new JsonObject();
			wsData.addProperty("username", username);
			wsData.addProperty("firstname", user.getFirstname());
			wsData.addProperty("lastname", user.getLastname());
			
			JsonObject rootObj = new JsonObject();
			rootObj.addProperty("messageType", "UserLogin");
			rootObj.add("messageObject", wsData);
			
			System.out.println("rootObj.toString():" + rootObj.toString());
			
			this.sendMessageOverWebSocket(rootObj.toString());
			// websocket end
			
			JsonObject responseObj = new JsonObject();
			responseObj.addProperty("username", username);

			response.setStatusCode(201);
			response.putHeader(HttpHeaders.CONTENT_TYPE, "text/html");
			response.end(responseObj.toString());
		}

		System.out.println("BlockedDBVertxBlogVerticle.login completes");
	}

	@Override
	public void logout(RoutingContext routingContext) {
		System.out.println("BlockedDBVertxBlogVerticle.logout");

		HttpServerResponse response = routingContext.response();
		String username = handleLogout(this.sessionDAO, routingContext);

		// websocket start
		JsonObject wsData = new JsonObject();
		wsData.addProperty("username", username);

		JsonObject rootObj = new JsonObject();
		rootObj.addProperty("messageType", "UserLogout");
		rootObj.add("messageObject", wsData);

		this.sendMessageOverWebSocket(rootObj.toString());
		// websocket end
					
		response.setStatusCode(201);
		response.putHeader(HttpHeaders.CONTENT_TYPE, "text/html");
		response.end();

		System.out.println("BlockedDBVertxBlogVerticle.logout completes");
	}

	@Override
	public void blog_not_found(RoutingContext routingContext) {
		System.out.println("BlockedDBVertxBlogVerticle.blog_not_found");
		System.out.println("BlockedDBVertxBlogVerticle.blog_not_found completes");
	}

	@Override
	public void internalError(RoutingContext routingContext) {
		System.out.println("BlockedDBVertxBlogVerticle.internalError");
		System.out.println("BlockedDBVertxBlogVerticle.internalError completes");
	}

	@Override
	public void getAllSignedInUsers(RoutingContext routingContext) {
		String username = verifySessionInRequest(routingContext, this.sessionDAO);
		HttpServerResponse response = routingContext.response();
		if (username == null) {
			System.out.println("BlockedDBVertxBlogVerticle.addNewComment username is null");
			response.setStatusCode(500);
			response.putHeader(HttpHeaders.CONTENT_TYPE, "text/html");
			response.end("user not signed in");
		} else {
			response.setStatusCode(200);
			response.putHeader(HttpHeaders.CONTENT_TYPE, "text/html");
			response.end("user in session");
		}
	}

	@Override
	public void getCompanies(RoutingContext routingContext) {
		System.out.println("BlockedDBVertxBlogVerticle.getCompanies");
		HttpServerResponse response = routingContext.response();

		List<Company> companies = companyDAO.getCompanies();

		List<CompanyDTO> dtos = new ArrayList<>();

		if (companies != null && companies.size() > 0) {
			dtos = new ArrayList<>(companies.size());

			for (Company company : companies) {
				CompanyDTO dto = new CompanyDTO(company);
				dtos.add(dto);
			}
		}
		
		ObjectMapper mapper = new ObjectMapper();
		JsonNode node = mapper.valueToTree(dtos);

		response.setStatusCode(200);
		response.putHeader(HttpHeaders.CONTENT_TYPE, "application/json");
		response.end(node.toString());

		System.out.println("BlockedDBVertxBlogVerticle.getCompanies completes");
	}

	@Override
	public void getSites(RoutingContext routingContext) {
		System.out.println("BlockedDBVertxBlogVerticle.getSites");
		HttpServerResponse response = routingContext.response();

		String companyId = routingContext.request().getParam("companyId");

		List<Site> sites = companyDAO.getSites(new ObjectId(companyId));

		List<SiteDTO> dtos = new ArrayList<>();

		if (sites != null && sites.size() > 0) {
			dtos = new ArrayList<>(sites.size());

			for (Site site : sites) {
				SiteDTO dto = new SiteDTO(site);
				dtos.add(dto);
			}
		}		
		
		ObjectMapper mapper = new ObjectMapper();
		JsonNode node = mapper.valueToTree(dtos);
		
		response.setStatusCode(200);
		response.putHeader(HttpHeaders.CONTENT_TYPE, "application/json");
		response.end(node.toString());

		System.out.println("BlockedDBVertxBlogVerticle.getSites completes");
	}

	@Override
	public void getDepartments(RoutingContext routingContext) {
		System.out.println("BlockedDBVertxBlogVerticle.getDepartments");
		HttpServerResponse response = routingContext.response();

		String companyId = routingContext.request().getParam("companyId");
		String siteId = routingContext.request().getParam("siteId");

		List<Department> departments = companyDAO.getDepartments(new ObjectId(companyId), Integer.parseInt(siteId));

		List<DepartmentDTO> dtos = new ArrayList<>();
		if (departments != null && departments.size() > 0) {
			dtos = new ArrayList<>(departments.size());

			for (Department department : departments) {
				DepartmentDTO dto = new DepartmentDTO(department);
				dtos.add(dto);
			}
		}
		
		ObjectMapper mapper = new ObjectMapper();
		JsonNode node = mapper.valueToTree(dtos);

		response.setStatusCode(200);
		response.putHeader(HttpHeaders.CONTENT_TYPE, "application/json");
		response.end(node.toString());

		System.out.println("BlockedDBVertxBlogVerticle.getDepartments completes");
	}

	@Override
	public void addCompany(RoutingContext routingContext) {
		System.out.println("BlockedDBVertxBlogVerticle.addCompany:" + routingContext.getBodyAsString());
		HttpServerResponse response = routingContext.response();

		CompanyDTO dto = Json.decodeValue(routingContext.getBodyAsString(), CompanyDTO.class);

		Company company = dto.toModel();
		ObjectId companyId = companyDAO.addCompany(company);

		response.setStatusCode(201);
		response.putHeader(HttpHeaders.CONTENT_TYPE, "text/plain");
		response.end(companyId.toHexString());
		System.out.println("BlockedDBVertxBlogVerticle.addCompany completes");
	}

	@Override
	public void addSite(RoutingContext routingContext) {
		System.out.println("BlockedDBVertxBlogVerticle.addSite:" + routingContext.getBodyAsString());
		HttpServerResponse response = routingContext.response();

		SiteDTO dto = Json.decodeValue(routingContext.getBodyAsString(), SiteDTO.class);
		String companyId = routingContext.request().getParam("companyId");

		Site site = dto.toModel();
		Integer siteId = companyDAO.addSite(new ObjectId(companyId), site);

		response.setStatusCode(201);
		response.putHeader(HttpHeaders.CONTENT_TYPE, "text/plain");
		response.end(siteId.toString());
		System.out.println("BlockedDBVertxBlogVerticle.addSite completes");
	}

	@Override
	public void addDepartment(RoutingContext routingContext) {
		System.out.println("BlockedDBVertxBlogVerticle.addDepartment:" + routingContext.getBodyAsString());
		HttpServerResponse response = routingContext.response();

		DepartmentDTO dto = Json.decodeValue(routingContext.getBodyAsString(), DepartmentDTO.class);
		String companyId = routingContext.request().getParam("companyId");
		String siteId = routingContext.request().getParam("siteId");

		Department dept = dto.toModel();
		Integer deptId = companyDAO.addDepartment(new ObjectId(companyId), Integer.parseInt(siteId), dept);

		response.setStatusCode(201);
		response.putHeader(HttpHeaders.CONTENT_TYPE, "text/plain");
		response.end(deptId.toString());
		System.out.println("BlockedDBVertxBlogVerticle.addDepartment completes");
	}

	@Override
	protected void getCompany(RoutingContext routingContext) {
		System.out.println("BlockedDBVertxBlogVerticle.getCompany");
		HttpServerResponse response = routingContext.response();
		
		String companyId = routingContext.request().getParam("companyId");

		Company company = companyDAO.getCompany(new ObjectId(companyId));
		
		CompanyDTO dto = null;

		if (company != null) {
			dto = new CompanyDTO(company);
		}
		
		ObjectMapper mapper = new ObjectMapper();
		JsonNode node = mapper.valueToTree(dto);

		response.setStatusCode(200);
		response.putHeader(HttpHeaders.CONTENT_TYPE, "application/json");
		response.end(node.toString());

		System.out.println("BlockedDBVertxBlogVerticle.getCompany completes");
		
	}

}
