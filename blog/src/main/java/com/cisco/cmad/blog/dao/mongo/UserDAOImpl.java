package com.cisco.cmad.blog.dao.mongo;

import com.cisco.cmad.blog.dao.UserDAO;
import com.cisco.cmad.blog.model.User;
import com.cisco.cmad.blog.util.Utility;
import com.mongodb.MongoWriteException;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

import org.bson.Document;

import java.security.SecureRandom;
import java.util.Random;

/**
 * Implementation for UserDAO using MongoDB native API.
 * 
 * @author abhsinh2
 *
 */
public class UserDAOImpl extends MongoDAO implements UserDAO {
	private MongoCollection<Document> usersCollection;
	private Random random = new SecureRandom();	

	public UserDAOImpl(final MongoDatabase mongoDatabase) {
		super(mongoDatabase);
		usersCollection = mongoDatabase.getCollection(Utility.getMongoUserCollection());
	}
	
	@Override
	public boolean add(User user) {
		if (user == null || user.getUsername() == null || user.getPassword() == null) {
			return false;
		}
		
		Document userDocument = user.toMongoDocument();
		
		String passwordHash = Utility.encodePassword(user.getPassword(), Integer.toString(random.nextInt()));
		userDocument.append(User.PASSWORD_TAG, passwordHash);

		try {
			usersCollection.insertOne(userDocument);
			return true;
		} catch (MongoWriteException e) {
			System.out.println("Username already in use: " + user.getUsername());			
		}
		return false;
	}
	
	@Override
	public User findByUsername(String username) {
		Document userDocument = usersCollection.find(new Document(User._ID_TAG, username)).first();
		if (userDocument != null) {
			return new User(userDocument);
		} 
		return null;
	}

	@Override
	public User validateLogin(String username, String password) {
		Document userDocument = usersCollection.find(new Document(User._ID_TAG, username)).first();

		if (userDocument == null) {
			System.out.println("User not in database");
			return null;
		}

		String hashedAndSalted = userDocument.get(User.PASSWORD_TAG).toString();		
		String salt = hashedAndSalted.split(",")[1];		
		String hashedPassword = Utility.encodePassword(password, salt);		

		if (!hashedAndSalted.equals(hashedPassword)) {
			System.out.println("Submitted password is not a match");
			return null;
		}	

		System.out.println("Login Successful");
		return new User(userDocument);
	}
	
	@Override
	public MongoCollection<Document> getCollection() {
		return usersCollection;
	}

	@Override
	public void clearAll() {
		//this.usersCollection.drop();		
		this.usersCollection.deleteMany(new Document());
	}	
}
