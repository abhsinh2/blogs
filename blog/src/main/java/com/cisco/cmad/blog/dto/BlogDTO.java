package com.cisco.cmad.blog.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import com.cisco.cmad.blog.model.Blog;
import com.cisco.cmad.blog.model.Comment;
import com.cisco.cmad.blog.model.User;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

/**
 * Represents Data Transfer Objects for Blog.
 * 
 * @author abhsinh2
 *
 */
public class BlogDTO implements Serializable {
	private static final long serialVersionUID = -2541586285467242180L;

	private String urllink;
	private String title;
	private String content;
	private String author;
	private String tags;
	private List<CommentDTO> comments = new ArrayList<CommentDTO>();
	private Date date;
	private String userFirst = "";
	private String userLast = "";

	public BlogDTO() {

	}
	
	public BlogDTO(Blog blog) {
		this.urllink = blog.getUrllink();
		this.title = blog.getTitle();
		this.content = blog.getContent();
		this.date = blog.getDate();
		this.author = blog.getAuthor();

		String temp = "";
		for (String str : blog.getTags()) {
			temp = temp + str + ",";
		}
		
		if (!temp.equals("")) {
			temp = temp.substring(0, temp.length() - 1);
			this.tags = temp;
		}		

		for (Comment comment : blog.getComments()) {
			comments.add(new CommentDTO(comment));
		}
	}

	public BlogDTO(Blog blog, User user) {
		this.urllink = blog.getUrllink();
		this.title = blog.getTitle();
		this.content = blog.getContent();
		this.date = blog.getDate();
		this.author = blog.getAuthor();

		String temp = "";
		for (String str : blog.getTags()) {
			temp = temp + str + ",";
		}
		temp = temp.substring(0, temp.length() - 1);
		this.tags = temp;

		for (Comment comment : blog.getComments()) {
			comments.add(new CommentDTO(comment));
		}
		
		if (user != null) {
			this.userFirst = user.getFirstname();
			this.userLast = user.getLastname();
		}
	}

	public String getUrllink() {
		return urllink;
	}

	public void setUrllink(String urllink) {
		this.urllink = urllink;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getTags() {
		return tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}

	public List<CommentDTO> getComments() {
		return comments;
	}

	public void setComments(List<CommentDTO> comments) {
		this.comments = comments;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getUserFirst() {
		return userFirst;
	}

	public void setUserFirst(String userFirst) {
		this.userFirst = userFirst;
	}

	public String getUserLast() {
		return userLast;
	}

	public void setUserLast(String userLast) {
		this.userLast = userLast;
	}

	public Blog toModel() {
		Blog blog = new Blog();
		blog.setAuthor(this.getAuthor());
		blog.setContent(this.getContent());
		blog.setTitle(this.getTitle());

		if (this.getDate() == null) {
			blog.setDate(new Date());
		} else {
			blog.setDate(this.getDate());
		}

		for (String str : extractTags(this.tags)) {
			blog.addTag(str);
		}

		for (CommentDTO commentDto : this.getComments()) {
			blog.addComment(commentDto.toModel());
		}

		return blog;
	}
	
	public JsonObject toJson() {
		Gson gson = new Gson();
		return gson.toJsonTree(this).getAsJsonObject();
	}

	// tags the tags string and put it into an array
	private ArrayList<String> extractTags(String tags) {
		ArrayList<String> cleaned = new ArrayList<String>();

		if (tags != null && !tags.isEmpty()) {
			tags = tags.replaceAll("\\s", "");
			String tagArray[] = tags.split(",");

			for (String tag : tagArray) {
				if (!tag.equals("") && !cleaned.contains(tag)) {
					cleaned.add(tag);
				}
			}
		}

		return cleaned;
	}

	@Override
	public String toString() {
		return "BlogDTO [urllink=" + urllink + ", title=" + title + ", content=" + content + ", author=" + author
				+ ", tags=" + tags + ", comments=" + comments + ", date=" + date + ", userFirst=" + userFirst
				+ ", userLast=" + userLast + "]";
	}
}
