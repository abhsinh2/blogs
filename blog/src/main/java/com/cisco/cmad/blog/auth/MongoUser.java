package com.cisco.cmad.blog.auth;

import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.AbstractUser;
import io.vertx.ext.auth.AuthProvider;

public class MongoUser extends AbstractUser {
	private MongoDBUserAuthProvider authProvider;
	private JsonObject principal;
	private String rolePrefix;
	private String username;

	public MongoUser(String username, MongoDBUserAuthProvider authProvider, String rolePrefix) {
		this.username = username;
		this.authProvider = authProvider;
		this.rolePrefix = rolePrefix;
	}

	@Override
	public void setAuthProvider(AuthProvider authProvider) {
		System.out.println("MongoUser.setAuthProvider");
		if (authProvider instanceof MongoDBUserAuthProvider) {
			this.authProvider = (MongoDBUserAuthProvider) authProvider;
		} else {
			throw new IllegalArgumentException("Not a JDBCAuthImpl");
		}
	}

	@Override
	public JsonObject principal() {
		System.out.println("MongoUser.principal");
		if (principal == null) {
			principal = new JsonObject().put("username", username);
		}
		return principal;
	}

	@Override
	protected void doIsPermitted(String permissionOrRole, Handler<AsyncResult<Boolean>> resultHandler) {
		System.out.println("MongoUser.doIsPermitted");
		// TODO: handle role permission		
		resultHandler.handle(Future.succeededFuture(true));
	}

}
