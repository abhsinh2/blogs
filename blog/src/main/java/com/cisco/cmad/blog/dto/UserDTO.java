package com.cisco.cmad.blog.dto;

import java.io.Serializable;

import com.cisco.cmad.blog.model.Company;
import com.cisco.cmad.blog.model.Department;
import com.cisco.cmad.blog.model.Site;
import com.cisco.cmad.blog.model.User;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

/**
 * Represents Data Transfer Objects for User.
 * 
 * @author abhsinh2
 *
 */
public class UserDTO implements Serializable {
	private static final long serialVersionUID = 3421161211935234478L;

	private String username;
	private String password;
	private String email;

	private String firstname;
	private String lastname;

	private Boolean isCompany = Boolean.FALSE;

	private String companyId;
	private Integer siteId;
	private Integer deptId;

	private String companyName;
	private String deptName;
	private String siteName;

	private String subdomain;

	public UserDTO() {

	}

	public UserDTO(User user) {
		this.username = user.getUsername();
		this.email = user.getEmail();

		this.firstname = user.getFirstname();
		this.lastname = user.getLastname();

		this.isCompany = user.getIsCompany();
		this.companyId = user.getCompanyId();
		this.siteId = user.getSiteId();
		this.deptId = user.getDeptId();
	}
	
	public UserDTO(User user, Company company) {
		this.username = user.getUsername();
		this.email = user.getEmail();

		this.firstname = user.getFirstname();
		this.lastname = user.getLastname();

		this.isCompany = user.getIsCompany();
		this.companyId = user.getCompanyId();
		this.siteId = user.getSiteId();
		this.deptId = user.getDeptId();

		if (company != null) {
			this.companyName = company.getName();
			for (Site site : company.getSites()) {
				if (site.getId().equals(user.getSiteId())) {
					this.siteName = site.getName();

					for (Department deprt : site.getDepartments()) {
						if (deprt.getId().equals(user.getDeptId())) {
							this.deptName = deprt.getName();
							break;
						}
					}

					break;
				}
			}
		}
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String first) {
		this.firstname = first;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String last) {
		this.lastname = last;
	}

	public Boolean getIsCompany() {
		return isCompany;
	}

	public void setIsCompany(Boolean isCompany) {
		if (isCompany == null)
			isCompany = Boolean.FALSE;
		this.isCompany = isCompany;
	}

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	public Integer getSiteId() {
		return siteId;
	}

	public void setSiteId(Integer siteId) {
		this.siteId = siteId;
	}

	public Integer getDeptId() {
		return deptId;
	}

	public void setDeptId(Integer deptId) {
		this.deptId = deptId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getSiteName() {
		return siteName;
	}

	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public String getSubdomain() {
		return subdomain;
	}

	public void setSubdomain(String subdomain) {
		this.subdomain = subdomain;
	}

	public User toModel() {
		User user = new User();
		user.setUsername(this.getUsername());
		user.setPassword(this.getPassword());
		user.setEmail(this.getEmail());

		user.setFirstname(this.getFirstname());
		user.setLastname(this.getLastname());

		user.setIsCompany(this.getIsCompany());
		
		user.setCompanyId(this.getCompanyId());
		user.setSiteId(this.getSiteId());
		user.setDeptId(this.getDeptId());
		
		user.setSubdomain(this.getSubdomain());

		return user;
	}
	
	public JsonObject toJson() {
		Gson gson = new Gson();
		return gson.toJsonTree(this).getAsJsonObject();
	}

}
