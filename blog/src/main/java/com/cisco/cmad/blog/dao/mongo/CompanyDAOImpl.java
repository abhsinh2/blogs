package com.cisco.cmad.blog.dao.mongo;

import java.util.ArrayList;
import java.util.List;

import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;

import com.cisco.cmad.blog.dao.CompanyDAO;
import com.cisco.cmad.blog.model.Company;
import com.cisco.cmad.blog.model.Department;
import com.cisco.cmad.blog.model.Site;
import com.cisco.cmad.blog.util.Utility;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import static com.mongodb.client.model.Filters.*;

/**
 * Implementation for CompanyDAO using MongoDB native API.
 * 
 * @author abhsinh2
 *
 */
public class CompanyDAOImpl extends MongoDAO implements CompanyDAO {

	private MongoCollection<Document> companyCollection;

	public CompanyDAOImpl(MongoDatabase mongoDatabase) {
		super(mongoDatabase);
		companyCollection = mongoDatabase.getCollection(Utility.getMongoCompanyCollection());
	}

	@Override
	public ObjectId addCompany(Company company) {
		Document compDocument = companyCollection.find(new Document(Company.NAME_TAG, company.getName())).first();

		if (compDocument == null) {
			System.out.println("Company " + company.getName() + " does not exists in db");
			List<Site> sites = company.getSites();
			if (sites != null && !sites.isEmpty()) {
				for (Site site : sites) {
					if (site.getId() == null) {
						site.setId(Site.getUniqueId());
					}

					List<Department> depts = site.getDepartments();
					if (depts != null && !depts.isEmpty()) {
						for (Department dept : depts) {
							if (dept.getId() == null) {
								dept.setId(Department.getUniqueId());
							}
						}
					}
				}
			}

			compDocument = company.toMongoDocument();

			ObjectId _id = compDocument.getObjectId(Company._ID_TAG);
			if (_id == null) {
				System.out.println("Company id was null");
				try {
					companyCollection.insertOne(compDocument);
					System.out.println("Company Inserted");
					return compDocument.getObjectId(Company._ID_TAG);
				} catch (Exception e) {
					System.out.println("Error inserting company");
				}
			}
		} else {
			System.out.println("Company " + company.getName() + " exists in db");
			// If Company exists in database
			Company dbCompany = new Company(compDocument);
			ObjectId companyId = compDocument.getObjectId(Company._ID_TAG);
			
			// Merge Site
			processSite(company, dbCompany);
			
			List<Document> siteDocs = new ArrayList<>();
			for (Site temp : dbCompany.getSites()) {
				siteDocs.add(temp.toMongoDocument());
			}
			
			try {
				companyCollection.updateOne(new Document(Company._ID_TAG, companyId),
						new Document("$set", new Document(Company.SITES_TAG, siteDocs)));
				System.out.println("Company Inserted");
				return companyId;
			} catch (Exception e) {
				System.out.println("Error inserting company " + e);
			}
		}

		return null;
	}

	private void processSite(Company company, Company dbCompany) {
		List<Site> sites = company.getSites();

		if (sites != null) {
			for (Site site : sites) {
				Site matchedDbSite = null;

				// Check if given site available in DB
				for (Site dbSite : dbCompany.getSites()) {
					if (site.getName().equals(dbSite.getName())) {
						matchedDbSite = dbSite;
						break;
					}
				}

				if (matchedDbSite == null) {
					// Site was not in DB
					if (site.getId() == null) {
						site.setId(Site.getUniqueId());
					}
					System.out.println("Site " + site.getName() + " does not exists in db");
					dbCompany.addSite(site);
				} else {
					// Site was in DB
					// Merge Departments
					processDepartment(site, matchedDbSite);
				}
			}
		}
	}

	private void processDepartment(Site site, Site dbSite) {
		List<Department> departments = site.getDepartments();

		if (departments != null) {
			for (Department dept : departments) {
				Department matchedDbDept = null;

				// Check if given dept available in DB
				for (Department dbDept : dbSite.getDepartments()) {
					if (dept.getName().equals(dbDept.getName())) {
						matchedDbDept = dbDept;
						break;
					}
				}

				if (matchedDbDept == null) {
					// Department was not in DB
					if (dept.getId() == null) {
						dept.setId(Department.getUniqueId());
					}
					System.out.println("Department " + dept.getName() + " does not exists in db");
					dbSite.addDepartment(dept);
				} else {
					System.out.println("Department " + dept.getName() + " exists in db");
				}
			}
		}
	}

	@Override
	public Integer addSite(ObjectId companyId, Site site) {	
		Document compDocument = companyCollection.find(new Document(Company._ID_TAG, companyId)).first();
		
		if (compDocument != null) {
			//System.out.println("Got company in db");
			Company dbCompany = new Company(compDocument);
			
			Site matchedDbSite = null;
			for (Site dbSite : dbCompany.getSites()) {
				if (site.getName().equals(dbSite.getName())) {
					matchedDbSite = dbSite;
					break;
				}
			}
			
			Integer siteId = null;
			
			// Site not found in DB
			if (matchedDbSite == null) {
				//System.out.println("Site not found in DB");
				if (site.getId() == null) {
					site.setId(Site.getUniqueId());
				}
				dbCompany.addSite(site);
				siteId = site.getId();
			} else {
				//System.out.println("Site found in DB");
				// Merge Departments
				processDepartment(site, matchedDbSite);
				siteId = matchedDbSite.getId();
			}
			
			List<Document> siteDocs = new ArrayList<>();
			for (Site temp : dbCompany.getSites()) {
				siteDocs.add(temp.toMongoDocument());
			}
			
			try {
				companyCollection.updateOne(new Document(Company._ID_TAG, companyId),
						new Document("$set", new Document(Company.SITES_TAG, siteDocs)));
				System.out.println("Site Inserted");
				return siteId;
			} catch (Exception e) {
				System.out.println("Error inserting company " + e);
			}
		} else {
			System.out.println("Company Not found in DB");
		}

		//Document siteDoc = site.toMongoDocument();
		//companyCollection.updateOne(new Document(Company._ID_TAG, companyId),
		//		new Document("$push", new Document(Company.SITES_TAG, siteDoc)));

		return site.getId();
	}

	@Override
	public Integer addDepartment(ObjectId companyId, Integer siteId, Department department) {		
		Document compDocument = companyCollection.find(new Document(Company._ID_TAG, companyId)).first();
		
		if (compDocument != null) {
			Company dbCompany = new Company(compDocument);
						
			Site matchedDbSite = null;
			for (Site dbSite : dbCompany.getSites()) {
				if (dbSite.getId().equals(siteId)) {
					matchedDbSite = dbSite;
					break;
				}
			}
			
			if (matchedDbSite != null) {
				Department matchedDbDept = null;
				for (Department dbDept : matchedDbSite.getDepartments()) {
					if (dbDept.getName().equals(department.getName())) {
						matchedDbDept = dbDept;
						break;
					}
				}
				
				if (matchedDbDept == null) {
					if (department.getId() == null) {
						department.setId(Department.getUniqueId());
					}
					matchedDbSite.addDepartment(department);
					
					List<Document> siteDocs = new ArrayList<>();
					for (Site temp : dbCompany.getSites()) {
						siteDocs.add(temp.toMongoDocument());
					}
					
					try {
						companyCollection.updateOne(new Document(Company._ID_TAG, companyId),
								new Document("$set", new Document(Company.SITES_TAG, siteDocs)));
						System.out.println("Department Inserted");
						return department.getId();
					} catch (Exception e) {
						System.out.println("Error inserting company " + e);
					}
					
				} else {
					return matchedDbDept.getId(); 
				}
			}			
		}	
		
		return null;
		
		/*
		if (deptartment.getId() == null) {
			deptartment.setId(Department.getUniqueId());
		}
		Document deptartmentDoc = deptartment.toMongoDocument();
		Bson filter = and(eq(Company._ID_TAG, companyId), eq("sites." + Site.ID_TAG, siteId));
		companyCollection.updateOne(filter, new Document("$push", new Document(Site.DEPTS_TAG, deptartmentDoc)));
		return deptartment.getId();
		*/
	}

	@Override
	public List<Company> getCompanies() {
		List<Document> compDocumentList = companyCollection.find().into(new ArrayList<Document>());
		List<Company> companies = new ArrayList<>();

		if (compDocumentList != null && !compDocumentList.isEmpty()) {
			for (Document compDocument : compDocumentList) {
				companies.add(new Company(compDocument));
			}
		}

		return companies;
	}

	@Override
	public List<Site> getSites(ObjectId companyId) {
		Bson filter = eq(Company._ID_TAG, companyId);

		Document compDocument = companyCollection.find(filter).first();
		Company company = new Company(compDocument);
		return company.getSites();
	}

	@Override
	public List<Department> getDepartments(ObjectId companyId, Integer siteId) {
		Bson filter = and(eq(Company._ID_TAG, companyId), eq("sites." + Site.ID_TAG, siteId));

		Document compDocument = companyCollection.find(filter).first();

		System.out.println("compDocument:" + compDocument);

		if (compDocument != null) {
			Company company = new Company(compDocument);
			for (Site site : company.getSites()) {
				if (site.getId().equals(siteId)) {
					return site.getDepartments();
				}
			}
		}

		return new ArrayList<Department>();
	}

	@Override
	public void clearAll() {
		this.companyCollection.deleteMany(new Document());
	}

	@Override
	public Site getSite(ObjectId companyId, String siteName) {
		Document compDocument = companyCollection.find(new Document(Company._ID_TAG, companyId)).first();
		
		if (compDocument != null) {
			Company dbCompany = new Company(compDocument);
			
			for (Site dbSite : dbCompany.getSites()) {
				if (dbSite.getName().equals(siteName)) {
					return dbSite;
				}
			}
		} else {
			System.out.println("Company Not found in DB");
		}

		return null;
	}

	@Override
	public Department getDepartment(ObjectId companyId, Integer siteId, String deptName) {
		Document compDocument = companyCollection.find(new Document(Company._ID_TAG, companyId)).first();
		
		if (compDocument != null) {
			Company dbCompany = new Company(compDocument);
			
			for (Site dbSite : dbCompany.getSites()) {
				if (dbSite.getId().equals(siteId)) {
					for (Department dbDept : dbSite.getDepartments()) {
						if (dbDept.getName().equals(deptName)) {
							return dbDept;
						}
					}
				}
			}
		} else {
			System.out.println("Company Not found in DB");
		}

		return null;
	}

	@Override
	public Company getCompany(ObjectId companyId) {
		Bson filter = eq(Company._ID_TAG, companyId);

		Document compDocument = companyCollection.find(filter).first();
		if(compDocument != null) {
		    Company company = new Company(compDocument);
	            System.out.println("company returned:"+company);
	            return company;
		} else {
		    System.out.println("Company Not found in DB for companyId:"+companyId);
		}
		return null;
	}

}
