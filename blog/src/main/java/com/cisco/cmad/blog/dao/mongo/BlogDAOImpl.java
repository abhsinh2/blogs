package com.cisco.cmad.blog.dao.mongo;

import com.cisco.cmad.blog.dao.BlogDAO;
import com.cisco.cmad.blog.model.Comment;
import com.cisco.cmad.blog.util.Utility;
import com.cisco.cmad.blog.model.Blog;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import static com.mongodb.client.model.Filters.*;

import org.bson.Document;
import org.bson.conversions.Bson;

import java.util.ArrayList;
import java.util.List;

/**
 * Implementation for BlogDAO using MongoDB native API.
 * 
 * @author abhsinh2
 *
 */
public class BlogDAOImpl extends MongoDAO implements BlogDAO {
	private MongoCollection<Document> blogsCollection;

	public BlogDAOImpl(MongoDatabase mongoDatabase) {
		super(mongoDatabase);
		blogsCollection = mongoDatabase.getCollection(Utility.getMongoBlogCollection());
	}

	@Override
	public Blog findByUrlLink(String urllink) {
		// Need to add index urllink
		Document blogDocument = blogsCollection.find(new Document(Blog.URLLINK_TAG, urllink)).first();
		if (blogDocument != null) {
			return new Blog(blogDocument);
		}
		return null;
	}

	@Override
	public List<Blog> find(Integer limit, Boolean dateAscending) {
		int ascendingNum = 1;
		if (dateAscending == Boolean.FALSE) {
			ascendingNum = -1;
		}

		List<Document> blogDocumentList;
		// Need to add index for date
		if (limit == null || limit.intValue() == -1) {
			blogDocumentList = blogsCollection.find().sort(new Document(Blog.DATE_TAG, ascendingNum))
					.into(new ArrayList<Document>());
		} else {
			blogDocumentList = blogsCollection.find().sort(new Document(Blog.DATE_TAG, ascendingNum)).limit(limit)
					.into(new ArrayList<Document>());
		}

		// System.out.println("BlogDAOImpl.find blogdocument size:" +
		// blogDocumentList.size());

		List<Blog> blogs = new ArrayList<>();

		for (Document blogDocument : blogDocumentList) {
			blogs.add(new Blog(blogDocument));
		}

		// System.out.println("BlogDAOImpl.find blogs size:" + blogs.size());

		return blogs;
	}

	@Override
	public List<Blog> findByTag(String tag, Integer limit, Boolean dateAscending) {
		Bson filter = in(Blog.TAGS_TAG, tag);

		int ascendingNum = 1;
		if (dateAscending == Boolean.FALSE) {
			ascendingNum = -1;
		}

		// Need to add index for tags,date
		List<Document> blogDocumentList;
		if (limit == null || limit.intValue() == -1) {
			blogDocumentList = blogsCollection.find(filter).sort(new Document(Blog.DATE_TAG, ascendingNum))
					.into(new ArrayList<Document>());
		} else {
			blogDocumentList = blogsCollection.find(filter).sort(new Document(Blog.DATE_TAG, ascendingNum)).limit(limit)
					.into(new ArrayList<Document>());
		}

		List<Blog> blogs = new ArrayList<>();
		for (Document blogDocument : blogDocumentList) {
			blogs.add(new Blog(blogDocument));
		}
		return blogs;
	}

	@Override
	public List<Blog> findByAuthor(String author, Integer limit, Boolean dateAscending) {
		Bson filter = eq(Blog.AUTHOR_TAG, author);

		int ascendingNum = 1;
		if (dateAscending == Boolean.FALSE) {
			ascendingNum = -1;
		}

		// Need to add index author,date
		List<Document> blogDocumentList;
		if (limit == null || limit.intValue() == -1) {
			blogDocumentList = blogsCollection.find(filter).sort(new Document(Blog.DATE_TAG, ascendingNum))
					.into(new ArrayList<Document>());
		} else {
			blogDocumentList = blogsCollection.find(filter).sort(new Document(Blog.DATE_TAG, ascendingNum)).limit(limit)
					.into(new ArrayList<Document>());
		}

		List<Blog> blogs = new ArrayList<>();
		for (Document blogDocument : blogDocumentList) {
			blogs.add(new Blog(blogDocument));
		}
		return blogs;
	}

	@Override
	public String add(Blog blog) {
		Document blogDocument = blog.toMongoDocument();		
		String urllink = blogDocument.getString(Blog.URLLINK_TAG);
		
		try {
			blogsCollection.insertOne(blogDocument);
			System.out.println("blog inserted with urllink " + urllink);
		} catch (Exception e) {
			System.out.println("Error inserting blog");
		}

		return urllink;	
	}

	@Override
	public void addComment(Comment comment, String urllink) {
		Document commentDoc = comment.toMongoDocument();
		blogsCollection.updateOne(new Document(Blog.URLLINK_TAG, urllink),
				new Document("$push", new Document(Blog.COMMENTS_TAG, commentDoc)));
	}

	@Override
	public MongoCollection<Document> getCollection() {
		return blogsCollection;
	}

	@Override
	public void clearAll() {
		// this.blogsCollection.drop();
		this.blogsCollection.deleteMany(new Document());
	}

}
