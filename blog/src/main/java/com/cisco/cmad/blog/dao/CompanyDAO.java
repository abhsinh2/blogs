package com.cisco.cmad.blog.dao;

import java.util.List;

import org.bson.types.ObjectId;

import com.cisco.cmad.blog.model.Company;
import com.cisco.cmad.blog.model.Department;
import com.cisco.cmad.blog.model.Site;

/**
 * Data Access Object to handle Company, Site, Department.
 * 
 * @author abhsinh2
 *
 */
public interface CompanyDAO {
	/**
	 * Adds a company to database
	 * 
	 * @param company
	 * @return
	 */
	public ObjectId addCompany(Company company);

	/**
	 * Adds a site to given company
	 * 
	 * @param companyId
	 * @param site
	 * @return siteId
	 */
	public Integer addSite(ObjectId companyId, Site site);

	/**
	 * Adds a department to given companyId and siteId.
	 * 
	 * @param companyId
	 * @param site
	 * @param department
	 * @return departmentId
	 */
	public Integer addDepartment(ObjectId companyId, Integer site, Department department);
	
	/**
	 * Returns Company for a given companyId
	 * 
	 * @return
	 */
	public Company getCompany(ObjectId companyId);

	/**
	 * Returns list of all the sites
	 * 
	 * @return
	 */
	public List<Company> getCompanies();

	/**
	 * Returns list of sites for given companyId
	 * 
	 * @param companyId
	 * @return
	 */
	public List<Site> getSites(ObjectId companyId);
	
	/**
	 * Returns Site for given companyId and site name.
	 * 
	 * @param companyId
	 * @param siteName
	 * @return
	 */
	public Site getSite(ObjectId companyId, String siteName);

	/**
	 * Returns list of departments for given companyId and siteId
	 * 
	 * @param companyId
	 * @param siteId
	 * @return
	 */
	public List<Department> getDepartments(ObjectId companyId, Integer siteId);
	
	/**
	 * Returns Department for given companyId, siteId and department name.
	 * 
	 * @param companyId
	 * @param siteId
	 * @param deptName
	 * @return
	 */
	public Department getDepartment(ObjectId companyId, Integer siteId, String deptName);
	
	/**
	 * Removes all the companies
	 */
	public void clearAll();
}
