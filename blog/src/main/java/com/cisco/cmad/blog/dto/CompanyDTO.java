package com.cisco.cmad.blog.dto;

import java.util.ArrayList;
import java.util.List;

import com.cisco.cmad.blog.model.Company;
import com.cisco.cmad.blog.model.Site;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

/**
 * Represents Data Transfer Objects for Company.
 * 
 * @author abhsinh2
 *
 */
public class CompanyDTO {
	private String id;
	private String companyName;
	private List<SiteDTO> sites;

	public CompanyDTO() {

	}

	public CompanyDTO(Company company) {
		this.id = company.get_id().toString();
		this.companyName = company.getName();

		sites = new ArrayList<SiteDTO>();
		List<Site> companySites = company.getSites();
		for (Site site : companySites) {
			this.sites.add(new SiteDTO(site));
		}
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	
	public List<SiteDTO> getSites() {
            return sites;
        }
    
        public void setSites(List<SiteDTO> sites) {
                this.sites = sites;
        }

	public Company toModel() {
		Company company = new Company();
		company.setName(this.getCompanyName());

		List<Site> sites = new ArrayList<Site>();
		if (this.getSites() != null) {
			for (SiteDTO siteDTO : this.getSites()) {
				sites.add(siteDTO.toModel());
			}
			company.setSites(sites);
		}
		return company;
	}
	
	public JsonObject toJson() {
		Gson gson = new Gson();
		return gson.toJsonTree(this).getAsJsonObject();
	}
}
