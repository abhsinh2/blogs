package com.cisco.cmad.blog.dto;

import java.io.Serializable;
import java.util.Date;

import com.cisco.cmad.blog.model.Comment;
import com.cisco.cmad.blog.model.User;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

/**
 * Represents Data Transfer Objects for Comment in Blog.
 * 
 * @author abhsinh2
 *
 */
public class CommentDTO implements Serializable {
	private static final long serialVersionUID = 2023844206234549137L;

	private String content;
	private String email;
	private String author;
	private Date date;
	private String blogId;
	private String firstname = "";
	private String lastname = "";

	public CommentDTO() {

	}

	public CommentDTO(Comment comment) {
		this.content = comment.getContent();
		this.email = comment.getEmail();
		this.author = comment.getAuthor();
		this.date = comment.getDate();
	}
	
	public CommentDTO(Comment comment, User user) {
		this.content = comment.getContent();
		this.email = comment.getEmail();
		this.author = comment.getAuthor();
		this.date = comment.getDate();
		
		if (user != null) {
			this.firstname = user.getFirstname();
			this.lastname = user.getLastname();
		}		
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getBlogId() {
		return blogId;
	}

	public void setBlogId(String blogId) {
		this.blogId = blogId;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public Comment toModel() {
		Comment comment = new Comment();
		comment.setAuthor(this.getAuthor());
		comment.setContent(this.getContent());
		comment.setEmail(this.getEmail());

		if (this.getDate() == null) {
			comment.setDate(new Date());
		} else {
			comment.setDate(this.getDate());
		}

		return comment;
	}
	
	public JsonObject toJson() {
		Gson gson = new Gson();
		return gson.toJsonTree(this).getAsJsonObject();
	}	

	@Override
	public String toString() {
		return "CommentDTO [content=" + content + ", email=" + email + ", author=" + author + ", date=" + date
				+ ", blogId=" + blogId + ", userFirst=" + firstname + ", userLast=" + lastname + "]";
	}
}
