package com.cisco.cmad.blog.dao.mongo.orm;

import com.cisco.cmad.blog.dao.SessionDAO;
import com.cisco.cmad.blog.model.Session;

import org.mongodb.morphia.Datastore;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

/**
 * Implementation for SessionDAO using Morphia API.
 * 
 * @author abhsinh2
 *
 */
public class SessionDAOImpl extends MongoDAO implements SessionDAO {

	public SessionDAOImpl(Datastore dataStore) {
		super(dataStore);
	}

	public String findUserNameBySessionId(String sessionId) {
		Session session = getSession(sessionId);

		if (session == null) {
			return null;
		} else {
			return session.getUsername();
		}
	}

	public String startSession(String username) {
		// get 32 byte random number. that's a lot of bits.
		SecureRandom generator = new SecureRandom();
		byte randomBytes[] = new byte[32];
		generator.nextBytes(randomBytes);

		Base64.Encoder encoder = Base64.getEncoder();

		String sessionId = encoder.encodeToString(randomBytes);

		return startSession(username, sessionId);
	}

	@Override
	public String startSession(String username, String sessionId) {
		Session session = new Session();
		session.setUsername(username);
		session.setSessionId(sessionId);

		dataStore.delete(dataStore.createQuery(Session.class).field(Session.USERNAME_TAG).equal(username));
		dataStore.save(session);

		return sessionId;
	}

	@Override
	public String endSession(String sessionID) {
		Session session = getSession(sessionID);
		dataStore.delete(dataStore.createQuery(Session.class).field(Session._ID_TAG).equal(sessionID));
		return session.getUsername();
	}

	@Override
	public Session getSession(String sessionID) {
		List<Session> sessions = dataStore.createQuery(Session.class).field(Session._ID_TAG).equal(sessionID).asList();
		if (sessions == null || sessions.isEmpty()) {
			return null;
		} else {
			return sessions.get(0);
		}		
	}

	@Override
	public void clearAll() {
		dataStore.delete(dataStore.createQuery(Session.class));		
	}

	@Override
	public List<String> getAllSignedInUsernames() {
		List<Session> sessions = dataStore.createQuery(Session.class).asList();
		List<String> usernames = new ArrayList<String>();
		
		if (sessions != null && !sessions.isEmpty()) {
			for (Session session : sessions) {
				usernames.add(session.getUsername());
			}
		}
		return usernames;
	}
}
