package com.cisco.cmad.blog.dao.mongo;

import org.bson.Document;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

/**
 * Base class for DAOs to provide MongoDatabase.
 * 
 * @author abhsinh2
 *
 */
public abstract class MongoDAO {
	protected MongoDatabase mongoDatabase;

	public MongoDAO(MongoDatabase mongoDatabase) {
		this.mongoDatabase = mongoDatabase;
	}
	
	public MongoCollection<Document> getCollection() {
		return null;
	}
}
