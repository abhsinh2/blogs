package com.cisco.cmad.blog.dao;

import java.util.List;

import com.cisco.cmad.blog.model.Comment;
import com.cisco.cmad.blog.model.Blog;

/**
 * Data Access Object to handle Blog
 * 
 * @author abhsinh2
 *
 */
public interface BlogDAO {

	/**
	 * Returns a blog for given urllink.
	 * 
	 * @param urllink
	 *            generated urllink by application. e.g.
	 *            http://host:port/app/blogs/<urllink>
	 * @return a blog for given urllink.
	 */
	public Blog findByUrlLink(String urllink);

	/**
	 * Find list of all the blogs with ascending/descending dates and limits
	 * 
	 * @param limit
	 * @param dateAscending
	 * @return
	 */
	public List<Blog> find(Integer limit, Boolean dateAscending);

	/**
	 * Find list of all the blogs with given tags and ascending/descending dates
	 * 
	 * @param tag
	 * @param dateAscending
	 * @return
	 */
	public List<Blog> findByTag(String tag, Integer limit, Boolean dateAscending);

	/**
	 * Find list of all the blogs for given author and ascending/descending
	 * dates
	 * 
	 * @param author
	 * @param dateAscending
	 * @return
	 */
	public List<Blog> findByAuthor(String author, Integer limit, Boolean dateAscending);

	/**
	 * Adds a blog with given Blog object
	 * 
	 * @param blog
	 * 
	 * @return urllink
	 */
	public String add(Blog blog);

	/**
	 * Adds a comment for blog having urllink.
	 * 
	 * @param comment
	 * @param urllink
	 */
	public void addComment(Comment comment, String urllink);

	/**
	 * Removes all the blogs
	 */
	public void clearAll();
}
