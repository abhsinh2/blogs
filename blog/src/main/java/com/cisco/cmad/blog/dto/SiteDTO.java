package com.cisco.cmad.blog.dto;

import java.util.ArrayList;
import java.util.List;

import com.cisco.cmad.blog.model.Department;
import com.cisco.cmad.blog.model.Site;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

/**
 * Represents Data Transfer Objects for Site in Company.
 * 
 * @author abhsinh2
 *
 */
public class SiteDTO {
	private Integer id;
	private String siteName;
	private List<DepartmentDTO> departments;

	public SiteDTO() {

	}

	public SiteDTO(Site site) {
		this.id = site.getId();
		this.siteName = site.getName();
		departments = new ArrayList<DepartmentDTO>();
		List<Department> siteDepartments = site.getDepartments();
		for (Department department : siteDepartments) {
			this.departments.add(new DepartmentDTO(department));
		}
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSiteName() {
		return siteName;
	}

	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}
	
	public List<DepartmentDTO> getDepartments() {
            return departments;
        }
    
        public void setDepartments(List<DepartmentDTO> departmentDTOs) {
                this.departments = departmentDTOs;
        }

	public Site toModel() {
		Site site = new Site();
		site.setName(this.getSiteName());
		
		List<Department> depts = new ArrayList<Department>();
		if (this.getDepartments() != null) {
			for (DepartmentDTO deptDTO : this.getDepartments()) {
				depts.add(deptDTO.toModel());
			}
			site.setDepartments(depts);
		}
		
		return site;
	}

	public JsonObject toJson() {
		Gson gson = new Gson();
		return gson.toJsonTree(this).getAsJsonObject();
	}
}
