package com.cisco.cmad.blog.dao.mongo.orm;

import com.cisco.cmad.blog.dao.UserDAO;
import com.cisco.cmad.blog.model.User;
import com.cisco.cmad.blog.util.Utility;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Key;

import java.security.SecureRandom;
import java.util.List;
import java.util.Random;

/**
 * Implementation for UserDAO using Morphia API.
 * 
 * @author abhsinh2
 *
 */
public class UserDAOImpl extends MongoDAO implements UserDAO {
	private Random random = new SecureRandom();

	public UserDAOImpl(Datastore dataStore) {
		super(dataStore);
	}

	@Override
	public boolean add(User user) {
		if (user == null || user.getUsername() == null || user.getPassword() == null) {
			return false;
		}
		
		String passwordHash = Utility.encodePassword(user.getPassword(), Integer.toString(random.nextInt()));
		user.setPassword(passwordHash);

		System.out.println("UserDAOImpl.add user:" + user);
		
		if (dataStore.exists(user) != null) {
			return false;
		}
		
		Key<User> key = dataStore.save(user);

		System.out.println(key);
		return true;		
	}

	@Override
	public User validateLogin(String username, String password) {
		List<User> users = dataStore.createQuery(User.class).field(User._ID_TAG).equal(username).asList();
		if (users.size() != 0) {
			User user = users.get(0);
			
			System.out.println("UserDAOImpl.validateLogin user:" + user);
			
			String hashedAndSalted = user.getPassword();
			
			String salt = hashedAndSalted.split(",")[1];
			String hashedPassword = Utility.encodePassword(password, salt);	
			
			System.out.println("UserDAOImpl.validateLogin hashedPassword:" + hashedPassword);
			
			if (!hashedAndSalted.equals(hashedPassword)) {
				System.out.println("Submitted password is not a match");
				return null;
			}

			return user;
		} else {
			System.out.println("UserDAOImpl.validateLogin No users found for " + username);
		}

		return null;
	}
	
	@Override
	public User findByUsername(String username) {
		List<User> users = dataStore.createQuery(User.class).field(User._ID_TAG).equal(username).asList();
		if (users.size() != 0) {
			User user = users.get(0);
			return user;
		}
		return null;
	}

	@Override
	public void clearAll() {
		dataStore.delete(dataStore.createQuery(User.class));		
	}

}
