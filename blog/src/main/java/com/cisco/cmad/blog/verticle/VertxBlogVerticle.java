package com.cisco.cmad.blog.verticle;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.cisco.cmad.blog.dao.SessionDAO;
import com.cisco.cmad.blog.util.Utility;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.http.ServerWebSocket;
import io.vertx.core.shareddata.LocalMap;
import io.vertx.core.shareddata.SharedData;
import io.vertx.ext.auth.AuthProvider;
import io.vertx.ext.web.Cookie;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.Session;
import io.vertx.ext.web.handler.BasicAuthHandler;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.CookieHandler;
import io.vertx.ext.web.handler.FormLoginHandler;
import io.vertx.ext.web.handler.LoggerFormat;
import io.vertx.ext.web.handler.LoggerHandler;
import io.vertx.ext.web.handler.SessionHandler;
import io.vertx.ext.web.handler.StaticHandler;
import io.vertx.ext.web.handler.UserSessionHandler;
import io.vertx.ext.web.sstore.LocalSessionStore;
import io.vertx.ext.web.sstore.SessionStore;

/**
 * Abstract Vertx Verticle to define all the routes.
 * 
 * @author abhsinh2
 *
 */
public abstract class VertxBlogVerticle extends AbstractVerticle {

	public static final String BLOG_SESSION = "blogSession";
	public static final String BLOG_COOKIE = "blogCookie";
	
	protected AuthProvider authProvider;
	protected SessionDAO sessionDAO;

	public VertxBlogVerticle() {
		
	}
	
	public VertxBlogVerticle(SessionDAO sessionDAO) {
		this.sessionDAO = sessionDAO;
	}

	public VertxBlogVerticle(SessionDAO sessionDAO, AuthProvider authProvider) {
		this.sessionDAO = sessionDAO;
		this.authProvider = authProvider;
	}

	@Override
	public void start(Future<Void> fut) {
		startWebApp((http) -> completeStartup(http, fut));
	}

	@Override
	public void stop() throws Exception {
		System.out.println("Stop");
	}

	/**
	 * Defines all the routes
	 * 
	 * @param next
	 */
	protected void startWebApp(Handler<AsyncResult<HttpServer>> next) {		
		
		// Create a router object.
		Router router = Router.router(vertx);	

		initStaticHandler(router);
		initCookieHandler(router);
		initSessionHandler(router);
		initAuthHandler(router);
		
		defineHomeServices(router);
		defineBlogWebServices(router);
		defineAuthenticationWebServices(router);
		defineErrorHandlingWebServices(router);
		defineCompaniesWebServices(router);
		//defineWebSocketServices(router);
		
		// Create the HTTP server and pass the "accept" method to the request
		// handler.		
		/*
		if (Utility.getHttpsPort() != null) {
			HttpServerOptions options = new HttpServerOptions();
			options.setSsl(true);
			options.setPort(Utility.getHttpPort());
			options.setHost("localhost");		
			String jksPath = System.getProperty("user.dir") + "/src/main/resources/conf/server-keystore.jks";		
			options.setKeyStoreOptions(new JksOptions().setPath(jksPath).setPassword("wibble"));
			
			vertx.createHttpServer(options).requestHandler(router::accept).listen(
					Utility.getHttpsPort(), next::handle);
		} else {
			vertx.createHttpServer().requestHandler(router::accept).listen(
					Utility.getHttpPort(), next::handle);
		}*/
		
		HttpServer httpServer = vertx.createHttpServer();
		httpServer.requestHandler(router::accept);
		httpServer.websocketHandler(this::chatHandler);
		httpServer.listen(Utility.getHttpPort(), next::handle);	
	}

	protected void completeStartup(AsyncResult<HttpServer> http, Future<Void> startFuture) {
		if (http.succeeded()) {
			startFuture.complete();
		} else {
			startFuture.fail(http.cause());
		}
	}
	
	private void defineHomeServices(Router router) {
		router.get("/").handler(this::homepage);
	}

	private void defineBlogWebServices(Router router) {
		// handle the new blog submission
		router.post("/services/blogs/newblog").handler(this::addNewBlog);

		// get all the blogs
		router.get("/services/blogs").handler(this::getAllBlogs);		

		// get a blog with given urllink
		router.get("/services/blogs/:urllink").handler(this::getBlog);

		// Get all the blogs in system for given username
		router.get("/services/blogs/user/:username").handler(this::getBlogsForUser);

		// handle the new comment submission
		router.post("/services/blogs/:urllink/newcomment").handler(this::addNewComment);

		// get all the blogs for given tag
		router.get("/services/blogs/tag/:thetag").handler(this::getBlogsByTag);
				
		// deletes a blog
		router.delete("/services/blogs/:urllink").handler(this::deleteBlog);
	}

	private void defineAuthenticationWebServices(Router router) {
		// login user
		router.post("/services/users/login").handler(this::login);

		// handles the signup
		router.post("/services/users/signup").handler(this::addNewUser);

		// allows the user to logout
		router.get("/services/users/logout").handler(this::logout);
		
		// checks if user is signed in. used in websocket
		router.get("/services/rest/user").handler(this::getAllSignedInUsers);
	}

	private void defineCompaniesWebServices(Router router) {
		// Get all the companies
		router.get("/services/companies").handler(this::getCompanies);
		// Adds a Company
		router.post("/services/companies").handler(this::addCompany);
		
		// Get the company for a given companyId
		router.get("/services/companies/:companyId").handler(this::getCompany);
		
		// Get all the sites for given company
		router.get("/services/companies/:companyId/sites").handler(this::getSites);
		// Adds a Site
		router.post("/services/companies/:companyId/sites").handler(this::addSite);
		
		// Get all the departments for given company and site
		router.get("/services/companies/:companyId/sites/:siteId/departments").handler(this::getDepartments);
		// Adds a Department
		router.post("/services/companies/:companyId/sites/:siteId/departments").handler(this::addDepartment);
	}
	
	private void defineErrorHandlingWebServices(Router router) {
		// tells the user that the blog is dead
		router.get("/services/blogs/blog_not_found").handler(this::blog_not_found);
		
		// used to process internal errors
		router.get("/services/internal_error").handler(this::internalError);
	}

	protected void initStaticHandler(Router router) {
		// if there was a request with path /static/css/mystyles.css then
		// the static serve will look for a file in the directory
		// webroot/static/css/mystyle.css		
		// router.route("/static/*").handler(StaticHandler.create());
		
		// setMaxAgeSeconds: cache-control is set to max-age=86400 by default.
		// This corresponds to one day. This can be configured
		// setCachingEnabled: If handling of cache headers is not required
		// setIndexPage:
		// setWebRoot

		router.route("/blogger/*").handler(StaticHandler.create("webroot").setCachingEnabled(false));
		//router.route().handler(StaticHandler.create());

		// A handler which gathers the entire request body and sets it on the
		// RoutingContext.
		// It also handles HTTP file uploads and can be used to limit body
		// sizes.
		router.route("/services/blogs/*").handler(BodyHandler.create());
		router.route("/services/users/*").handler(BodyHandler.create());
		router.route("/services/companies/*").handler(BodyHandler.create());
		//router.route().handler(BodyHandler.create());
	}
	
	protected void initCookieHandler(Router router) {
		CookieHandler cookieHandler = CookieHandler.create();
		
		router.route().handler(cookieHandler);
		router.route("/blogger/*").handler(cookieHandler);
		router.route("/services/*").handler(cookieHandler);
		router.route("/services/blogs/*").handler(cookieHandler);
		router.route("/services/blogs/newblog").handler(cookieHandler);
		router.route("/services/blogs/*/newcomment").handler(cookieHandler);
		router.route("/services/users/*").handler(cookieHandler);
	}

	protected void initSessionHandler(Router router) {
		// For Single Instance of Vert.x
		SessionStore store = LocalSessionStore.create(vertx);

		// For Multiple Instances of Vert.x
		// Create a clustered session store using defaults
		// SessionStore store = ClusteredSessionStore.create(vertx);

		SessionHandler sessionHandler = SessionHandler.create(store);
		sessionHandler.setSessionTimeout(15 * 60 * 1000);
		
		// Make sure all requests are routed through the session handler too
		router.route().handler(sessionHandler);
		router.route("/blogger/*").handler(sessionHandler);
		router.route("/services/*").handler(sessionHandler);
		router.route("/services/blogs/*").handler(sessionHandler);
		router.route("/services/blogs/newblog").handler(sessionHandler);
		router.route("/services/blogs/*/newcomment").handler(sessionHandler);
		router.route("/services/users/*").handler(sessionHandler);
	}

	protected void initAuthHandler(Router router) {
		if (this.authProvider != null) {
			
			// We need a user session handler too to make sure the user is stored in the session between requests
			router.route().handler(UserSessionHandler.create(this.authProvider));

			// Any requests to URI starting '/private/' require login
			// If we need to redirect to login page
		    //router.route("/private/*").handler(RedirectAuthHandler.create(authProvider, "/blogger/index.html#/login"));
			
			// Basic Auth Handler
		    // All requests to paths starting with '/private/' will be protected
		    router.route("/private/*").handler(BasicAuthHandler.create(this.authProvider));
		    
		    // Serve the static private pages from directory 'private'
		    router.route("/private/*").handler(StaticHandler.create("private").setCachingEnabled(false));

		    router.route("/loginhandler").handler(FormLoginHandler.create(this.authProvider));
		    
			// Implement logout
			router.route("/logout").handler(context -> {
				System.out.println("Form Logout");
				context.clearUser();
				// Redirect back to the index page
				context.response().putHeader("location", "/").setStatusCode(302).end();
			});
		}
	}
	
	private static LocalMap<String, String> wsChatSessions;
	
	protected void chatHandler(final ServerWebSocket ws) {
		System.out.println("VertxBlogVerticle.chatHandler");
		Pattern chatUrlPattern = Pattern.compile("/services/chat/(\\w+)");
		Matcher m = chatUrlPattern.matcher(ws.path());
				
		if (!m.matches()) {
			ws.reject();
			return;
		} 		
		
		final String chatRoom = m.group(1);
		final String id = ws.textHandlerID();
		System.out.println("registering new connection with id: " + id + " for chat-room: " + chatRoom);
		
		SharedData sd = vertx.sharedData();
        wsChatSessions = sd.getLocalMap("ws.chat.sessions");        
        wsChatSessions.put(ws.textHandlerID(), ws.toString());		
 
		ws.closeHandler(ch -> {
			String vertx_session_key = "vertx-web.session";
			String sessionId = null;
			for (String cookie : ws.headers().getAll(HttpHeaders.COOKIE)) {
				if (cookie.startsWith(vertx_session_key)) {
					sessionId = cookie.substring(vertx_session_key.length() + 1, cookie.length());
					break;
				}
			}
			
			if (sessionId != null) {
				System.out.println("Removing session id " + sessionId);
				if (sessionDAO != null) {
					sessionDAO.endSession(sessionId);
				}
			}
			
            System.out.println("Closing ws-connection to client " + ws.textHandlerID());
            wsChatSessions.remove(ws.textHandlerID());
        });		
 
		ws.handler(new Handler<Buffer>(){
            @Override
            public void handle(final Buffer data) {
                String message = data.getString(0, data.length());
                System.out.println("Message from ws-client " + ws.textHandlerID() 
                        + ": " + message);
                
                JsonObject receivedObj = new JsonParser().parse(message).getAsJsonObject();
                
                for (String textHandlerID : wsChatSessions.keySet()) {
                	System.out.println("Sending to " + textHandlerID);
                	
                	JsonObject rootObj = new JsonObject();
    				rootObj.addProperty("messageType", receivedObj.get("event").getAsString());
    				
    				JsonObject msgObj = new JsonObject();
    				msgObj.addProperty("text", receivedObj.get("data").getAsString());
    				msgObj.addProperty("sender", receivedObj.get("data").getAsString());
    				
    				rootObj.add("messageObject", msgObj);
    				
                	vertx.eventBus().send(textHandlerID, rootObj.toString());
                }
            }
        });		
	}
	
	protected void sendMessageOverWebSocket(String message) {
		if (wsChatSessions != null) {
			for (String textHandlerID : wsChatSessions.keySet()) {
	        	System.out.println("Sending to " + textHandlerID);				
	        	vertx.eventBus().send(textHandlerID, message);
	        }
		}		
	}

	protected void initLoggerHandler(Router router) {
		LoggerHandler loggerHandler = LoggerHandler.create(LoggerFormat.DEFAULT);
		router.route().handler(loggerHandler);
	}
	
	protected void homepage(RoutingContext routingContext) {
		Session session = routingContext.session();
		session.destroy();
		routingContext.clearUser();
		routingContext.response().end("<h1>Hello Blogger</h1>");
	}

	/**
	 * this method will be called when adding a new blog. Implementation should
	 * extract BlogDTO, get username, create Blog and save it.
	 * 
	 * @param routingContext
	 */
	protected abstract void addNewBlog(RoutingContext routingContext);

	/**
	 * this method will be called when retrieving a blog
	 * 
	 * @param routingContext
	 */
	protected abstract void getBlog(RoutingContext routingContext);
	
	/**
	 * this method will be called when retrieving all the blogs
	 * 
	 * @param routingContext
	 */
	protected abstract void getAllBlogs(RoutingContext routingContext);

	/**
	 * this method will be called when retrieving all blogs for given user
	 * Implementation should retrieve :username and find all the blogs.
	 * 
	 * @param routingContext
	 */
	protected abstract void getBlogsForUser(RoutingContext routingContext);

	/**
	 * this method will be called when deleting a blog. Implementation should
	 * retrieve :urllink, and delete the blog.
	 * 
	 * @param routingContext
	 */
	protected abstract void deleteBlog(RoutingContext routingContext);

	/**
	 * this method will be called when adding a new comment to blog.
	 * 
	 * @param routingContext
	 */
	protected abstract void addNewComment(RoutingContext routingContext);

	/**
	 * this method will be called when user logs in. Implementation will receive
	 * UserDTO from which userName and password need to retrieved and used to
	 * validate user.
	 * 
	 * @param routingContext
	 */
	protected abstract void login(RoutingContext routingContext);

	/**
	 * this method will be called when new user registers
	 * 
	 * @param routingContext
	 */
	protected abstract void addNewUser(RoutingContext routingContext);

	/**
	 * this method will be called when user logs out
	 * 
	 * @param routingContext
	 */
	protected abstract void logout(RoutingContext routingContext);

	/**
	 * this method will be called to retrieve blog by given tag Implementation
	 * should look for :thetag and retrieve all the blogs for given tag.
	 * 
	 * @param routingContext
	 */
	protected abstract void getBlogsByTag(RoutingContext routingContext);

	/**
	 * this method will be called when blog is not found
	 * 
	 * @param routingContext
	 */
	protected abstract void blog_not_found(RoutingContext routingContext);

	/**
	 * this method will be called for any internal exception
	 * 
	 * @param routingContext
	 */
	protected abstract void internalError(RoutingContext routingContext);
	
	/**
	 * this method will be called to retrieve all the companies
	 * 
	 * @param routingContext
	 */
	protected abstract void getCompanies(RoutingContext routingContext);
	
	
	/**
	 * this method will be called to retrieve the company for a given companyId as :companyId
	 * 
	 * @param routingContext
	 */
	protected abstract void getCompany(RoutingContext routingContext);
	
	/**
	 * this method will be called to retrieve all the sites for given companyId as :companyId
	 * 
	 * @param routingContext
	 */
	protected abstract void getSites(RoutingContext routingContext);
	
	/**
	 * this method will be called to retrieve all the departments for given companyId as :companyId and siteId as :sideId
	 * 
	 * @param routingContext
	 */
	protected abstract void getDepartments(RoutingContext routingContext);
	
	/**
	 * this method will be called to add a Company
	 * 
	 * @param routingContext
	 */
	protected abstract void addCompany(RoutingContext routingContext);
	
	/**
	 * this method will be called to add a Site given companyId as :companyId
	 * 
	 * @param routingContext
	 */
	protected abstract void addSite(RoutingContext routingContext);
	
	/**
	 * this method will be called to add a Department for given companyId as :companyId and siteId as :sideId
	 * 
	 * @param routingContext
	 */
	protected abstract void addDepartment(RoutingContext routingContext);
	
	/**
	 * Checks if user is signed in.
	 * 
	 * @param routingContext
	 */
	protected abstract void getAllSignedInUsers(RoutingContext routingContext);
	
	protected String verifySessionInRequest(RoutingContext routingContext, SessionDAO sessionDAO) {
		HttpServerResponse response = routingContext.response();

		String sessionId = getSessionId(routingContext);

		//System.out.println("BlockedDBVertxBlogVerticle.verifySessionInRequest sessionId:" + sessionId);
		
		if (sessionId == null) {
			response.setStatusCode(401);			
			return null;
		}

		// TODO: Need to see if we can make it non-blocking
		String username = sessionDAO.findUserNameBySessionId(sessionId);

		if (username != null) {
			System.out.println("VertxBlogVerticle.verifySessionInRequest username:" + username);
		} else {
			System.out.println("VertxBlogVerticle.verifySessionInRequest username was null");
		}

		if (username == null) {
			response.setStatusCode(401);
		}

		return username;
	}

	protected String getSessionId(RoutingContext routingContext) {
		//System.out.println("VertxBlogVerticle.getSessionId " + routingContext.cookies());
		Cookie cookie = routingContext.getCookie(BLOG_COOKIE);
		if (cookie == null) {
			//System.out.println("VertxBlogVerticle.getSessionId cookie is null");
			Session session = routingContext.session();
			
			if (session == null) {
				//System.out.println("VertxBlogVerticle.getSessionId session is null");
				return null;
			} else {
				//System.out.println("VertxBlogVerticle.getSessionId blogSession " + session.get(BLOG_SESSION));
				return session.get(BLOG_SESSION);
			}
		} else {
			//System.out.println("VertxBlogVerticle.getSessionId " + cookie.getValue());
			return cookie.getValue();
		}
	}

	protected boolean handleLogin(String username, SessionDAO sessionDAO, RoutingContext routingContext) {
		Session session = routingContext.session();
		String sessionId = session.id();
		System.out.println("handleLogin:" + sessionId);
		
		sessionDAO.startSession(username, session.id());
		
		//System.out.println("VertxBlogVerticle.handleLoginUsingSession sessionID:" + session.id());
		//System.out.println("VertxBlogVerticle.handleLoginUsingSession sessionId:" + sessionId);
		
		if (sessionId == null) {
			System.out.println("VertxBlogVerticle.handleLoginUsingSession sessionID is null. Server Error");
		} else {
			//System.out.println("VertxBlogVerticle.handleLoginUsingSession Adding Session");
			routingContext.addCookie(Cookie.cookie(BLOG_COOKIE, sessionId));
			session.put(BLOG_SESSION, sessionId);
			//System.out.println("VertxBlogVerticle.handleLoginUsingSession sessionId " + session.get(BLOG_SESSION));
			return true;
		}
		return false;
	}
	
	protected String handleLogout(SessionDAO sessionDAO, RoutingContext routingContext) {
		String sessionId = this.getSessionId(routingContext);
		Session session = routingContext.session();
		
		System.out.println("handleLogout:" + sessionId + "::" + session.id());
		
		if (sessionId == null) {
			sessionId = session.id();
		}
		
		if (sessionId != null) {
			// deletes from session table
			String username = sessionDAO.endSession(sessionId);
			
			// this should delete the cookie
			session.remove(BLOG_SESSION);
			session.destroy();

			// this should delete the cookie
			Cookie c = routingContext.removeCookie(BLOG_COOKIE);
			c.setMaxAge(0);
			routingContext.addCookie(c);
			
			return username;
		}
		
		return null;
	}
}
