package com.cisco.cmad.blog.dao.mongo.orm;

import java.util.List;

import org.bson.types.ObjectId;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Key;
import com.cisco.cmad.blog.dao.CompanyDAO;
import com.cisco.cmad.blog.model.Company;
import com.cisco.cmad.blog.model.Department;
import com.cisco.cmad.blog.model.Site;

/**
 * Implementation for CompanyDAO using Morphia API.
 * 
 * @author abhsinh2
 *
 */
public class CompanyDAOImpl extends MongoDAO implements CompanyDAO {

	public CompanyDAOImpl(Datastore dataStore) {
		super(dataStore);
	}

	@Override
	public ObjectId addCompany(Company company) {
		Company dbcompany = dataStore.createQuery(Company.class).field("name").equal(company.getName()).get();

		if (dbcompany == null) {
			System.out.println("Company " + company.getName() + " does not exists in db");
			List<Site> sites = company.getSites();
			if (sites != null && !sites.isEmpty()) {
				for (Site site : sites) {
					if (site.getId() == null) {
						site.setId(Site.getUniqueId());
					}

					List<Department> depts = site.getDepartments();
					if (depts != null && !depts.isEmpty()) {
						for (Department dept : depts) {
							if (dept.getId() == null) {
								dept.setId(Department.getUniqueId());
							}
						}
					}
				}
			}

			Key<Company> key = dataStore.save(company);
			return (ObjectId) key.getId();
		} else {
			System.out.println("Company " + company.getName() + " exists in db");
			processSite(company, dbcompany);
			Key<Company> key = dataStore.save(dbcompany);
			return (ObjectId) key.getId();
		}
	}

	private void processSite(Company company, Company dbCompany) {
		List<Site> sites = company.getSites();

		if (sites != null) {
			for (Site site : sites) {
				Site matchedDbSite = null;

				// Check if given site available in DB
				for (Site dbSite : dbCompany.getSites()) {
					if (site.getName().equals(dbSite.getName())) {
						matchedDbSite = dbSite;
						break;
					}
				}

				if (matchedDbSite == null) {
					// Site was not in DB
					if (site.getId() == null) {
						site.setId(Site.getUniqueId());
					}
					System.out.println("Site " + site.getName() + " does not exists in db");
					dbCompany.addSite(site);
				} else {
					// Site was in DB
					processDepartment(site, matchedDbSite);
				}
			}
		}
	}

	private void processDepartment(Site site, Site dbSite) {
		List<Department> departments = site.getDepartments();

		if (departments != null) {
			for (Department dept : departments) {
				Department matchedDbDept = null;

				// Check if given dept available in DB
				for (Department dbDept : dbSite.getDepartments()) {
					if (dept.getName().equals(dbDept.getName())) {
						matchedDbDept = dbDept;
						break;
					}
				}

				if (matchedDbDept == null) {
					// Department was not in DB
					if (dept.getId() == null) {
						dept.setId(Department.getUniqueId());
					}
					System.out.println("Department " + dept.getName() + " does not exists in db");
					dbSite.addDepartment(dept);
				}
			}
		}
	}

	@Override
	public Integer addSite(ObjectId companyId, Site site) {
		Company company = dataStore.createQuery(Company.class).field(Company._ID_TAG).equal(companyId).get();
		if (company != null) {
			
			Site matchedDbSite = null;
			for (Site dbSite : company.getSites()) {
				if (site.getName().equals(dbSite.getName())) {
					matchedDbSite = dbSite;
					break;
				}
			}
			
			if (matchedDbSite == null) {
				if (site.getId() == null) {
					site.setId(Site.getUniqueId());
				}
				company.addSite(site);
				dataStore.save(company);
				return site.getId();
			} else {
				return matchedDbSite.getId();
			}			
		}

		return null;
	}

	@Override
	public Integer addDepartment(ObjectId companyId, Integer siteId, Department department) {
		// Query<Company> query = dataStore.createQuery(Company.class);
		// query.and(query.criteria(Company._ID_TAG).equal(companyId),
		// query.criteria("sites." + Site.ID_TAG).equal(siteId));

		Company company = dataStore.createQuery(Company.class).field(Company._ID_TAG).equal(companyId).get();
		if (company != null) {
			for (Site site : company.getSites()) {
				if (site.getId().equals(siteId)) {
					
					Department matchDbDept = null;
					for (Department dbDept : site.getDepartments()) {
						if (department.getName().equals(dbDept.getName())) {
							matchDbDept = dbDept;
							break;
						}
					}
					
					if (matchDbDept == null) {
						if (department.getId() == null) {
							department.setId(Department.getUniqueId());
						}
						site.addDepartment(department);
						dataStore.save(company);
						return department.getId();
					} else {
						return matchDbDept.getId();
					}					
				}
			}
		}

		return null;
	}

	@Override
	public List<Company> getCompanies() {
		return dataStore.createQuery(Company.class).asList();
	}

	@Override
	public List<Site> getSites(ObjectId companyId) {
		Company company = dataStore.createQuery(Company.class).field(Company._ID_TAG).equal(companyId).get();

		if (company == null) {
			return null;
		} else {
			return company.getSites();
		}
	}

	@Override
	public List<Department> getDepartments(ObjectId companyId, Integer siteId) {
		Company company = dataStore.createQuery(Company.class).field(Company._ID_TAG).equal(companyId).get();
		if (company != null) {
			for (Site site : company.getSites()) {
				if (site.getId().equals(siteId)) {
					return site.getDepartments();
				}
			}
		}
		return null;
	}

	@Override
	public void clearAll() {
		dataStore.delete(dataStore.createQuery(Company.class));
	}

	@Override
	public Site getSite(ObjectId companyId, String siteName) {
		Company company = dataStore.createQuery(Company.class).field(Company._ID_TAG).equal(companyId).get();
		if (company != null) {			
			for (Site dbSite : company.getSites()) {
				if (dbSite.getName().equals(siteName)) {
					return dbSite;
				}
			}			
		}
		return null;
	}

	@Override
	public Department getDepartment(ObjectId companyId, Integer siteId, String deptName) {
		Company company = dataStore.createQuery(Company.class).field(Company._ID_TAG).equal(companyId).get();
		if (company != null) {			
			for (Site dbSite : company.getSites()) {
				if (dbSite.getId().equals(siteId)) {
					for (Department dbDept : dbSite.getDepartments()) {
						if (dbDept.getName().equals(deptName)) {
							return dbDept;
						}
					}
				}
			}			
		}
		return null;
	}

	@Override
	public Company getCompany(ObjectId companyId) {
		Company company = dataStore.createQuery(Company.class).field(Company._ID_TAG).equal(companyId).get();
		if (company == null) {
			return null;
		} else {
			return company;
		}
	}

}
