package com.cisco.cmad.blog.dao.mongo.orm;

import com.cisco.cmad.blog.dao.BlogDAO;
import com.cisco.cmad.blog.model.Comment;
import com.cisco.cmad.blog.model.Blog;
import org.mongodb.morphia.Datastore;

import java.util.ArrayList;
import java.util.List;

/**
 * Implementation for BlogDAO using Morphia API.
 * 
 * @author abhsinh2
 *
 */
public class BlogDAOImpl extends MongoDAO implements BlogDAO {

	public BlogDAOImpl(Datastore dataStore) {
		super(dataStore);
	}

	@Override
	public Blog findByUrlLink(String urllink) {
		List<Blog> blogs = dataStore.createQuery(Blog.class).field(Blog.URLLINK_TAG).equal(urllink).asList();

		if (blogs == null || blogs.isEmpty()) {
			return null;
		} else {
			return blogs.get(0);
		}
	}

	@Override
	public List<Blog> find(Integer limit, Boolean dateAscending) {
		String dateOrder = null;
		if (dateAscending == Boolean.TRUE) {
			dateOrder = Blog.DATE_TAG;
		} else {
			dateOrder = "-" + Blog.DATE_TAG;
		}

		if (limit == null || limit.intValue() == -1) {
			return dataStore.createQuery(Blog.class).order(dateOrder).asList();
		} else {
			return dataStore.createQuery(Blog.class).order(dateOrder).limit(limit).asList();
		}
	}

	@Override
	public List<Blog> findByTag(String tag, Integer limit, Boolean dateAscending) {		
		String dateOrder = null;
		if (dateAscending) {
			dateOrder = Blog.DATE_TAG;
		} else {
			dateOrder = "-" + Blog.DATE_TAG;
		}

		List<String> tags = new ArrayList<>();
		tags.add(tag);

		if (limit == null || limit.intValue() == -1) {
			return dataStore.createQuery(Blog.class).field(Blog.TAGS_TAG).in(tags).order(dateOrder).asList();
		} else {
			return dataStore.createQuery(Blog.class).field(Blog.TAGS_TAG).in(tags).order(dateOrder).limit(limit)
					.asList();
		}
	}

	@Override
	public List<Blog> findByAuthor(String author, Integer limit, Boolean dateAscending) {
		String dateOrder = null;
		if (dateAscending) {
			dateOrder = Blog.DATE_TAG;
		} else {
			dateOrder = "-" + Blog.DATE_TAG;
		}

		if (limit == null || limit.intValue() == -1) {
			return dataStore.createQuery(Blog.class).field(Blog.AUTHOR_TAG).equal(author).order(dateOrder).asList();
		} else {
			return dataStore.createQuery(Blog.class).field(Blog.AUTHOR_TAG).equal(author).order(dateOrder).limit(limit)
					.asList();
		}
	}

	@Override
	public String add(Blog blog) {
		blog.setUrllink(blog.createUrllink());

		dataStore.save(blog);

		return blog.getUrllink();
	}

	@Override
	public void addComment(Comment comment, String urllink) {
		Blog blog = this.findByUrlLink(urllink);
		if (blog != null) {
			blog.addComment(comment);
			dataStore.save(blog);
		}
	}

	@Override
	public void clearAll() {
		dataStore.delete(dataStore.createQuery(Blog.class));
	}

}
