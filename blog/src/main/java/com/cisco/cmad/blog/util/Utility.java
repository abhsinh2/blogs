package com.cisco.cmad.blog.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;

/**
 * Utility class.
 * 
 * @author abhsinh2
 *
 */
public class Utility {
	
	public static final String DEFAULT_BLOG_COLLECTION = "blogs";
	public static final String DEFAULT_SESSION_COLLECTION = "sessions";
	public static final String DEFAULT_USER_COLLECTION = "users";
	public static final String DEFAULT_COMPANY_COLLECTION = "companies";
	
	public static final String KEY_HTTP_PORT = "http.port";
	public static final String KEY_HTTPS_PORT = "https.port";
	public static final String KEY_DB_NAME = "db_name";
	public static final String KEY_CONNECTION_STRING = "connection_string";
	public static final String KEY_BLOG_COLLECTION = "blog_collection";
	public static final String KEY_SESSION_COLLECTION = "session_collection";
	public static final String KEY_USER_COLLECTION = "user_collection";
	public static final String KEY_COMPANY_COLLECTION = "company_collection";

	private static String mongoBlogCollection;
	private static String mongoSessionCollection;
	private static String mongoUserCollection;
	private static String mongoCompanyCollection;
	private static String mongoURIString;
	private static String mongoDatabase;
	private static Boolean useMongoORM = Boolean.FALSE;
	private static Integer httpPort;
	private static Integer httpsPort;
		
	public static void setConfigurationJsonObject(com.google.gson.JsonObject configurationJsonObject) {
		if (configurationJsonObject.get(KEY_HTTP_PORT) != null && !configurationJsonObject.get(KEY_HTTP_PORT).isJsonNull()) {
			httpPort = configurationJsonObject.get(KEY_HTTP_PORT).getAsInt();
		}
		
		if (configurationJsonObject.get(KEY_HTTPS_PORT) != null && !configurationJsonObject.get(KEY_HTTPS_PORT).isJsonNull()) {
			httpsPort = configurationJsonObject.get(KEY_HTTPS_PORT).getAsInt();
		}		
		
		mongoDatabase = configurationJsonObject.get(KEY_DB_NAME).getAsString();
		mongoURIString = configurationJsonObject.get(KEY_CONNECTION_STRING).getAsString();
		mongoBlogCollection = configurationJsonObject.get(KEY_BLOG_COLLECTION).getAsString();
		mongoSessionCollection = configurationJsonObject.get(KEY_SESSION_COLLECTION).getAsString();
		mongoUserCollection = configurationJsonObject.get(KEY_USER_COLLECTION).getAsString();
		mongoCompanyCollection = configurationJsonObject.get(KEY_COMPANY_COLLECTION).getAsString();
	}

	/**
	 * Returns encrypted password
	 * 
	 * @param password
	 * @param salt
	 * @return
	 */
	public static String encodePassword(String password, String appender) {		
		String hashedPass = password + "," + appender;
		return Base64.getEncoder().encodeToString(hashedPass.getBytes()) + "," + appender;
	}
	
	public static String decodePassword(String password) {
		String[] str = password.split(",");		
		String hashedPass = str[0];
		String appendedPass = new String(Base64.getDecoder().decode(hashedPass));
		return appendedPass.split(",")[0];
	}

	/**
	 * Returns Date object for given date string
	 * 
	 * @param dateString
	 * @return
	 */
	public static Date convertToDate(String dateString) {
		// TODO
		// http://www.mkyong.com/java/java-date-and-calendar-examples/
		SimpleDateFormat format = new SimpleDateFormat("dd MMM yyyy HH:mm:ss z");
		Date date = null;
		try {
			date = format.parse(dateString);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}

	/**
	 * Returns Date string for given Date object
	 * 
	 * @param date
	 * @return
	 */
	public static String convertToString(Date date) {
		SimpleDateFormat format = new SimpleDateFormat("E, dd MMM yyyy HH:mm:ss z");
		String dateToStr = format.format(date);
		return dateToStr;
	}

	/**
	 * Converts Google JsonObject to Vertx JsonObject
	 * 
	 * @param googleJsonObject
	 * @return
	 */
	public static io.vertx.core.json.JsonObject toVertxJson(com.google.gson.JsonObject googleJsonObject) {
		io.vertx.core.json.JsonObject json = new io.vertx.core.json.JsonObject(googleJsonObject.toString());
		return json;
	}

	public static String getMongoBlogCollection() {
		return mongoBlogCollection;
	}

	public static String getMongoSessionCollection() {
		return mongoSessionCollection;
	}

	public static String getMongoUserCollection() {
		return mongoUserCollection;
	}
	
	public static String getMongoCompanyCollection() {
		return mongoCompanyCollection;
	}

	public static String getMongoURIString() {
		return mongoURIString;
	}

	public static String getMongoDatabase() {
		return mongoDatabase;
	}

	public static Boolean getUseMongoORM() {
		return useMongoORM;
	}

	public static Integer getHttpPort() {
		return httpPort;
	}
	
	public static Integer getHttpsPort() {
		return httpsPort;
	}
}
