package com.cisco.cmad.blog.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.bson.Document;
import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Field;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Index;
import org.mongodb.morphia.annotations.Indexed;
import org.mongodb.morphia.annotations.Indexes;
import org.mongodb.morphia.utils.IndexDirection;
import org.mongodb.morphia.utils.IndexType;

import com.cisco.cmad.blog.util.Utility;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

/**
 * Model class to store Blogs
 * 
 * @author abhsinh2
 *
 */
@Entity(value = Utility.DEFAULT_BLOG_COLLECTION, noClassnameStored = true)
@Indexes({ @Index(fields = { @Field("tags"), @Field(value = "date", type = IndexType.DESC) }),
		@Index(fields = { @Field("author"), @Field(value = "date", type = IndexType.DESC) }) })
public class Blog extends InstanceImpl {

	// Attributes used as json keys
	public static final String CONTENT_TAG = "content";
	public static final String URLLINK_TAG = "urllink";
	public static final String AUTHOR_TAG = "author";
	public static final String TITLE_TAG = "title";
	public static final String TAGS_TAG = "tags";
	public static final String COMMENTS_TAG = "comments";
	public static final String DATE_TAG = "date";

	@Id
	private ObjectId id;

	@Indexed(unique = true, dropDups = true)
	private String urllink;
	private String title;
	private String content;

	@Indexed(unique = false, dropDups = false)
	private String author;

	@Indexed(unique = false, dropDups = false)
	private Set<String> tags = new HashSet<String>();

	@Embedded
	private List<Comment> comments = new ArrayList<Comment>();

	@Indexed(value = IndexDirection.DESC, unique = false, dropDups = false)
	private Date date;

	public Blog() {
		super();
	}

	public Blog(JsonObject json) {
		super(json);
		this.urllink = json.get(_ID_TAG).getAsString();
		this.title = json.get(TITLE_TAG).getAsString();
		this.content = json.get(CONTENT_TAG).getAsString();
		this.author = json.get(AUTHOR_TAG).getAsString();
		this.date = Utility.convertToDate(json.get(DATE_TAG).getAsString());

		JsonArray tagsArray = json.get(TAGS_TAG).getAsJsonArray();
		for (int i = 0; i < tagsArray.size(); i++) {
			this.tags.add(tagsArray.get(i).getAsString());
		}

		JsonArray commentsArray = json.get(COMMENTS_TAG).getAsJsonArray();
		for (int i = 0; i < commentsArray.size(); i++) {
			JsonObject commentJsonObject = commentsArray.get(i).getAsJsonObject();
			Comment comment = new Comment();
			comment.setAuthor(commentJsonObject.get(Comment.AUTHOR_TAG).getAsString());
			comment.setEmail(commentJsonObject.get(Comment.EMAIL_TAG).getAsString());
			comment.setContent(commentJsonObject.get(Comment.CONTENT_TAG).getAsString());
			comment.setDate(Utility.convertToDate(commentJsonObject.get(Comment.DATE_TAG).getAsString()));

			this.comments.add(comment);
		}
	}

	@SuppressWarnings("unchecked")
	public Blog(Document document) {
		super(document);

		if (document != null) {
			this.setId(document.getObjectId(Blog._ID_TAG));
			this.setUrllink(document.getString(Blog.URLLINK_TAG));
			this.setContent(document.getString(Blog.CONTENT_TAG));
			this.setAuthor(document.getString(Blog.AUTHOR_TAG));
			this.setTitle(document.getString(Blog.TITLE_TAG));
			this.setDate(document.getDate(Blog.DATE_TAG));

			List<String> tagsList = (List<String>) document.get(Blog.TAGS_TAG);
			this.setTags(new HashSet<>(tagsList));

			List<Document> commentDocuments = (List<Document>) document.get(Blog.COMMENTS_TAG);

			for (Document commentDocument : commentDocuments) {
				this.addComment(new Comment(commentDocument));
			}
		}
	}

	public Blog(String title, String content, String author) {
		this(title, content, author, new HashSet<String>());
	}
	
	public Blog(String title, String content, String author, Set<String> tags) {
		this.title = title;
		this.content = content;
		this.author = author;
		this.tags = tags;
	}

	public ObjectId getId() {
		return id;
	}

	public void setId(ObjectId id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getUrllink() {
		return urllink;
	}

	public void setUrllink(String urllink) {
		this.urllink = urllink;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public Set<String> getTags() {
		return tags;
	}

	public void setTags(Set<String> tags) {
		if (tags != null)
			this.tags = tags;
	}

	public void addTag(String tag) {
		if (tag != null)
			this.tags.add(tag);
	}

	public List<Comment> getComments() {
		return comments;
	}

	public void setComments(List<Comment> comments) {
		if (comments != null)
			this.comments = comments;
	}

	public void addComment(Comment comment) {
		if (comment != null)
			this.comments.add(comment);
	}

	public Date getDate() {
		if (this.date == null) {
			return new Date();
		}
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public JsonObject toJson() {
		JsonObject json = new JsonObject();
		json.addProperty(CONTENT_TAG, this.content);
		json.addProperty(_ID_TAG, this.urllink);
		json.addProperty(AUTHOR_TAG, this.author);
		json.addProperty(TITLE_TAG, this.title);
		json.addProperty(DATE_TAG, Utility.convertToString(this.date));

		JsonArray tagsArray = new JsonArray();
		for (String tag : this.tags) {
			tagsArray.add(new JsonPrimitive(tag));
		}

		json.add(TAGS_TAG, tagsArray);

		JsonArray commentsArray = new JsonArray();
		for (Comment comment : this.comments) {
			commentsArray.add(comment.toJson());
		}

		json.add(COMMENTS_TAG, commentsArray);

		return json;
	}

	@Override
	public Document toMongoDocument() {
		Document blog = new Document();
		blog.append(Blog.TITLE_TAG, this.getTitle());
		blog.append(Blog.AUTHOR_TAG, this.getAuthor());
		blog.append(Blog.CONTENT_TAG, this.getContent());
		blog.append(Blog.URLLINK_TAG, this.createUrllink());
		blog.append(Blog.TAGS_TAG, this.getTags());
		
		List<Document> commentDocs = new ArrayList<>();
		for (Comment comment : this.getComments()) {
			commentDocs.add(comment.toMongoDocument());
		}
		blog.append(Blog.COMMENTS_TAG, commentDocs);
		
		blog.append(Blog.DATE_TAG, this.getDate());
		return blog;
	}

	public String createUrllink() {
		// whitespace becomes _
		String urllink = title.replaceAll("\\s", "_");
		// get rid of non alphanumeric
		urllink = urllink.replaceAll("\\W", "");

		urllink = urllink.toLowerCase();

		String permLinkExtra = String.valueOf(GregorianCalendar.getInstance().getTimeInMillis());
		urllink += permLinkExtra;

		return urllink;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((urllink == null) ? 0 : urllink.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Blog other = (Blog) obj;
		if (urllink == null) {
			if (other.urllink != null)
				return false;
		} else if (!urllink.equals(other.urllink))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Blog [" + "id=" + id + ", urllink=" + urllink + ", title=" + title + ", content=" + content
				+ ", author=" + author + ", tags=" + tags + ", comments=" + comments + ", date=" + date + "]";
	}

}
