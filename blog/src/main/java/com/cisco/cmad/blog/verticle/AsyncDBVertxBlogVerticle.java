package com.cisco.cmad.blog.verticle;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bson.types.ObjectId;

import com.cisco.cmad.blog.auth.MongoDBUserAuthProvider;
import com.cisco.cmad.blog.dao.BlogDAO;
import com.cisco.cmad.blog.dao.CompanyDAO;
import com.cisco.cmad.blog.dao.SessionDAO;
import com.cisco.cmad.blog.dao.UserDAO;
import com.cisco.cmad.blog.dto.BlogDTO;
import com.cisco.cmad.blog.dto.CommentDTO;
import com.cisco.cmad.blog.dto.CompanyDTO;
import com.cisco.cmad.blog.dto.DepartmentDTO;
import com.cisco.cmad.blog.dto.SiteDTO;
import com.cisco.cmad.blog.dto.UserDTO;
import com.cisco.cmad.blog.model.Blog;
import com.cisco.cmad.blog.model.Comment;
import com.cisco.cmad.blog.model.Company;
import com.cisco.cmad.blog.model.Department;
import com.cisco.cmad.blog.model.Site;
import com.cisco.cmad.blog.model.User;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonObject;

import io.vertx.core.http.HttpHeaders;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.Json;
import io.vertx.ext.auth.AuthProvider;
import io.vertx.ext.web.RoutingContext;

/**
 * Vertx Verticle for async db operation
 * 
 * @author abhsinh2
 *
 */
public class AsyncDBVertxBlogVerticle extends VertxBlogVerticle {

	protected BlogDAO blogDAO;
	protected UserDAO userDAO;
	protected SessionDAO sessionDAO;
	protected CompanyDAO companyDAO;

	public AsyncDBVertxBlogVerticle(BlogDAO blogDAO, UserDAO userDAO, SessionDAO sessionDAO, CompanyDAO companyDAO) {
		this(new MongoDBUserAuthProvider(userDAO), blogDAO, userDAO, sessionDAO, companyDAO);
	}

	public AsyncDBVertxBlogVerticle(AuthProvider authProvider, BlogDAO blogDAO, UserDAO userDAO,
			SessionDAO sessionDAO, CompanyDAO companyDAO) {
		super(sessionDAO, authProvider);

		this.blogDAO = blogDAO;
		this.userDAO = userDAO;
		this.sessionDAO = sessionDAO;
		this.companyDAO = companyDAO;
	}

	@Override
	public void addNewBlog(RoutingContext routingContext) {
		System.out.println("AsyncDBVertxBlogVerticle.addNewBlog:" + routingContext.getBodyAsString());
		HttpServerResponse response = routingContext.response();
		
		// TODO: This is blocking
		String username = verifySessionInRequest(routingContext, this.sessionDAO);
		if (username == null) {
			System.out.println("AsyncDBVertxBlogVerticle.addNewBlog username is null");
			response.end();
			return;
		}

		BlogDTO blogdto = Json.decodeValue(routingContext.getBodyAsString(), BlogDTO.class);
		blogdto.setDate(new Date());

		Blog blog = blogdto.toModel();	
		blog.setAuthor(username);

		vertx.executeBlocking(future -> {
			// Call some blocking API that takes a significant amount of time to
			// return
			String urllink = blogDAO.add(blog);
			future.complete(urllink);
		} , res -> {
			if (res.succeeded()) {
				String urllink = (String) res.result();
				response.setStatusCode(201);
				response.putHeader(HttpHeaders.CONTENT_TYPE, "text/plain");
				response.end(urllink);
			} else if (res.failed()) {
				String urllink = (String) res.result();
				response.setStatusCode(500);
				response.putHeader(HttpHeaders.CONTENT_TYPE, "text/plain");
				response.end(urllink);
			}			
		});

		System.out.println("AsyncDBVertxBlogVerticle.addNewBlog completes");
	}

	@Override
	public void getBlog(RoutingContext routingContext) {
		System.out.println("AsyncDBVertxBlogVerticle.getBlog");

		HttpServerResponse response = routingContext.response();
		String urllink = routingContext.request().getParam("urllink");

		vertx.executeBlocking(future -> {
			Blog blog = blogDAO.findByUrlLink(urllink);
			future.complete(blog);
		} , res -> {
			Blog blog = (Blog) res.result();

			if (blog != null) {
				vertx.executeBlocking(future1 -> {
					User user = userDAO.findByUsername(blog.getAuthor());
					future1.complete(user);
				} , res1 -> {
					User user = (User) res1.result();

					BlogDTO blogdto = new BlogDTO(blog);

					if (user != null) {
						blogdto.setUserFirst(user.getFirstname());
						blogdto.setUserLast(user.getLastname());
					}

					ObjectMapper mapper = new ObjectMapper();
					JsonNode node = mapper.valueToTree(blogdto);

					response.setStatusCode(200);
					response.putHeader(HttpHeaders.CONTENT_TYPE, "application/json; charset=utf-8");
					response.end(node.toString());
				});
			} else {
				response.setStatusCode(404).end("Blog not found");
			}
		});
		System.out.println("AsyncDBVertxBlogVerticle.getBlog completes");
	}

	@SuppressWarnings("unchecked")
	@Override
	protected void getAllBlogs(RoutingContext routingContext) {
		System.out.println("AsyncDBVertxBlogVerticle.getAllBlogs");
		HttpServerResponse response = routingContext.response();			

		vertx.executeBlocking(future -> {
			List<Blog> blogs = blogDAO.find(10, true);

			future.complete(blogs);
		} , res -> {
			List<Blog> blogs = (List<Blog>) res.result();

			if (blogs != null && blogs.size() > 0) {
				vertx.executeBlocking(future1 -> {
					List<BlogDTO> dtos = new ArrayList<>(blogs.size());

					for (Blog blog : blogs) {
						User user = userDAO.findByUsername(blog.getAuthor());

						BlogDTO blogdto = new BlogDTO(blog);

						if (user != null) {
							blogdto.setUserFirst(user.getFirstname());
							blogdto.setUserLast(user.getLastname());
						}

						dtos.add(blogdto);
					}

					future1.complete(dtos);
				} , res1 -> {
					List<BlogDTO> dtos = (List<BlogDTO>) res1.result();

					ObjectMapper mapper = new ObjectMapper();
					JsonNode node = mapper.valueToTree(dtos);

					response.setStatusCode(200);
					response.putHeader(HttpHeaders.CONTENT_TYPE, "application/json; charset=utf-8");
					response.end(node.toString());
				});
			} else {
				List<BlogDTO> dtos = new ArrayList<>();
				ObjectMapper mapper = new ObjectMapper();
				JsonNode node = mapper.valueToTree(dtos);

				response.setStatusCode(200);
				response.putHeader(HttpHeaders.CONTENT_TYPE, "application/json; charset=utf-8");
				response.end(node.toString());
			}
		});

		System.out.println("AsyncDBVertxBlogVerticle.getAllBlogs completes");
	}

	@SuppressWarnings("unchecked")
	@Override
	protected void getBlogsForUser(RoutingContext routingContext) {
		System.out.println("AsyncDBVertxBlogVerticle.getBlogsForUser");
		String username = routingContext.request().getParam("username");
		HttpServerResponse response = routingContext.response();

		vertx.executeBlocking(future -> {
			List<Blog> blogs = blogDAO.findByAuthor(username, null, Boolean.TRUE);
			future.complete(blogs);
		} , res -> {
			List<Blog> blogs = (List<Blog>) res.result();

			if (blogs != null && blogs.size() > 0) {
				vertx.executeBlocking(future1 -> {
					List<BlogDTO> dtos = new ArrayList<>(blogs.size());
					for (Blog blog : blogs) {
						User user = userDAO.findByUsername(blog.getAuthor());

						BlogDTO blogdto = new BlogDTO(blog);

						if (user != null) {
							blogdto.setUserFirst(user.getFirstname());
							blogdto.setUserLast(user.getLastname());
						}

						dtos.add(blogdto);
					}
					future1.complete(dtos);
				} , res1 -> {
					List<BlogDTO> dtos = (List<BlogDTO>) res1.result();

					ObjectMapper mapper = new ObjectMapper();
					JsonNode node = mapper.valueToTree(dtos);

					response.setStatusCode(200);
					response.putHeader(HttpHeaders.CONTENT_TYPE, "application/json");
					response.end(node.toString());
				});
			} else {
				List<BlogDTO> dtos = new ArrayList<>();
				ObjectMapper mapper = new ObjectMapper();
				JsonNode node = mapper.valueToTree(dtos);

				response.setStatusCode(200);
				response.putHeader(HttpHeaders.CONTENT_TYPE, "application/json; charset=utf-8");
				response.end(node.toString());
			}
		});

		System.out.println("AsyncDBVertxBlogVerticle.getBlogsForUser completes");
	}

	@SuppressWarnings("unchecked")
	@Override
	public void getBlogsByTag(RoutingContext routingContext) {
		System.out.println("AsyncDBVertxBlogVerticle.getBlogsByTag");

		String tag = routingContext.request().getParam("thetag");
		HttpServerResponse response = routingContext.response();	

		vertx.executeBlocking(future -> {
			List<Blog> blogs = null;
			if (tag == null || tag.equals("all")) {
				blogs = blogDAO.find(null, Boolean.TRUE);
			} else {
				blogs = blogDAO.findByTag(tag, null, Boolean.TRUE);
			}
			
			future.complete(blogs);
		} , res -> {
			List<Blog> blogs = (List<Blog>) res.result();
			
			System.out.println("AsyncDBVertxBlogVerticle.getBlogsByTag blogs size " + blogs.size());
			
			if (blogs != null && blogs.size() > 0) {
				vertx.executeBlocking(future1 -> {
					List<BlogDTO> dtos = new ArrayList<>(blogs.size());
					for (Blog blog : blogs) {
						User user = userDAO.findByUsername(blog.getAuthor());

						BlogDTO blogdto = new BlogDTO(blog);

						if (user != null) {
							blogdto.setUserFirst(user.getFirstname());
							blogdto.setUserLast(user.getLastname());
						}

						dtos.add(blogdto);
					}
					future1.complete(dtos);
				} , res1 -> {
					List<BlogDTO> dtos = (List<BlogDTO>) res1.result();
					
					System.out.println("AsyncDBVertxBlogVerticle.getBlogsByTag dtos size " + blogs.size());
					
					ObjectMapper mapper = new ObjectMapper();
					JsonNode node = mapper.valueToTree(dtos);

					response.setStatusCode(200);
					response.putHeader(HttpHeaders.CONTENT_TYPE, "application/json");
					
					System.out.println("AsyncDBVertxBlogVerticle.getBlogsByTag node " + node);
					
					response.end(node.toString());
				});
			} else {
				List<BlogDTO> dtos = new ArrayList<>();
				ObjectMapper mapper = new ObjectMapper();
				JsonNode node = mapper.valueToTree(dtos);

				response.setStatusCode(200);
				response.putHeader(HttpHeaders.CONTENT_TYPE, "application/json; charset=utf-8");
				response.end(node.toString());
			}
		});

		System.out.println("AsyncDBVertxBlogVerticle.getBlogsByTag completes");
	}

	@Override
	public void deleteBlog(RoutingContext routingContext) {
		vertx.executeBlocking(future -> {
			future.complete();
		} , res -> {
			routingContext.response().setStatusCode(201);
			routingContext.response().putHeader(HttpHeaders.CONTENT_TYPE, "application/json; charset=utf-8");
			routingContext.response().end(Json.encodePrettily("Blog Added"));
		});
	}

	@Override
	public void addNewComment(RoutingContext routingContext) {
		System.out.println("AsyncDBVertxBlogVerticle.addNewComment:" + routingContext.getBodyAsString());
		HttpServerResponse response = routingContext.response();
		
		// TODO: This is blocking
		String username = verifySessionInRequest(routingContext, this.sessionDAO);
		if (username == null) {
			System.out.println("AsyncDBVertxBlogVerticle.addNewComment username is null");
			
			response.setStatusCode(401);
			response.putHeader(HttpHeaders.CONTENT_TYPE, "text/html");
			response.end();
			return;
		}

		String urllink = routingContext.request().getParam("urllink");

		System.out.println("AsyncDBVertxBlogVerticle.addNewComment urllink:" + urllink);		

		CommentDTO commentDto = Json.decodeValue(routingContext.getBodyAsString(), CommentDTO.class);
		commentDto.setDate(new Date());
		commentDto.setAuthor(username);

		Comment comment = commentDto.toModel();
		comment.setAuthor(username);

		vertx.executeBlocking(future -> {
			blogDAO.addComment(comment, urllink);
			future.complete();
		} , res -> {	
			if (res.succeeded()) {
				// websocket start	
				JsonObject commentWSData = commentDto.toJson();
				
				JsonObject wsData = new JsonObject();
				wsData.addProperty("urllink", urllink);
				wsData.add("comment", commentWSData);
				
				JsonObject rootObj = new JsonObject();
				rootObj.addProperty("messageType", "NewComment");
				rootObj.add("messageObject", wsData);
				
				this.sendMessageOverWebSocket(rootObj.toString());
				// websocket end
				
				JsonObject returnObj = new JsonObject();
				returnObj.addProperty("author", username);
				
				response.setStatusCode(201);
				response.putHeader(HttpHeaders.CONTENT_TYPE, "application/json");
				response.end(returnObj.toString());
			} else if (res.failed()) {
				response.setStatusCode(500);
				response.putHeader(HttpHeaders.CONTENT_TYPE, "text/html");
				response.end();
			}			
		});

		System.out.println("AsyncDBVertxBlogVerticle.addNewComment");
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void addNewUser(RoutingContext routingContext) {
		System.out.println("AsyncDBVertxBlogVerticle.addNewUser");
		System.out.println("AsyncDBVertxBlogVerticle.addNewUser:" + routingContext.getBodyAsString());
		HttpServerResponse response = routingContext.response();

		UserDTO userDto = Json.decodeValue(routingContext.getBodyAsString(), UserDTO.class);
		User user = userDto.toModel();
				
		if (userDto.getIsCompany()) {
			String companyName = userDto.getCompanyName();
			String siteName = userDto.getSiteName();
			String deptName = userDto.getDeptName();
			
			Department dept = new Department();
			dept.setName(deptName);
			
			Site site = new Site();
			site.setName(siteName);
			site.addDepartment(dept);
			
			Company company = new Company();
			company.setName(companyName);
			company.addSite(site);	
			
			user.setIsCompany(Boolean.TRUE);
						
			vertx.executeBlocking(future -> {
				ObjectId companyId = companyDAO.addCompany(company);
				System.out.println("AsyncDBVertxBlogVerticle.addNewUser companyId:" + companyId);
				
				Integer siteId = companyDAO.getSite(companyId, siteName).getId();
				Integer deptId = companyDAO.getDepartment(companyId, siteId, deptName).getId();
				
				Map<String, Object> map = new HashMap<>();
				map.put("companyId", companyId);
				map.put("siteId", siteId);
				map.put("deptId", deptId);
				
				future.complete(map);
			} , res -> {
				Map<String, Object> map = (Map<String, Object>)res.result();
				ObjectId companyId = (ObjectId) map.get("companyId");
				Integer siteId = (Integer) map.get("siteId");
				Integer deptId = (Integer) map.get("deptId");
				
				user.setCompanyId(companyId.toString());
				user.setSiteId(siteId);
				user.setDeptId(deptId);
				
				vertx.executeBlocking(future1 -> {
					boolean isUserAdded = userDAO.add(user);
					future1.complete(isUserAdded);
				} , res1 -> {
					Boolean isUserAdded = (Boolean) res1.result();
					
					if (isUserAdded) {
						response.setStatusCode(201);
						response.putHeader(HttpHeaders.CONTENT_TYPE, "text/plain");
						response.end("User Added");
					} else {
						response.setStatusCode(500);
						response.putHeader(HttpHeaders.CONTENT_TYPE, "text/plain");
						response.end("User Already Exists");
					}			
				});			
			});		
		} else {
			vertx.executeBlocking(future -> {
				boolean isAdded = userDAO.add(user);
				future.complete(isAdded);
			} , res -> {
				Boolean isAdded = (Boolean) res.result();
				
				if (isAdded) {
					response.setStatusCode(201);
					response.putHeader(HttpHeaders.CONTENT_TYPE, "text/plain");
					response.end("User Added");
				} else {
					response.setStatusCode(500);
					response.putHeader(HttpHeaders.CONTENT_TYPE, "text/plain");
					response.end("User Already Exists");
				}			
			});
		}

		System.out.println("AsyncDBVertxBlogVerticle.addNewUser completes");
	}

	@Override
	public void login(RoutingContext routingContext) {
		System.out.println("AsyncDBVertxBlogVerticle.login");
		//System.out.println("AsyncDBVertxBlogVerticle.login:" + routingContext.getBodyAsString());
		HttpServerResponse response = routingContext.response();
		
		UserDTO userDto = Json.decodeValue(routingContext.getBodyAsString(), UserDTO.class);

		String username = userDto.getUsername();
		String password = userDto.getPassword();

		vertx.executeBlocking(future -> {
			User user = userDAO.validateLogin(username, password);
			future.complete(user);
		} , res -> {
			User user = (User) res.result();

			if (user == null) {
				response.setStatusCode(401);
				response.putHeader(HttpHeaders.CONTENT_TYPE, "text/html");
				response.end("Invalid Login");
			} else {
				// Handle session using Session
				handleLogin(username, this.sessionDAO, routingContext);				
				
				// websocket start	
				JsonObject wsData = new JsonObject();
				wsData.addProperty("username", username);
				wsData.addProperty("firstname", user.getFirstname());
				wsData.addProperty("lastname", user.getLastname());
				
				JsonObject rootObj = new JsonObject();
				rootObj.addProperty("messageType", "UserLogin");
				rootObj.add("messageObject", wsData);
								
				this.sendMessageOverWebSocket(rootObj.toString());
				// websocket end
				
				JsonObject responseObj = new JsonObject();
				responseObj.addProperty("username", username);
				responseObj.addProperty("isCompany", user.getIsCompany());
				responseObj.addProperty("companyId", user.getCompanyId());
				
				response.setStatusCode(201);
				response.putHeader(HttpHeaders.CONTENT_TYPE, "text/html");
				response.end(responseObj.toString());
			}
		});

		System.out.println("AsyncDBVertxBlogVerticle.login completes");
	}	

	@Override
	public void logout(RoutingContext routingContext) {
		System.out.println("AsyncDBVertxBlogVerticle.logout");
		HttpServerResponse response = routingContext.response();
		
		vertx.executeBlocking(future -> {
			String username = handleLogout(this.sessionDAO, routingContext);
			future.complete(username);
		} , res -> {
			String username = (String)res.result();
			
			// websocket start	
			JsonObject wsData = new JsonObject();
			wsData.addProperty("username", username);
			
			JsonObject rootObj = new JsonObject();
			rootObj.addProperty("messageType", "UserLogout");
			rootObj.add("messageObject", wsData);
			
			this.sendMessageOverWebSocket(rootObj.toString());
			// websocket end
			
			response.setStatusCode(201);
			response.putHeader(HttpHeaders.CONTENT_TYPE, "text/html");
			response.end();
		});
		System.out.println("AsyncDBVertxBlogVerticle.logout completes");
	}

	@Override
	public void blog_not_found(RoutingContext routingContext) {
		HttpServerResponse response = routingContext.response();
		vertx.executeBlocking(future -> {
			future.complete();
		} , res -> {
			response.setStatusCode(201);
			response.putHeader(HttpHeaders.CONTENT_TYPE, "application/json; charset=utf-8");
			response.end("Some");
		});
	}

	@Override
	public void internalError(RoutingContext routingContext) {
		HttpServerResponse response = routingContext.response();

		vertx.executeBlocking(future -> {
			future.complete();
		} , res -> {
			response.setStatusCode(201);
			response.putHeader(HttpHeaders.CONTENT_TYPE, "application/json; charset=utf-8");
			response.end("Some");
		});
	}

	@Override
	public void getAllSignedInUsers(RoutingContext routingContext) {
		HttpServerResponse response = routingContext.response();
		vertx.executeBlocking(future -> {
			String username = verifySessionInRequest(routingContext, this.sessionDAO);			
			future.complete(username);
		} , res -> {
			String username = (String) res.result();
			
			System.out.println("AsyncDBVertxBlogVerticle.isUserSignedIn username:" + username);			
			
			if (username == null) {
				System.out.println("AsyncDBVertxBlogVerticle.isUserSignedIn username is null");
				response.setStatusCode(500);
				response.putHeader(HttpHeaders.CONTENT_TYPE, "text/html");
				response.end("user not signed in");
			} else {				
				List<String> allSignedInUserNames = sessionDAO.getAllSignedInUsernames();
				
				List<UserDTO> usersDto = new ArrayList<>();
				for (String uu : allSignedInUserNames) {
					User user = userDAO.findByUsername(uu);
					UserDTO userDto = new UserDTO(user);
					usersDto.add(userDto);
				}				
				
				ObjectMapper mapper = new ObjectMapper();
				JsonNode node = mapper.valueToTree(usersDto);
				
				response.setStatusCode(200);
				response.putHeader(HttpHeaders.CONTENT_TYPE, "text/html");
				response.end(node.toString());
			}
		});		
	}

	@SuppressWarnings("unchecked")
	@Override
	public void getCompanies(RoutingContext routingContext) {
		System.out.println("AsyncDBVertxBlogVerticle.getCompanies");
		HttpServerResponse response = routingContext.response();
		
		vertx.executeBlocking(future -> {
			List<Company> companies = companyDAO.getCompanies();
			future.complete(companies);
		} , res -> {			
			if (res.succeeded()) {				
				List<Company> companies = (List<Company>) res.result();
				List<CompanyDTO> dtos = new ArrayList<>();
				
				if (companies != null && companies.size() > 0) {
					dtos = new ArrayList<>(companies.size());

					for (Company company : companies) {
						CompanyDTO dto = new CompanyDTO(company);
						dtos.add(dto);
					}				
				} else {
					
				}
				
				ObjectMapper mapper = new ObjectMapper();
				JsonNode node = mapper.valueToTree(dtos);

				response.setStatusCode(200);
				response.putHeader(HttpHeaders.CONTENT_TYPE, "application/json");
				response.end(node.toString());
				
			} else if (res.failed()) {
				response.setStatusCode(500);
				response.putHeader(HttpHeaders.CONTENT_TYPE, "text/plain");
				response.end();
			}			
		});
		
		System.out.println("AsyncDBVertxBlogVerticle.getCompanies completes");
	}

	@SuppressWarnings("unchecked")
	@Override
	public void getSites(RoutingContext routingContext) {
		System.out.println("AsyncDBVertxBlogVerticle.getSites");
		HttpServerResponse response = routingContext.response();
		String companyId = routingContext.request().getParam("companyId");
		
		vertx.executeBlocking(future -> {
			List<Site> sites = companyDAO.getSites(new ObjectId(companyId));
			future.complete(sites);
		} , res -> {			
			if (res.succeeded()) {				
				List<Site> sites = (List<Site>) res.result();
				
				List<SiteDTO> dtos = new ArrayList<>();

				if (sites != null && sites.size() > 0) {
					dtos = new ArrayList<>(sites.size());

					for (Site site : sites) {
						SiteDTO dto = new SiteDTO(site);
						dtos.add(dto);
					}
				} else {
					
				}
				
				ObjectMapper mapper = new ObjectMapper();
				JsonNode node = mapper.valueToTree(dtos);

				response.setStatusCode(200);
				response.putHeader(HttpHeaders.CONTENT_TYPE, "application/json");
				response.end(node.toString());
				
			} else if (res.failed()) {
				response.setStatusCode(500);
				response.putHeader(HttpHeaders.CONTENT_TYPE, "text/plain");
				response.end();
			}			
		});
		
		System.out.println("AsyncDBVertxBlogVerticle.getSites completes");		
	}

	@SuppressWarnings("unchecked")
	@Override
	public void getDepartments(RoutingContext routingContext) {
		System.out.println("AsyncDBVertxBlogVerticle.getDepartments");
		HttpServerResponse response = routingContext.response();
		
		String companyId = routingContext.request().getParam("companyId");
		String siteId = routingContext.request().getParam("siteId");
		
		vertx.executeBlocking(future -> {
			List<Department> departments = companyDAO.getDepartments(new ObjectId(companyId), Integer.parseInt(siteId));
			future.complete(departments);
		} , res -> {			
			if (res.succeeded()) {				
				List<Department> departments = (List<Department>) res.result();
				
				List<DepartmentDTO> dtos = new ArrayList<>();
				if (departments != null && departments.size() > 0) {
					dtos = new ArrayList<>(departments.size());

					for (Department department : departments) {
						DepartmentDTO dto = new DepartmentDTO(department);
						dtos.add(dto);
					}
				} else {
					
				}
				
				ObjectMapper mapper = new ObjectMapper();
				JsonNode node = mapper.valueToTree(dtos);

				response.setStatusCode(200);
				response.putHeader(HttpHeaders.CONTENT_TYPE, "application/json");
				response.end(node.toString());				
			} else if (res.failed()) {
				response.setStatusCode(500);
				response.putHeader(HttpHeaders.CONTENT_TYPE, "text/plain");
				response.end();
			}			
		});
		System.out.println("AsyncDBVertxBlogVerticle.getDepartments completes");		
	}

	@Override
	public void addCompany(RoutingContext routingContext) {
		System.out.println("AsyncDBVertxBlogVerticle.addCompany");
		HttpServerResponse response = routingContext.response();
		
		CompanyDTO dto = Json.decodeValue(routingContext.getBodyAsString(), CompanyDTO.class);
		Company company = dto.toModel();

		vertx.executeBlocking(future -> {
			ObjectId companyId = companyDAO.addCompany(company);
			future.complete(companyId);
		} , res -> {
			if (res.succeeded()) {
				ObjectId companyId = (ObjectId) res.result();
				response.setStatusCode(201);
				response.putHeader(HttpHeaders.CONTENT_TYPE, "text/plain");
				response.end(companyId.toHexString());
			} else if (res.failed()) {
				response.setStatusCode(500);
				response.putHeader(HttpHeaders.CONTENT_TYPE, "text/plain");
				response.end();
			}			
		});
		
		System.out.println("AsyncDBVertxBlogVerticle.addCompany completes");		
	}

	@Override
	public void addSite(RoutingContext routingContext) {
		System.out.println("AsyncDBVertxBlogVerticle.addSite");
		HttpServerResponse response = routingContext.response();
		
		SiteDTO dto = Json.decodeValue(routingContext.getBodyAsString(), SiteDTO.class);
		String companyId = routingContext.request().getParam("companyId");
		Site site = dto.toModel();
		
		vertx.executeBlocking(future -> {
			Integer siteId = companyDAO.addSite(new ObjectId(companyId), site);
			future.complete(siteId);
		} , res -> {
			if (res.succeeded()) {
				Integer siteId = (Integer) res.result();
				response.setStatusCode(201);
				response.putHeader(HttpHeaders.CONTENT_TYPE, "text/plain");
				response.end(siteId.toString());
			} else if (res.failed()) {
				response.setStatusCode(500);
				response.putHeader(HttpHeaders.CONTENT_TYPE, "text/plain");
				response.end();
			}			
		});
		System.out.println("AsyncDBVertxBlogVerticle.addSite completes");		
	}

	@Override
	public void addDepartment(RoutingContext routingContext) {
		System.out.println("AsyncDBVertxBlogVerticle.addDepartment");
		HttpServerResponse response = routingContext.response();
		
		DepartmentDTO dto = Json.decodeValue(routingContext.getBodyAsString(), DepartmentDTO.class);
		String companyId = routingContext.request().getParam("companyId");
		String siteId = routingContext.request().getParam("siteId");

		Department dept = dto.toModel();
		
		vertx.executeBlocking(future -> {
			Integer deptId = companyDAO.addDepartment(new ObjectId(companyId), Integer.parseInt(siteId), dept);
			future.complete(deptId);
		} , res -> {
			if (res.succeeded()) {
				Integer deptId = (Integer) res.result();
				response.setStatusCode(201);
				response.putHeader(HttpHeaders.CONTENT_TYPE, "text/plain");
				response.end(deptId.toString());
			} else if (res.failed()) {
				response.setStatusCode(500);
				response.putHeader(HttpHeaders.CONTENT_TYPE, "text/plain");
				response.end();
			}			
		});
		System.out.println("AsyncDBVertxBlogVerticle.addDepartment completes");		
	}

	@Override
	protected void getCompany(RoutingContext routingContext) {
		
		HttpServerResponse response = routingContext.response();
		
		String companyId = routingContext.request().getParam("companyId");
		
		System.out.println("AsyncDBVertxBlogVerticle.getCompany for a given companyId : "+companyId);
		
		vertx.executeBlocking(future -> {
			Company company = companyDAO.getCompany(new ObjectId(companyId));
			future.complete(company);
		} , res -> {			
			if (res.succeeded()) {				
				Company company = (Company) res.result();

				CompanyDTO dto = null;

				if (company != null) {
					dto = new CompanyDTO(company);
				}
				ObjectMapper mapper = new ObjectMapper();
				JsonNode node = mapper.valueToTree(dto);

				response.setStatusCode(200);
				response.putHeader(HttpHeaders.CONTENT_TYPE, "application/json");
				response.end(node.toString());
				
			} else if (res.failed()) {
				response.setStatusCode(500);
				response.putHeader(HttpHeaders.CONTENT_TYPE, "text/plain");
				response.end();
			}			
		});
		
		System.out.println("AsyncDBVertxBlogVerticle.getCompany completes");
		
	}	
}
