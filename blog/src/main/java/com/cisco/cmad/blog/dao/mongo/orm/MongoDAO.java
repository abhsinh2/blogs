package com.cisco.cmad.blog.dao.mongo.orm;

import org.mongodb.morphia.Datastore;

/**
 * Base class for DAOs to provide morphia Datastore.
 * 
 * @author abhsinh2
 *
 */
public abstract class MongoDAO {
	protected Datastore dataStore;

	public MongoDAO(Datastore dataStore) {
		this.dataStore = dataStore;
	}
}
