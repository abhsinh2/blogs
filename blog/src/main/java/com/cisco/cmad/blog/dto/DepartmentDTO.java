package com.cisco.cmad.blog.dto;

import com.cisco.cmad.blog.model.Department;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

/**
 * Represents Data Transfer Objects for Department in Site.
 * 
 * @author abhsinh2
 *
 */
public class DepartmentDTO {
	private Integer id;
	private String deptName;

	public DepartmentDTO() {

	}

	public DepartmentDTO(Department department) {
		this.id = department.getId();
		this.deptName = department.getName();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public Department toModel() {
		Department department = new Department();
		department.setName(this.getDeptName());
		return department;
	}
	
	public JsonObject toJson() {
		Gson gson = new Gson();
		return gson.toJsonTree(this).getAsJsonObject();
	}
}
