package com.cisco.cmad.blog.model;

import org.bson.Document;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

/**
 * Base class for models
 * 
 * @author abhsinh2
 *
 */
public abstract class InstanceImpl {
	public static final String _ID_TAG = "_id";
	public static final String ID_TAG = "id";
	public static final String NAME_TAG = "name";

	public InstanceImpl() {

	}

	public InstanceImpl(JsonObject json) {

	}

	public InstanceImpl(Document document) {

	}

	protected abstract JsonObject toJson();

	protected abstract Document toMongoDocument();

	@SuppressWarnings("unchecked")
	protected <T> T getValue(JsonObject json, String key, Class<T> t) {
		JsonElement ele = json.get(key);
		if (ele != null && !ele.isJsonNull()) {
			if (t == String.class) {
				return (T) ele.getAsString();
			} else if (t == Boolean.class) {
				return (T) Boolean.valueOf(ele.getAsBoolean());
			} else if (t == JsonObject.class) {
				return (T) ele.getAsJsonObject();
			} else if (t == JsonArray.class) {
				return (T) ele.getAsJsonArray();
			} else if (t == Long.class) {
				return (T) Long.valueOf(ele.getAsLong());
			} else if (t == Integer.class) {
				return (T) Integer.valueOf(ele.getAsInt());
			}
		}
		return null;
	}
	
	protected void addToDocument(Document userDocument, String key, Object value) {
		if (value != null) {
			userDocument.append(key, value);
		}
	}
}
