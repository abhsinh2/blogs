package com.cisco.cmad.blog;

import java.io.File;
import java.io.FileReader;

import org.mongodb.morphia.Datastore;

import com.cisco.cmad.blog.dao.BlogDAO;
import com.cisco.cmad.blog.dao.CompanyDAO;
import com.cisco.cmad.blog.dao.SessionDAO;
import com.cisco.cmad.blog.dao.UserDAO;
import com.cisco.cmad.blog.mongo.ServicesFactory;
import com.cisco.cmad.blog.util.Utility;
import com.cisco.cmad.blog.verticle.AsyncDBVertxBlogVerticle;
import com.cisco.cmad.blog.verticle.BlockedDBVertxBlogVerticle;
import com.cisco.cmad.blog.verticle.VertxBlogVerticle;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.mongodb.client.MongoDatabase;

import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;

/**
 * Starts Application
 * 
 * @author abhsinh2
 *
 */
public class StartUp {
	//public static Logger vertxLogger;
	
	public static void main(String[] args) {
		String jsonFilePath = System.getProperty("BlogConfFile");
		
		if (jsonFilePath == null || !(new File(jsonFilePath)).exists()) {			
			if (args != null && args.length > 0) {
				jsonFilePath = args[0];
				
				if (jsonFilePath == null || !(new File(jsonFilePath)).exists()) {
					jsonFilePath = System.getProperty("user.dir") + "/src/main/resources"
							+ "/conf/blog-application-conf.json";
				} else {
					System.out.println("Got File from Argument " + jsonFilePath);
				}
			} else {
				System.out.println("No Argument passed");
				jsonFilePath = System.getProperty("user.dir") + "/src/main/resources"
						+ "/conf/blog-application-conf.json";
			}
		} else {
			System.out.println("Got File from BlogConfFile " + jsonFilePath);
		}

		JsonObject rootJsonObject = null;
		try {
			JsonElement root = new JsonParser().parse(new FileReader(new File(jsonFilePath)));
			System.out.println("File Parsed");
			rootJsonObject = root.getAsJsonObject();
			Utility.setConfigurationJsonObject(rootJsonObject);
		} catch (Exception e) {
			System.out.println("File not Parsed");
			System.out.println(e.getMessage());
			System.out.println(e);
		}		
		
		System.out.println("rootJsonObject:" + rootJsonObject);
		//vertxLogger = LoggerFactory.getLogger(StartUp.class.getName());

		BlogDAO blogDAO = null;
		UserDAO userDAO = null;
		SessionDAO sessionDAO = null;
		CompanyDAO companyDAO = null;

		String db_query_type = rootJsonObject.get("mongo_query_type") != null
				? rootJsonObject.get("mongo_query_type").getAsString() : "native";

		if (db_query_type.equals("native")) {
			MongoDatabase database = ServicesFactory.getMongoDatabase();
			blogDAO = new com.cisco.cmad.blog.dao.mongo.BlogDAOImpl(database);
			userDAO = new com.cisco.cmad.blog.dao.mongo.UserDAOImpl(database);
			sessionDAO = new com.cisco.cmad.blog.dao.mongo.SessionDAOImpl(database);
			companyDAO = new com.cisco.cmad.blog.dao.mongo.CompanyDAOImpl(database);
		} else if (db_query_type.equals("morphic")) {
			Datastore datastore = ServicesFactory.getMongoMorphiaDatastore();
			blogDAO = new com.cisco.cmad.blog.dao.mongo.orm.BlogDAOImpl(datastore);
			userDAO = new com.cisco.cmad.blog.dao.mongo.orm.UserDAOImpl(datastore);
			sessionDAO = new com.cisco.cmad.blog.dao.mongo.orm.SessionDAOImpl(datastore);
			companyDAO = new com.cisco.cmad.blog.dao.mongo.orm.CompanyDAOImpl(datastore);
		}

		String verticle_type = rootJsonObject.get("verticle_type") != null
				? rootJsonObject.get("verticle_type").getAsString() : "async";

		VertxBlogVerticle verticle = null;
		if (verticle_type.equals("async")) {
			verticle = new AsyncDBVertxBlogVerticle(blogDAO, userDAO, sessionDAO, companyDAO);
		} else {
			verticle = new BlockedDBVertxBlogVerticle(blogDAO, userDAO, sessionDAO, companyDAO);
		}
		
		VertxOptions options = new VertxOptions().setWorkerPoolSize(10);
		Vertx vertx = Vertx.vertx(options);
		
		vertx.deployVerticle(verticle, new Handler<AsyncResult<String>>() {
			@Override
			public void handle(AsyncResult<String> result) {
				System.out.println("Startup Handle");
			}
		});
	}
}
