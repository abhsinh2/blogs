package com.cisco.cmad.blog.model;

import java.util.ArrayList;
import java.util.List;

import org.bson.Document;
import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Field;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Index;
import org.mongodb.morphia.annotations.Indexes;

import com.cisco.cmad.blog.util.Utility;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

/**
 * Model class to store Company
 * 
 * @author abhsinh2
 *
 */
@Entity(value = Utility.DEFAULT_COMPANY_COLLECTION, noClassnameStored = true)
@Indexes({
	@Index(fields = {@Field(value = "sites.id")}),
	@Index(fields = {@Field(value = "sites.departments.id")})
})
public class Company extends InstanceImpl {
	public static final String SITES_TAG = "sites";

	@Id
	private ObjectId _id;
	private String name;
	@Embedded
	private List<Site> sites = new ArrayList<>();

	public Company() {
		super();
	}

	public Company(JsonObject json) {
		super(json);
		this._id = new ObjectId(this.getValue(json, _ID_TAG, String.class));
		this.name = this.getValue(json, NAME_TAG, String.class);

		JsonArray siteArr = this.getValue(json, SITES_TAG, JsonArray.class);
		if (siteArr != null) {
			for (int i = 0; i < siteArr.size(); i++) {
				JsonObject siteObj = siteArr.get(i).getAsJsonObject();
				Site site = new Site(siteObj);
				addSite(site);
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	public Company(Document document) {
		this.set_id(document.getObjectId(Company._ID_TAG));
		this.setName(document.getString(Company.NAME_TAG));

		List<Document> siteDocs = (List<Document>) document.get(Company.SITES_TAG);
		if (siteDocs != null && !siteDocs.isEmpty()) {
			for (Document siteDoc : siteDocs) {
				this.addSite(new Site(siteDoc));
			}
		}
	}

	public ObjectId get_id() {
		return _id;
	}

	public void set_id(ObjectId _id) {
		this._id = _id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Site> getSites() {
		return sites;
	}

	public void setSites(List<Site> sites) {
		this.sites = sites;
	}

	public void addSite(Site site) {
		if (site != null) {
			this.sites.add(site);
		}
	}

	@Override
	public JsonObject toJson() {
		JsonObject json = new JsonObject();
		json.addProperty(_ID_TAG, this._id.toHexString());
		json.addProperty(NAME_TAG, this.name);
		return json;
	}
	
	@Override
	public Document toMongoDocument() {
		Document company = new Document();
		company.append(Company.NAME_TAG, this.getName());
		
		List<Document> siteDocs = new ArrayList<>();
		for (Site site : this.getSites()) {
			siteDocs.add(site.toMongoDocument());
		}
		
		company.append(Company.SITES_TAG, siteDocs);		
		
		return company;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((_id == null) ? 0 : _id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Company other = (Company) obj;
		if (_id == null) {
			if (other._id != null)
				return false;
		} else if (!_id.equals(other._id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Company [_id=" + _id + ", name=" + name + ", sites=" + sites + "]";
	}
	
}
