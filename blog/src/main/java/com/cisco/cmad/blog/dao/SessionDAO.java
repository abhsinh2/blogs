package com.cisco.cmad.blog.dao;

import java.util.List;

import com.cisco.cmad.blog.model.Session;

/**
 * Data Access Object to handle Session
 * 
 * @author abhsinh2
 *
 */
public interface SessionDAO {

	/**
	 * Returns username for given sessionId
	 * 
	 * @param sessionId
	 * @return
	 */
	public String findUserNameBySessionId(String sessionId);

	/**
	 * Creates a session for username
	 * 
	 * @param username
	 * @return
	 */
	public String startSession(String username);
	
	/**
	 * Creates a session for username and given sessionId
	 * 
	 * @param username
	 * @return
	 */
	public String startSession(String username, String sessionId);

	/**
	 * Destroys the given sessionId
	 * 
	 * @param sessionID
	 */
	public String endSession(String sessionID);

	/**
	 * Returns Session for sessionId
	 * 
	 * @param sessionID
	 * @return
	 */
	public Session getSession(String sessionID);
	
	public List<String> getAllSignedInUsernames();
	
	/**
	 * remove all the sessions
	 */
	public void clearAll();
}
