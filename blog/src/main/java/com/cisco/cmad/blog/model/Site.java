package com.cisco.cmad.blog.model;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import org.bson.Document;
import org.mongodb.morphia.annotations.Embedded;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

/**
 * Model class to store Sites
 * 
 * @author abhsinh2
 *
 */
@Embedded
public class Site extends InstanceImpl {
	public static final String DEPTS_TAG = "departments";

	private Integer id;
	private String name;
	@Embedded
	private List<Department> departments = new ArrayList<>();
	
	private static AtomicInteger atomicInteger = new AtomicInteger(1000);

	public Site() {
		super();
	}

	public Site(JsonObject json) {
		super(json);
		this.id = this.getValue(json, ID_TAG, Integer.class);
		this.name = this.getValue(json, NAME_TAG, String.class);

		JsonArray deptArr = this.getValue(json, DEPTS_TAG, JsonArray.class);
		if (deptArr != null) {
			for (int i = 0; i < deptArr.size(); i++) {
				JsonObject deptObj = deptArr.get(i).getAsJsonObject();
				Department dept = new Department(deptObj);
				addDepartment(dept);
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	public Site(Document document) {
		this.setId(document.getInteger(Site.ID_TAG));
		this.setName(document.getString(Site.NAME_TAG));

		List<Document> deptsDocs = (List<Document>) document.get(Site.DEPTS_TAG);
		if (deptsDocs != null && !deptsDocs.isEmpty()) {
			for (Document deptsDoc : deptsDocs) {
				this.addDepartment(new Department(deptsDoc));
			}
		}
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Department> getDepartments() {
		return departments;
	}

	public void setDepartments(List<Department> departments) {
		this.departments = departments;
	}

	public void addDepartment(Department department) {
		if (department != null) {
			this.departments.add(department);
		}
	}

	@Override
	public String toString() {
		return "Site [id=" + id + ", name=" + name + ", departments=" + departments + "]";
	}

	@Override
	public JsonObject toJson() {
		JsonObject json = new JsonObject();
		json.addProperty(ID_TAG, this.id);
		json.addProperty(NAME_TAG, this.name);

		JsonArray deptArr = new JsonArray();
		for (Department dept : this.getDepartments()) {
			deptArr.add(dept.toJson());
		}

		json.add(DEPTS_TAG, deptArr);

		return json;
	}
	
	@Override
	public Document toMongoDocument() {
		Document site = new Document();
		site.append(Site.ID_TAG, this.getId());
		site.append(Site.NAME_TAG, this.getName());
		
		List<Document> deptDocs = new ArrayList<>();
		for (Department department : this.getDepartments()) {
			deptDocs.add(department.toMongoDocument());
		}
		
		site.append(Site.DEPTS_TAG, deptDocs);		
		
		return site;
	}
	
	public static Integer getUniqueId() {
		return atomicInteger.incrementAndGet();
	}
	
}
