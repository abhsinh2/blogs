package com.cisco.cmad.blog.verticle;

import java.util.Date;

import com.cisco.cmad.blog.dto.BlogDTO;
import com.cisco.cmad.blog.model.Blog;
import com.cisco.cmad.blog.model.User;
import com.cisco.cmad.blog.util.Utility;

import io.vertx.core.Vertx;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.mongo.MongoClient;
import io.vertx.ext.web.RoutingContext;

/**
 * Vertx Verticle by using Vertx MongoDb async APIs.
 * 
 * @author abhsinh2
 *
 */
public class VertxMongoVertxBlogVerticle extends VertxBlogVerticle {
	private MongoClient mongo;

	public VertxMongoVertxBlogVerticle(Vertx myvertx) {
		String uri = Utility.getMongoURIString();
		String db = Utility.getMongoDatabase();
		
		System.out.println(myvertx);

		JsonObject mongoconfig = new JsonObject().put("connection_string", uri).put("db_name", db);

		mongo = MongoClient.createShared(myvertx, mongoconfig);
	}

	@Override
	public void addNewBlog(RoutingContext routingContext) {
		BlogDTO blogDto = Json.decodeValue(routingContext.getBodyAsString(), BlogDTO.class);
		Blog blog = blogDto.toModel();
		blog.setDate(new Date());
		
		mongo.insert(Utility.getMongoBlogCollection(), Utility.toVertxJson(blog.toJson()), lookup -> {
			// error handling
			if (lookup.failed()) {
				routingContext.fail(500);
				return;
			}

			routingContext.response().setStatusCode(201);
			routingContext.response().putHeader(HttpHeaders.CONTENT_TYPE, "text/html");
			routingContext.response().end();
		});
	}

	@Override
	public void getBlog(RoutingContext routingContext) {
		mongo.findOne(Utility.getMongoBlogCollection(),
				new JsonObject().put(Blog.URLLINK_TAG, routingContext.request().getParam("urllink")), null, lookup -> {
					// error handling
					if (lookup.failed()) {
						routingContext.fail(500);
						return;
					}

					JsonObject blog = lookup.result();

					if (blog == null) {
						routingContext.fail(404);
					} else {
						routingContext.response().setStatusCode(200);
						routingContext.response().putHeader(HttpHeaders.CONTENT_TYPE, "application/json");
						routingContext.response().end(blog.encode());
					}
				});
	}

	@Override
	protected void getAllBlogs(RoutingContext routingContext) {
		mongo.find(Utility.getMongoBlogCollection(), new JsonObject(), lookup -> {
			// error handling
			if (lookup.failed()) {
				routingContext.fail(500);
				return;
			}

			final JsonArray json = new JsonArray();

			for (JsonObject o : lookup.result()) {
				json.add(o);
			}

			routingContext.response().setStatusCode(201);
			routingContext.response().putHeader(HttpHeaders.CONTENT_TYPE, "application/json");
			routingContext.response().end(json.encode());
		});
	}

	@Override
	protected void getBlogsForUser(RoutingContext routingContext) {
		mongo.find(Utility.getMongoBlogCollection(),
				new JsonObject().put(Blog.AUTHOR_TAG, routingContext.request().getParam("username")), lookup -> {
					// error handling
					if (lookup.failed()) {
						routingContext.fail(500);
						return;
					}

					final JsonArray json = new JsonArray();

					for (JsonObject o : lookup.result()) {
						json.add(o);
					}

					routingContext.response().setStatusCode(201);
					routingContext.response().putHeader(HttpHeaders.CONTENT_TYPE, "application/json");
					routingContext.response().end(json.encode());
				});
	}

	@Override
	public void getBlogsByTag(RoutingContext routingContext) {
		mongo.find(Utility.getMongoBlogCollection(),
				new JsonObject().put(Blog.TAGS_TAG, routingContext.request().getParam("thetag")), lookup -> {
					// error handling
					if (lookup.failed()) {
						routingContext.fail(500);
						return;
					}

					final JsonArray json = new JsonArray();

					for (JsonObject o : lookup.result()) {
						json.add(o);
					}

					routingContext.response().setStatusCode(201);
					routingContext.response().putHeader(HttpHeaders.CONTENT_TYPE, "application/json");
					routingContext.response().end(json.encode());
				});
	}

	@Override
	public void deleteBlog(RoutingContext routingContext) {
		mongo.findOne(Utility.getMongoBlogCollection(),
				new JsonObject().put(Blog.URLLINK_TAG, routingContext.request().getParam("urllink")), null, lookup -> {
					// error handling
					if (lookup.failed()) {
						routingContext.fail(500);
						return;
					}

					JsonObject blog = lookup.result();

					if (blog == null) {
						routingContext.fail(404);
					} else {
						mongo.remove(Utility.getMongoBlogCollection(),
								new JsonObject().put(Blog.URLLINK_TAG, routingContext.request().getParam("urllink")),
								remove -> {
							// error handling
							if (remove.failed()) {
								routingContext.fail(500);
								return;
							}

							routingContext.response().setStatusCode(204);
							routingContext.response().end();
						});
					}
				});
	}

	@Override
	public void addNewComment(RoutingContext routingContext) {
		JsonObject newComment = routingContext.getBodyAsJson();

		mongo.findOne(Utility.getMongoBlogCollection(),
				new JsonObject().put(Blog.URLLINK_TAG, routingContext.request().getParam("urllink")), null, lookup -> {
					// error handling
					if (lookup.failed()) {
						routingContext.fail(500);
						return;
					}

					JsonObject blog = lookup.result();

					if (blog == null) {
						routingContext.fail(404);
					} else {
						JsonArray comments = blog.getJsonArray(Blog.COMMENTS_TAG);
						comments.add(newComment);

						mongo.update(Utility.getMongoBlogCollection(), blog, null, lookup1 -> {
							// error handling
							if (lookup1.failed()) {
								routingContext.fail(500);
								return;
							}

							routingContext.response().setStatusCode(201);
							routingContext.response().putHeader(HttpHeaders.CONTENT_TYPE,
									"application/json; charset=utf-8");
							routingContext.response().end();
						});
					}
				});
	}

	@Override
	public void addNewUser(RoutingContext routingContext) {
		JsonObject newUser = routingContext.getBodyAsJson();

		mongo.findOne(Utility.getMongoUserCollection(),
				new JsonObject().put(User._ID_TAG, newUser.getString(User.USERNAME_TAG)), null, lookup -> {
					// error handling
					if (lookup.failed()) {
						routingContext.fail(500);
						return;
					}

					JsonObject user = lookup.result();

					if (user != null) {
						// already exists
						routingContext.fail(500);
					} else {
						mongo.insert(Utility.getMongoUserCollection(), newUser, insert -> {
							// error handling
							if (insert.failed()) {
								routingContext.fail(500);
								return;
							}

							// add the generated id to the user object
							// newUser.put("_id", insert.result());
							routingContext.response().setStatusCode(200);
							routingContext.response().putHeader(HttpHeaders.CONTENT_TYPE, "application/json");
							routingContext.response().end(newUser.encode());
						});
					}
				});
	}

	@Override
	public void login(RoutingContext routingContext) {
		JsonObject userJson = routingContext.getBodyAsJson();
		String username = userJson.getString(User.USERNAME_TAG);
		String password = userJson.getString(User.PASSWORD_TAG);

		System.out.println("login: " + userJson);
		System.out.println("login: " + username + ", " + password);
		
		HttpServerResponse response = routingContext.response();
		
		mongo.findOne(Utility.getMongoUserCollection(), new JsonObject().put(User._ID_TAG, username), null, lookup -> {
			// error handling
			if (lookup.failed()) {
				routingContext.fail(500);
				return;
			}

			JsonObject user = lookup.result();
			
			if (user == null) {
				response.setStatusCode(401);
				response.end();
				return;
			}			

			String hashedAndSalted = user.getString(User.PASSWORD_TAG);
			String salt = hashedAndSalted.split(",")[1];
			String hashedPassword = Utility.encodePassword(password, salt);

			if (!hashedAndSalted.equals(hashedPassword)) {
				System.out.println("Submitted password is not a match");

				response.setStatusCode(401);
			} else {
				response.setStatusCode(201);
			}

			response.putHeader(HttpHeaders.CONTENT_TYPE, "text/html");
			response.end();
		});
	}

	@Override
	public void logout(RoutingContext routingContext) {
		System.out.println("BlockedDBVertxBlogVerticle.logout");
		
		System.out.println("BlockedDBVertxBlogVerticle.logout completes");
	}

	@Override
	public void blog_not_found(RoutingContext routingContext) {

	}

	@Override
	public void internalError(RoutingContext routingContext) {

	}

	@Override
	protected void getAllSignedInUsers(RoutingContext routingContext) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void getCompanies(RoutingContext routingContext) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void getSites(RoutingContext routingContext) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void getDepartments(RoutingContext routingContext) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void addCompany(RoutingContext routingContext) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void addSite(RoutingContext routingContext) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void addDepartment(RoutingContext routingContext) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void getCompany(RoutingContext routingContext) {
		// TODO Auto-generated method stub
		
	}
}
