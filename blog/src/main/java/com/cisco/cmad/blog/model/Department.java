package com.cisco.cmad.blog.model;

import java.util.concurrent.atomic.AtomicInteger;

import org.bson.Document;
import org.mongodb.morphia.annotations.Embedded;

import com.google.gson.JsonObject;

/**
 * Model class to store Departments
 * 
 * @author abhsinh2
 *
 */
@Embedded
public class Department extends InstanceImpl {
	private Integer id;
	private String name;

	private static AtomicInteger atomicInteger = new AtomicInteger(2000);

	public Department() {
		super();
	}

	public Department(JsonObject json) {
		super(json);
		this.id = this.getValue(json, ID_TAG, Integer.class);
		this.name = this.getValue(json, NAME_TAG, String.class);
	}

	public Department(Document document) {
		super(document);

		this.setId(document.getInteger(Department.ID_TAG));
		this.setName(document.getString(Department.NAME_TAG));
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public JsonObject toJson() {
		JsonObject json = new JsonObject();
		json.addProperty(ID_TAG, this.getId());
		json.addProperty(NAME_TAG, this.getName());
		return json;
	}

	@Override
	public Document toMongoDocument() {
		Document department = new Document();
		department.append(Department.ID_TAG, this.getId());
		department.append(Department.NAME_TAG, this.getName());
		return department;
	}

	@Override
	public String toString() {
		return "Department [id=" + id + ", name=" + name + "]";
	}

	public static Integer getUniqueId() {
		return atomicInteger.incrementAndGet();
	}
}
