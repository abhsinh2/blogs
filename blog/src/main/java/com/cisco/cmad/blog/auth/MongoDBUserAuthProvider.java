package com.cisco.cmad.blog.auth;

import com.cisco.cmad.blog.dao.UserDAO;

import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.AuthProvider;
import io.vertx.ext.auth.User;

/**
 * Authenticates and Authorizes User
 * 
 * @author abhsinh2
 *
 */
public class MongoDBUserAuthProvider implements AuthProvider {

	private UserDAO userDAO;
	private String rolePrefix = "role:";

	public MongoDBUserAuthProvider(UserDAO userDAO) {
		this.userDAO = userDAO;
	}

	@Override
	public void authenticate(JsonObject authInfo, Handler<AsyncResult<User>> resultHandler) {
		String username = authInfo.getString("username");
		String password = authInfo.getString("password");
		
		System.out.println("MongoDBUserAuthProvider.username:" + username);
		System.out.println("MongoDBUserAuthProvider.password:" + password);

		if (username == null) {
			resultHandler.handle(Future.failedFuture("authInfo must contain username in 'username' field"));
			return;
		}

		if (password == null) {
			resultHandler.handle(Future.failedFuture("authInfo must contain password in 'password' field"));
			return;
		}

		com.cisco.cmad.blog.model.User userModel = userDAO.validateLogin(username, password);

		if (userModel == null) {
			resultHandler.handle(Future.failedFuture("Invalid username/password"));
			return;
		} else {
			resultHandler.handle(Future.succeededFuture(new MongoUser(username, this, rolePrefix)));
		}
	}

}
