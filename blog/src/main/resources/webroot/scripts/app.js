//Each of the controllers should be moved to separate files for easy readability 
//In the end they will be assembled as a single js file using requireJS and grunt build system
(function($) {
	//Setup dependencies for the module
	var app = angular.module('mysocial', ['ngRoute','textAngular','ngWebsocket','ngCookies']);
	app.run(function($http,$rootScope,$location,$log,$websocket,$cookies) {
		$log.debug("App run...");
		$rootScope.currentPath = $location.path()
	});

	//ROUTE configurations for all views
	app.config([ '$routeProvider', function($routeProvider) {
		$routeProvider.when('/', {
			templateUrl : 'templates/appHome.html',
			controller : 'AppHomeController'
		}).when('/login', {
			templateUrl : 'templates/login.html',
			controller : 'LoginController'
		}).when('/register', {
			templateUrl : 'templates/register.html',
			controller : 'LoginController'
		}).when('/newPost', {
			templateUrl : 'templates/BlogEdit.html',
			controller : 'BlogController'
		}).when('/updateCompany', {
			templateUrl : 'templates/companyUpdate.html',
			controller : 'CompanyUpdateController'
		}).when('/logout', {
			templateUrl : 'templates/appHome.html',
			controller : 'LogoutController'
		}).otherwise({
			templateUrl : '/404.html'
		});
	} ]).factory('authHttpResponseInterceptor',
			[ '$q', '$location','$log', function($q, $location, $log, $cookies) {
				return {
					response : function(response) {
						$log.debug($cookies);
						if (response.status === 401) {
							$log.debug("Response 401");
						}
						return response || $q.when(response);
					},
					responseError : function(rejection) {
						if (rejection.status === 401) {
							$log.debug("Response Error 401", rejection);
							$location.path('/login');
						}
						return $q.reject(rejection);
					},
					request: function (config) {							
						$log.debug(config.headers);
						$log.debug($cookies);
					    return config;
					}
				}
			} ]).config([ '$httpProvider', function($httpProvider) {
				// Http Intercpetor to check auth failures for xhr requests
				$httpProvider.interceptors.push('authHttpResponseInterceptor');
				$httpProvider.defaults.withCredentials = true;
			} ]);

	//------------------------------------------------------------------------------------------------------------------
	// Controller for the home page with blogs and live users
	//------------------------------------------------------------------------------------------------------------------
	app.controller('AppHomeController', function($http, $log, $scope, $rootScope, $websocket, $location, $cookies) {
		var controller = this;
		$log.debug("AppHomeController...");
		
		$http.get('/services/blogs').success(function(data, status, headers, config) {
			$scope.blogs = data;
			$scope.loading = false;
		}).error(function(data, status, headers, config) {
			$scope.loading = false;
			$scope.error = status;
		});
		
		var ws=null;
		$http.get('/services/rest/user?signedIn=true').success(function(data, status, headers, config) {
			$scope.connectedUsers = data;
			$scope.loading = false;
			// instance of ngWebsocket, handled by $websocket service
			//Setup a websocket connection to server using current host
			//ws = $websocket.$new('ws://'+$location.host()+':'+$location.port()+'/services/chat/abhi', ['binary', 'base64']);
			var prot = $location.protocol();
			if (prot === 'https') {
				prot = 'wss';
			} else {
				prot = 'ws';
			}
			ws = $websocket.$new({'url': prot + '://' + $location.host() + ':' + $location.port() + '/services/chat/abhi', 'protocols': [], 'subprotocols': ['binary', 'base46'] });
			ws.binaryType = "arraybuffer";
				
			$log.debug("Web socket established...");
				
		    ws.$on('$open', function () {
		        $log.debug('Socket is open');
		    });
			        
	        ws.$on('$message', function(data){
	        	$log.debug('The websocket server has sent the following data:');
	        	 $log.debug(data);
	        	 $log.debug(data.messageType);
	        	 
	        	 if(data.messageType==="UserLogin"){
	        		 var found = false;
	        		 for(var index in $scope.connectedUsers){
	        			 if($scope.connectedUsers[index].username==data.messageObject.username){
	        				 found=true;
	        				 break;
	        			 }
	        		 }
	        		 if(!found){
	        			 $log.debug("Adding user to list: "+data.messageObject.username);
	        			 $scope.connectedUsers.push(data.messageObject);
	        			 $scope.$digest();
	        		 }
	        	 }else if(data.messageType==="UserLogout"){
	        		 var found = false;
	        		 for(var index in $scope.connectedUsers){
	        			 if($scope.connectedUsers[index].username==data.messageObject.username){
	        				 found=true;
	        				 break;
	        			 }
	        		 }
	        		 if(found){
	        			 $log.debug("Removing user to list: "+data.messageObject.username);
	        			 $scope.connectedUsers.pop(data.messageObject);
	        			 $scope.$digest();
	        		 }
	        	 }else if(data.messageType==="chatMessage"){
	        		 $scope.showChat=true
	        		 $log.debug("Updating chat message: ");
	        		 $log.debug(data.messageObject);
	        		 if($scope.chatMessages===undefined)
	        			 $scope.chatMessages=[];
	        		 $scope.chatMessages.push(data.messageObject);
	        		 $log.debug("Chat Messages: ");
	        		 $log.debug($scope.chatMessages);
	        		 $scope.$digest();
	        	 }else if(data.messageType==="NewComment"){
	        		 //Make sure chat window opensup
	        		 var found = false;
	        		 var matchedBlog;
	        		 for(var index in $scope.blogs){
	        			 if($scope.blogs[index].urllink==data.messageObject.urllink){
	        				 found=true;
	        				 matchedBlog = $scope.blogs[index];
	        				 break;
	        			 }
	        		 }
	        		 if(found){
	        			 matchedBlog.comments.push(data.messageObject.comment);
	        			 $scope.$digest();
	        		 }
	        	 }
	        });
			        
	        ws.$on('$close', function () {
	            console.log('Web socket closed');
	            ws.$close();
	        });
		}).error(function(data, status, headers, config) {
			$log.debug("Error");
			$scope.loading = false;
			$scope.error = status;
		});
		
		$scope.tagSearch = function() {
			console.log('Searching tag ' + $scope.searchTag);
			var temptag;
			if ($scope.searchTag == undefined || $scope.searchTag === "") {
				temptag = "all";
			} else {
				temptag = $scope.searchTag
			}
			console.log('Searching tag temptag:' + temptag);
			$http.get('/services/blogs/tag/' + temptag).success(function(data, status, headers, config) {
				$scope.blogs = data;
				$scope.loading = false;
			}).error(function(data, status, headers, config) {
				$scope.loading = false;
				$scope.error = status;
			});
		};
			
		$scope.submitComment = function(comment, urllink){
			$log.debug(comment);
			//var blogId = comment.blogId;
			$http.post('/services/blogs/' + urllink + '/newcomment', comment).success(function(data, status, headers, config) {
				$scope.loading = false;
				var found = false;
				for(var index in $scope.blogs){
					if($scope.blogs[index].urllink == urllink){
						$log.debug("Pushing the added comment to list");
						comment.author = data.author;
						$scope.blogs[index].comments.push(comment);
						found = true;						
						break;
					}
				}
				
				/*if (found) {
					comment.content = "";
					$scope.$digest();
				}*/
			}).error(function(data, status, headers, config) {
				$scope.loading = false;
				$scope.error = status;
			});
		};
		
		$scope.sendMessage = function(chatMessage){
			$log.debug("Sending "+chatMessage);
			ws.$emit('chatMessage', chatMessage); // send a message to the websocket server
			$scope.chatMessage="";
		}
	});
	
	//------------------------------------------------------------------------------------------------------------------
	// Controller for the login view and the registration screen
	//------------------------------------------------------------------------------------------------------------------
	app.controller('LoginController', function($http, $log, $scope, $location, $rootScope, $cookies) {
		var controller = this;
		$scope.isLoadingCompanies = true;
		
		$http.get('/services/companies').success(function(data, status, headers, config) {
			$scope.companies = data;
			$scope.isLoadingCompanies = false;
		}).error(function(data, status, headers, config) {
			$scope.isLoadingCompanies = false;
			$scope.error = status;
		});
		
		$scope.login = function(user) {
			$log.debug("Logging in user..." + $cookies);
			$http.post("/services/users/login", user).success(function(data, status, headers, config) {
				$rootScope.loggedIn = true;
				$rootScope.loggedInUser = data.username;
				$rootScope.isCompany = data.isCompany;
				$rootScope.companyId = data.companyId;
				$log.debug(data);
				$location.path("/");				
			}).error(function(data, status, headers, config) {	
				$scope.data = data;
				$scope.error = status;
				$location.path("/login");
			});
		};
		
		$scope.register = function() {
			$log.debug("Navigating to register...");
			$location.path("/register");
		};
		
		$scope.submitRegister = function(user){
			$log.debug("Registering...");
			$http.post("/services/users/signup", user).success(function(data, status, headers, config) {
				if (status == 500) {
					$scope.data = data;
					$scope.error = status;
					$location.path("/register");
				} else {
					$log.debug(data);
					$location.path("/");
				}						
			}).error(function(data, status, headers, config) {	
				$scope.data = data;
				$scope.error = status;
				$location.path("/register");
			});
		};
		
		$scope.companyChange = function(companyId) {
			$log.debug("Loading sites for company: " + companyId);
			// Load sites
			$http.get('/services/companies/' + companyId + '/sites').success(function(data, status, headers, config) {
				$scope.sites = data;
				$scope.isLoadingSites = false;
			}).error(function(data, status, headers, config) {
				$scope.isLoadingSites = false;
				$scope.error = status;
			});
		};
		
		$scope.siteChange = function(companyId, siteId) {
			$log.debug("Loading departments: " + companyId);
			// Load sites
			$http.get('/services/companies/' + companyId + '/sites/' + siteId + '/departments').success(function(data, status, headers, config) {
				$scope.departments = data;
				$scope.isLoadingDepts = false;
			}).error(function(data, status, headers, config) {
				$scope.isLoadingDepts = false;
				$scope.error = status;
			});
		};
	});
	
	
	
	
	//------------------------------------------------------------------------------------------------------------------
	// Controller for the Company Update view
	//------------------------------------------------------------------------------------------------------------------
	app.controller('CompanyUpdateController', function($http, $log, $scope, $location, $rootScope, $cookies) {
		var controller = this;
		$scope.isLoadingCompanies = true;
		
		$scope.cancelUpdate = function(companyId, site, dept, isSiteAddition){
			$location.path("/");
		};
		
		$http.get('/services/companies/' + $rootScope.companyId + '').success(function(data, status, headers, config) {
			$scope.company = data;
			$scope.isLoadingCompanies = false;
			$scope.sites = data.sites;
		}).error(function(data, status, headers, config) {
			$scope.isLoadingCompanies = false;
			$scope.error = status;
		});
		
		$scope.siteChange = function(companyId, siteId) {
			$log.debug("Loading departments for company: " + companyId);
			$log.debug("Loading departments for siteId: " + companyId);
			// Load sites
			$http.get('/services/companies/' + companyId + '/sites/' + siteId + '/departments').success(function(data, status, headers, config) {
				$scope.departments = data;
				$scope.isLoadingDepts = false;
			}).error(function(data, status, headers, config) {
				$scope.isLoadingDepts = false;
				$scope.error = status;
			});
		};
		
		
		$scope.updateCompany = function(companyId, site, dept, isSiteAddition){
			var url;
			var object;
			if(isSiteAddition == null || !isSiteAddition) {
				$log.debug("Updating the Department for site.id..."+site.id);
				$log.debug("The Dept Details..."+dept.deptName);
				//Dept Addition
				url = '/services/companies/' + companyId + '/sites/' + site.id + '/departments';
				object = dept;
			} else {
				$log.debug("Updating the Site for company.id..."+companyId);
				$log.debug("The Site Details..."+site.siteName);
				//Site Addition
				url = '/services/companies/' + companyId + '/sites';
				object = site;
			}
			
			$http.post(url, object).success(function(data, status, headers, config) {
				if (status == 500) {
					$scope.data = data;
					$scope.error = status;
					$location.path("/updateCompany");
				} else {
					$log.debug(data);
					$location.path("/");
				}						
			}).error(function(data, status, headers, config) {	
				$scope.data = data;
				$scope.error = status;
				$location.path("/updateCompany");
			});
			
		};
		
	});
	
	//------------------------------------------------------------------------------------------------------------------
	// Controller for the logout view 
	//------------------------------------------------------------------------------------------------------------------
	app.controller('LogoutController', function($http, $log, $scope, $location, $rootScope, $cookies) {
		var controller = this;
		$scope.isLoadingCompanies = true;
		
		$http.get('/services/users/logout').success(function(data, status, headers, config) {
			$rootScope.loggedIn = false;
			$rootScope.loggedInUser = null;
			$rootScope.isCompany = false;
			$rootScope.companyId = null;
			$location.path('/');
		}).error(function(data, status, headers, config) {
			$rootScope.loggedIn = false;
			$rootScope.loggedInUser = null;
			$rootScope.isCompany = false;
			$rootScope.companyId = null;
			$location.path('/');
		});
	});
	
	//------------------------------------------------------------------------------------------------------------------
	// Controller for the navigation bar.. currently has no functions
	//------------------------------------------------------------------------------------------------------------------
	app.controller('NavbarController', function($http, $log, $scope, $rootScope) {
		var controller = this;
		$log.debug("Navbar controller...");
	});

	//------------------------------------------------------------------------------------------------------------------
	// Controller for new blog post view
	//------------------------------------------------------------------------------------------------------------------
	app.controller('BlogController', function($http, $log, $scope, $location, $cookies) {
		var controller = this;
		$log.debug("Blog controller..." + $cookies);
		$scope.blog={};
		$scope.blog.content = 'Blog text here...';
		$scope.saveBlog = function(blog){
			$http.post("/services/blogs/newblog", blog).success(function() {
				$log.debug("Saved blog...");
				$location.path("/");
			}).error(function(data, status, headers, config) {
				console.log(data);
				console.log(status);
				console.log(headers);
				console.log(config);
			});
		};
		$scope.cancel = function(blog){
			$location.path("/");
		};
	});

})($);//Passing jquery object just in case 
